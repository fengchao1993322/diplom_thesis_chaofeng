#remove this import
from o2sc import compressor

class Decompressor:
    __stored_pattern   = []

    __context_current  = []
    __context_previous = []
    __context_deltas   = []

    STATE_NONE         = -1
    STATE_FULL         = 0
    __state            = STATE_NONE

    __temp_packet     = []

    __current_offset  = 0
    __flags           = 0

    __uc_packet       = []
    __co_packet       = []

    debug_info_on     = False

    def __init__(self):
        self.__stored_pattern   = []

        self.__context_current  = []
        self.__context_previous = []
        self.__context_deltas   = []

        self.__state            = self.STATE_NONE

        self.__temp_packet     = []

        self.__current_offset  = 0
        self.__flags           = 0

        self.__uc_packet       = []
        self.__co_packet       = []

    def decompress(self, packet,):
        if len(packet) == 0:
            raise ValueError("Empty packet received!")

        self.__uc_packet = []
        self.__co_packet = packet
        self.__current_offset = 0

        # save context
        self.__context_previous = self.__context_current.copy()

        self.__flags = packet[self.__current_offset]
        self.__current_offset += 1

        # remove this
        if (self.__flags & compressor.Compressor.FLAG_SEQ_PKT_2) == compressor.Compressor.FLAG_SEQ_PKT_2:
            if self.debug_info_on:
                print('Sequential packet 2')
            self.__evaluate_seq_pkt_2()
        elif (self.__flags & compressor.Compressor.FLAG_SEQ_PKT_0) == compressor.Compressor.FLAG_SEQ_PKT_0:
            if self.debug_info_on:
                print('Sequential packet 0')
            self.__evaluate_seq_pkt_0()
        elif (self.__flags & compressor.Compressor.FLAG_SEQ_PKT_1) == compressor.Compressor.FLAG_SEQ_PKT_1:
            if self.debug_info_on:
                print('Sequential packet 1')
            self.__evaluate_seq_pkt_1()

        else:
            if self.debug_info_on:
                print('Non-sequential packet')
            self.__evaluate_non_seq_pkt()

        # decompress
        self.__assemble_uc_pkt()

        return self.__uc_packet

    def __evaluate_non_seq_pkt(self):
        # pattern length
        pattern_len = 0
        if (self.__flags & compressor.Compressor.FLAG_LONG_PATTERN) == compressor.Compressor.FLAG_LONG_PATTERN:
            pattern_len = self.__co_packet[self.__current_offset] << 8
            self.__current_offset += 1
        pattern_len += self.__co_packet[self.__current_offset]
        self.__current_offset += 1

        if self.debug_info_on:
            print("Pattern length: " + str(pattern_len))

        # decypher pattern
        #if self.__state == Compressor.STATE_NONE:
        self.__evaluate_pattern(pattern_len)
        if self.debug_info_on:
            print("Pattern: " + str(self.__stored_pattern))

        # reserve bytes
        #print("patlen: " + str(len(self.__stored_pattern)))
        for i in range(len(self.__stored_pattern)):
            self.__context_current.append(0x00)
            self.__context_deltas.append(0x00)

        # static
        self.__evaluate_static()
        # sequential
        self.__evaluate_sequential_as_random()
        # random
        self.__evaluate_random()

    def __evaluate_pattern(self, pattern_len):
        self.__stored_pattern = []
        #print(str(len(self.__co_packet)) + " - " + str(self.__current_offset) + " - " + str(pattern_len))
        #c = 0
        #print(len(self.__co_packet) - self.__current_offset - pattern_len)
        #for i in range(len(self.__co_packet) - self.__current_offset - pattern_len):
        if (self.__co_packet[self.__current_offset] & 0x40) > 0:
            i = 2
            while len(self.__stored_pattern) < (len(self.__co_packet) - self.__current_offset - pattern_len):
                flag = (self.__co_packet[self.__current_offset + (i // 8)] >> (7 - i % 8) & 0x01)
                self.__stored_pattern.append(compressor.Compressor.PATTERN_CO_STATIC
                    if flag == 0 else compressor.Compressor.PATTERN_CO_RANDOM)
                i += 1
        else:
            i = 1
            last_non_infered_pattern = 0
            while len(self.__stored_pattern) < (len(self.__co_packet) - self.__current_offset - pattern_len):
                #print(i)

                #if len(self.__stored_pattern) > (len(self.__co_packet) - self.__current_offset - pattern_len):
                #    raise ValueError("")

                flag = 0x00
                if (i % 4) == 0:
                    flag = (self.__co_packet[self.__current_offset + (i // 4)] & 0xc0) >> 6
                    #print(self.__co_packet[self.__current_offset + (i // 4)])
                elif (i % 4) == 1:
                    flag = (self.__co_packet[self.__current_offset + (i // 4)] & 0x30) >> 4
                elif (i % 4) == 2:
                    flag = (self.__co_packet[self.__current_offset + (i // 4)] & 0x0c) >> 2
                else:
                    flag = self.__co_packet[self.__current_offset + (i // 4)] & 0x03
                    #c += 1
                #print("q" + str(self.__stored_pattern[len(self.__stored_pattern)-1]))
            #print(c)
                if flag == compressor.Compressor.PATTERN_CO_INFERED:
                    #print("Infered")
                    rep = 0x00
                    for k in range(1, 4):
                        #print("IK " + str(i+k))
                        bits = 0x00
                        if ((i+k) % 4) == 0:
                            bits = (self.__co_packet[self.__current_offset + ((i+k) // 4)] & 0xc0) >> 6
                        elif ((i+k) % 4) == 1:
                            bits = (self.__co_packet[self.__current_offset + ((i+k) // 4)] & 0x30) >> 4
                        elif ((i+k) % 4) == 2:
                            bits = (self.__co_packet[self.__current_offset + ((i+k) // 4)] & 0x0c) >> 2
                        else:
                            bits = self.__co_packet[self.__current_offset + ((i+k) // 4)] & 0x03
                        #print("B " + str(bits))
                        # flag + h2
                        if k == 1:
                            rep |= (bits & 0x01) << 4
                            #print("R2 " + str(hex(rep)))
                        # h4 + h8
                        elif k == 2:
                            rep |= bits << 2
                            #print("R48 " + str(hex(rep)))
                        # h16 + h32
                        else:
                            rep |= bits
                            #print("R1632 " + str(hex(rep)))
                    rep += 1

                    if self.debug_info_on:
                        print("Repeat " + str(hex(last_non_infered_pattern)) + " " + str(rep) + "times")

                    for k in range(rep):
                        self.__stored_pattern.append(last_non_infered_pattern)
                        #print(last_non_infered_pattern)
                    i += 3
                else:
                    last_non_infered_pattern = flag
                    #print(last_non_infered_pattern)
                    self.__stored_pattern.append(flag)

                i += 1
        self.__current_offset += pattern_len

    def __evaluate_seq_pkt_0(self):
        # sequential
        self.__evaluate_sequential_as_random()
        # random
        self.__evaluate_random()

        # update deltas
        self.__compute_deltas()

    def __evaluate_seq_pkt_1(self):
        # sequential
        self.__evaluate_sequential_lsb4()
        # random
        self.__evaluate_random()

        # update deltas
        self.__compute_deltas()

    def __evaluate_seq_pkt_2(self):
        # sequential
        self.__evaluate_sequential_deltas()
        # random
        self.__evaluate_random()

    def __evaluate_static(self):
        c = 0
        for i in range(len(self.__stored_pattern)):
            if self.__stored_pattern[i] == compressor.Compressor.PATTERN_CO_STATIC:
                #print('+' + str(i))
                c += 1
                self.__context_current[i] = self.__co_packet[self.__current_offset]
                self.__current_offset += 1
        #print("Static " + str(c))

    def __evaluate_sequential_as_random(self):
        #c = 0
        for i in range(len(self.__stored_pattern)):
            #print(self.__co_packet[self.__current_offset])
            if self.__stored_pattern[i] == compressor.Compressor.PATTERN_CO_SEQ:
                #c += 1
                self.__context_current[i] = self.__co_packet[self.__current_offset]
                self.__current_offset += 1
        #print("Seq " + str(c))

    def __evaluate_sequential_lsb4(self):
        is_upper = True
        byte = 0
        for i in range(len(self.__stored_pattern)):
            if self.__stored_pattern[i] == compressor.Compressor.PATTERN_CO_SEQ:
                value = 0
                if is_upper:
                    byte = self.__co_packet[self.__current_offset]
                    self.__current_offset += 1
                    value = byte >> 4
                    is_upper = False
                else:
                    value = byte & 0x0f
                    is_upper = True
                self.__context_current[i] = Decompressor.__decompress_lsb4(self.__context_current[i], value)

    def __evaluate_sequential_deltas(self):
        if len(self.__context_deltas) == 0:
            raise ValueError("Delta compression is invalid at this point in time!")

        for i in range(len(self.__stored_pattern)):
            if self.__stored_pattern[i] == compressor.Compressor.PATTERN_CO_SEQ:
                if (self.__context_current[i] + self.__context_deltas[i]) > 255:
                    self.__context_current[i] = self.__context_current[i] + self.__context_deltas[i] % 256
                else:
                    self.__context_current[i] = self.__context_current[i] + self.__context_deltas[i]

    def __evaluate_random(self):
        for i in range(len(self.__stored_pattern)):
            if self.__stored_pattern[i] == compressor.Compressor.PATTERN_CO_RANDOM:
                self.__context_current[i] = self.__co_packet[self.__current_offset]
                self.__current_offset += 1

    def __assemble_uc_pkt(self):
        for i in range(len(self.__stored_pattern)):
            self.__uc_packet.append(0x00)
            if self.__stored_pattern[i] == compressor.Compressor.PATTERN_CO_STATIC:
                self.__uc_packet[i] = self.__context_current[i]
            elif self.__stored_pattern[i] == compressor.Compressor.PATTERN_CO_SEQ:
                self.__uc_packet[i] = self.__context_current[i]
            elif self.__stored_pattern[i] == compressor.Compressor.PATTERN_CO_RANDOM:
                self.__uc_packet[i] = self.__context_current[i]
            else:
                raise ValueError("Unknown field encountered!")

    def __compute_deltas(self):
        for i in range(len(self.__stored_pattern)):
            #if self.__context_current[i] - self.__context_previous
            self.__context_deltas[i] = self.__context_current[i] - self.__context_previous[i]

    def __decompress_lsb4(v_ref, value, k=4, p=1):
        mask = 0x0f
        lower_bound = compressor.Compressor.lsb_lower(v_ref, p)
        upper_bound = compressor.Compressor.lsb_upper(v_ref, k, p)

        if lower_bound < 0:
            lower_bound += 1 << 32
        if upper_bound < 0:
            upper_bound += 1 << 32

        #print("v_ref " + str(v_ref))
        #print("value " + str(value))
        #print("k " + str(k))

        #print("lower_bound " + str(lower_bound))
        #print("upper_bound " + str(upper_bound))

        if (value >> k) != 0:
            raise ValueError("LSB4 compression failed!")

        if k == 4:
            c_mask = 0x0f
        else:
            raise Exception("??")

        # normal case
        i = 0
        if lower_bound < upper_bound:
            for i in range(v_ref, upper_bound+1):
                #print("i & (~mask) " + str(i & c_mask))
                #print("~mask " + str(c_mask))
                if (i & c_mask) == value:
                    return i;
            for i in range(lower_bound, v_ref+1):
                if (i & c_mask) == value:
                    return i

            raise ValueError("LSB4 compression failed!")
        # overflow case
        else:
            # search first from v_ref
            if v_ref <= upper_bound:
                for i in range(v_ref, upper_bound+1):
                    if (i & (c_mask & 0x0f)) == value:
                        return i
            else:
                i = v_ref
                while i != 0:
                    if (i & c_mask) == value:
                        return i
                    i += 1

            for i in range(0, v_ref+1):
                if (i & c_mask) == value:
                    return i

            for i in range(lower_bound, v_ref+1):
                if (i & c_mask) == value:
                    return i

            raise ValueError("LSB4 compression failed!")