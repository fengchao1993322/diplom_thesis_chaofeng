import scapy.all as scapy
import numpy as np
from sklearn.model_selection import train_test_split
import sys
from binascii import hexlify
import pandas as pd

pkts = scapy.rdpcap("/home/student/Documents/aa_scripter/test0.pcap")

for pktsIndex in range(len(pkts)): 
    pkt = pkts[pktsIndex]
    pktBytes=bytes(pkt)
    if pktsIndex == 0 :
        pktBytearray=bytearray(pktBytes)
    else:
        pktBytearrayAdding=bytearray(pktBytes)
        pktBytearray=np.vstack((pktBytearray,pktBytearrayAdding))



pktsDataframe=pd.DataFrame(pktBytearray)

resultPkts2dArray = np.empty([pktsDataframe.shape[0]-1, pktsDataframe.shape[1]])

for rowIndexOfPktsDataframe in range(pktsDataframe.shape[0]-1):
    for columnIndexOfPktsDataframe in range(pktsDataframe.shape[1]):
        resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe]=(pktsDataframe.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe]-pktsDataframe.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe])


print("substitudeResult:\n", resultPkts2dArray[0])

colFeatureName=["increasing", "declining", "constant"]
pktsBytePosiData=pd.DataFrame(columns=colFeatureName)

for indexOfResultPkts2dArray in range(len(resultPkts2dArray[0])):
    if resultPkts2dArray[0][indexOfResultPkts2dArray] == 1:
        posiData=np.array([1, 0 ,0])
        label="id"
        print("idBytePosition: ", indexOfResultPkts2dArray, ", increasing")
    elif resultPkts2dArray[0][indexOfResultPkts2dArray] == 255:
        posiData=np.array([0, 1 ,0])
        label="chksum"
        print("chksumBytePosition: ", indexOfResultPkts2dArray, ", declining")
    else:
        posiData=np.array([0, 0 ,1])
        label="restBytePosi"
        print("restBytePosition: ", indexOfResultPkts2dArray, ", constant")
    posiDataAppending = pd.DataFrame([posiData], index=[label], columns=colFeatureName)
    pktsBytePosiData = pd.concat([pktsBytePosiData, posiDataAppending], axis=0)

#print(pktsBytePosiData)


##############################################plot 2D############################################## 
# import matplotlib.pyplot as plt
# from matplotlib.colors import ListedColormap


# def plot_decision_regions(X, y, classifier, test_idx=None, resolution=0.02):
#     # setup marker generator and color map
#     markers = ('s', 'x', 'o', '^', 'v')
#     colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
#     cmap = ListedColormap(colors[:len(np.unique(y))])
#     # plot the decision surface
#     x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
#     x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
#     xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution), np.arange(x2_min, x2_max, resolution))
#     Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()  ]).T)
#     Z = Z.reshape(xx1.shape)
#     fig = plt.gcf()
#     fig.set_size_inches(16, 9)
#     plt.contourf(xx1, xx2, Z, alpha=0.4, cmap=cmap)
#     plt.xlim(xx1.min(), xx1.max())
#     plt.ylim(xx2.min(), xx2.max())
#     # plot class samples
#     for idx, cl in enumerate(np.unique(y)):
#         plt.scatter(x=X[y == cl, 0], y=X[y == cl, 1],alpha=0.8, c=cmap(idx),marker=markers[idx], label=cl)
#     # highlight test samples
#     if test_idx:
#         X_test, y_test = X[test_idx, :], y[test_idx]
#     plt.scatter(X_test[:, 0], X_test[:, 1], c='', alpha=1.0, linewidths=1, marker='o',edgecolors='k', s=65, label='test set')


##############################################sklearn logistic regression##############################################
X_train, X_test, y_train, y_test = train_test_split(pktsBytePosiData.astype(float), pktsBytePosiData.index, test_size=0.1, random_state=0) 

#print("X_train: ", X_train)

# optimalize the ML, make it standardlized
from sklearn.preprocessing import StandardScaler 
sc = StandardScaler() 
#Compute the mean and std
sc.fit(X_train) 
X_train_std = sc.transform(X_train) 
X_test_std = sc.transform(X_test) 
#print("X_train_std: ", X_train_std)

X_combined_std = np.vstack((X_train_std, X_test_std))
y_combined = np.hstack((y_train, y_test))

from sklearn.linear_model import LogisticRegression 

lr = LogisticRegression(C=1000.0, random_state=0, class_weight='balanced', solver='liblinear', multi_class= 'auto') 
lr.fit(X_train_std, y_train) 

print('Test accuracy:', lr.score(X_test_std, y_test))
print(lr.classes_)
print(lr.predict_proba(X_test_std)[0,:]) # check the first test sample's probality

# plot_decision_regions(X_combined_std, y_combined, classifier=lr, test_idx=range(10,12)) 
# plt.xlabel('petal length [standardized]') 
# plt.ylabel('petal width [standardized]') 
# plt.legend(loc='upper left') 
# plt.show()