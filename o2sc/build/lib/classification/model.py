from o2sc import pcap

import sys

import sklearn.preprocessing
import sklearn.linear_model
import sklearn.metrics

class BasicModel(pcap.PcapProcessingOracle):

    __classes       = None
    __model         = None
    __classifier    = None

    x_train = None
    x_test  = None
    y_train = None
    y_test  = None

    def __init__(self):
        pass

    def train(self, pcap_path):
        x, y, self.__classes = self.get_preprocessed_data(pcap_path)
        if len(self.__classes) > 1:
            if len(self.__classes) > 2:
                raise Exception("Multi-classification not implemented!")
            self.x_train, self.x_test, self.y_train, self.y_test = self.split(x, y, 0.5)

            self.__model = sklearn.linear_model.LogisticRegression(
                C=1.0, class_weight='balanced', solver='liblinear', multi_class='ovr')
            self.__classifier = self.__model.fit(self.x_train, self.y_train)

    def score(self):
        try:
            if len(self.__classes) > 1:
                print("F1 = " + str(
                    sklearn.metrics.f1_score(self.y_test,
                    self.__classifier.predict(self.x_test))))
            else:
                print("Model not scoreable!")
        except:
            raise Exception("Model not trained!")

    def predict(self, packet):
        try:
            if len(self.__classes) == 1:
                return self.__classes[0]
            else:
                return self.__classes[int(self.__classifier.predict([packet]))]
        except:
            raise Exception("Model not trained!")