import pandas as pd
import csv 
import matplotlib.pyplot as plt

# from . import summaryFunc as sFunc 
# from . import synthesizedPlot as sPlot
import summaryFunc as sFunc
import synthesizedPlot as sPlot

csv_read = csv.reader(open('acc.csv', 'r+'))
acc_list=list()
for line in csv_read:
    acc_list.append(line)


size = 8
n = len(acc_list)
namelist = ['LogisticR','L-SVC','SVC','D-Tree', 'R-Forest','GaussianNB','KNeighbors', 'NN']
objectName= ['packets','bytes']
acc0 = list(map(float, acc_list[0]))
acc1 = list(map(float, acc_list[1]))

plt.figure(figsize=(10, 8))
sPlot.plot_parallel_histogram(size, n, namelist,objectName,acc0,acc1)
plt.show()
plt.pause(3)
plt.close()


