'''
    data format like common previous lR classification, used the whole packet byte as label 
'''
import sys
from binascii import hexlify

import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression

import summaryFunc as sFunc


loadingPath = "/home/student/Documents/aa_scripter/test3.pcap"

pktsDataFloat = sFunc.readPcap(loadingPath)
resultPkts2dArray = sFunc.byteLabelPreOperation(pktsDataFloat)
pktsDataFloat.index = sFunc.pktLabelGeneration(resultPkts2dArray)

###################################################################################
pktsDataframe_std, _ = sFunc.dataStandardization(pktsDataFloat, pktsDataFloat)

###################################################################################
#########################dynamical quantity of train set###########################
###################################################################################
lr = LogisticRegression(C=100000.0, random_state=0, class_weight= 'balanced', solver='liblinear', multi_class= 'auto') 
stepLength = 10
for quantityOfTrain in range(10, len(pktsDataFloat)+1, stepLength):
    lr.fit(pktsDataframe_std[0:quantityOfTrain], pktsDataFloat.index[0:quantityOfTrain]) 
    '''
    check sample's probality, 
                    ['01000300203300000000' '10010300113311101111']
    example index:    rest                   0                     
    '''
    if quantityOfTrain>=len(pktsDataFloat):
        continue

    print('Test accuracy:', lr.score(pktsDataframe_std[quantityOfTrain:quantityOfTrain+stepLength,:], pktsDataFloat.index[quantityOfTrain:quantityOfTrain+stepLength]))    
    print("Train set has total %d packages, predict the next %d packages' probality after train set: "%(quantityOfTrain,stepLength))
    print(lr.classes_)
    for i in range(stepLength):
        print(lr.predict_proba(pktsDataframe_std[quantityOfTrain:quantityOfTrain+stepLength,:])[i], 'predict: ', lr.predict(pktsDataframe_std[quantityOfTrain:quantityOfTrain+stepLength])[i], "true label: ", pktsDataFloat.index[quantityOfTrain+i])
    
