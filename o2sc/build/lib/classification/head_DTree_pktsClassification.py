import sys
from binascii import hexlify

import numpy as np
import pandas as pd
from sklearn import tree

import summaryFunc as sFunc

loadingPath = "/home/student/Documents/aa_scripter/test3.pcap"
pktsDataFloat = sFunc.readPcap(loadingPath)
resultPkts2dArray = sFunc.byteLabelPreOperation(pktsDataFloat)
pktsDataFloat.index = sFunc.pktLabelGeneration(resultPkts2dArray)

###################################################################################
osm = sFunc.overSamplingMethod(methodChosen='naiveRandom')
X_sampled, y_sampled = osm.dataInput(pktsDataFloat, pktsDataFloat.index)
testSize = 0.2
X_train, X_test, y_train, y_test = sFunc.trainTestSeparate(X_sampled, y_sampled, testSize)

X_train_std, X_test_std = sFunc.dataStandardization(X_train, X_test)
_, pktsDataframe_std = sFunc.dataStandardization(X_train, pktsDataFloat)

clf = tree.DecisionTreeClassifier(class_weight='balanced', criterion='entropy')
clf.fit(X_train, y_train)


print('Test accuracy:', clf.score(X_test_std, y_test))
print(clf.classes_)
'''
check sample's probality, 
                ['01000300203300000000' '10010300113311101111']
example index:    rest                   0                     
'''
print('pktsDataframe_Std[0,:]: ', clf.predict(pktsDataframe_std)[0])

# print('pktsDataframe_Std[1,:]: ', clf.predict(pktsDataframe_std)[1,:])
# print('pktsDataframe_Std[2,:]: ', clf.predict(pktsDataframe_std)[2,:])
# print('pktsDataframe_Std[3,:]: ', clf.predict(pktsDataframe_std)[3,:])
# print('pktsDataframe_Std[4,:]: ', clf.predict(pktsDataframe_std)[4,:])
# print('pktsDataframe_Std[191,:]: ', clf.predict(pktsDataframe_std)[191,:])
# print('pktsDataframe_Std[194,:]: ', clf.predict(pktsDataframe_std)[194,:])

for i in range(len(X_test)):
    print('predict: ', clf.predict(X_test_std)[i], "true label: ", y_test[i])

