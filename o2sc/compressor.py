import math

# patter change, static advancment
class Compressor:
    FLAG_LONG_PATTERN  = 0x01
    # random
    FLAG_SEQ_PKT_0     = 0x80
    # lsb
    FLAG_SEQ_PKT_1     = 0x40
    # delta
    FLAG_SEQ_PKT_2     = 0xc0

    PATTERN_MODE_NON_STATIC     = 0
    PATTERN_MODE_STATIC         = 1
    PATTERN_MODE_COMPRESSION_OFF= 0
    PATTERN_MODE_COMPRESSION_ON = 1

    PATTERN_STATIC     = 0
    PATTERN_INCREASING = 1
    PATTERN_DECREASING = 2
    PATTERN_RANDOM     = 3
    PATTERN_CO_STATIC  = 0
    PATTERN_CO_SEQ     = 1
    PATTERN_CO_RANDOM  = 2
    PATTERN_CO_INFERED = 3
    __stored_pattern   = None

    __context_current  = []
    __context_previous = []
    __context_deltas   = []

    STATE_NONE         = -1
    STATE_NON_SEQ      = 0
    STATE_SEQ          = 1
    #STATE_SEQ_DELTA    = 2
    __state            = STATE_NONE

    __uc_packet       = []
    __co_packet       = []

    __bytes_received  = 0
    __bytes_sent      = 0
    __packets_sent    = 0

    is_pattern_changed   = False
    is_pattern_violated  = False

    __static_compression = False

    debug_info_on     = False

    def __init__(self, pattern=None, static_compression=False):
        if pattern != None:
            pattern_changed, converted_pattern = self.__interpret_pattern(pattern)
            self.__stored_pattern = converted_pattern

        self.__context_current  = []
        self.__context_previous = []
        self.__context_deltas   = []

        self.__state            = self.STATE_NONE

        self.__uc_packet       = []
        self.__co_packet       = []

        self.__bytes_received  = 0
        self.__bytes_sent      = 0
        self.__packets_sent    = 0

        self.is_pattern_changed  = False
        self.is_pattern_violated = False
        self.debug_info_on       = False

        self.__static_compression = static_compression


    def __interpret_pattern(self, str_pattern):
        pattern = [int(i) for i in str_pattern]

        tmp_pattern     = []
        pattern_changed = False
        # conver pattern indices
        for i in range(len(pattern)):
            # static compression enabled
            if self.__static_compression == True:
                if (pattern[i] == self.PATTERN_DECREASING
                    or pattern[i] == self.PATTERN_INCREASING
                    or pattern[i] == self.PATTERN_RANDOM):
                    tmp_pattern.append(self.PATTERN_CO_RANDOM)
                else:
                    tmp_pattern.append(self.PATTERN_CO_STATIC)
            else:
                if pattern[i] == self.PATTERN_DECREASING:
                    tmp_pattern.append(self.PATTERN_CO_SEQ)
                elif pattern[i] == self.PATTERN_RANDOM:
                    tmp_pattern.append(self.PATTERN_CO_RANDOM)
                else:
                    tmp_pattern.append(pattern[i])

        # if this is the first pattern
        #if self.__stored_pattern == None:
        #    self.__stored_pattern = tmp_pattern.copy()

        # compare patterns
        if self.__stored_pattern != None:
            if len(tmp_pattern) != len(self.__stored_pattern):
                raise ValueError("Non-matching pattern lengths!")
            for i in range(len(self.__stored_pattern)):
                if self.__stored_pattern[i] != tmp_pattern[i]:
                    if self.debug_info_on:
                        print(str(self.__stored_pattern[i]) + " vs. " + str(tmp_pattern[i]))
                    pattern_changed = True
                    break

        return pattern_changed, tmp_pattern


    def compress(self, packet, pattern=None):
        if (pattern == None) and (self.__stored_pattern == None):
            raise ValueError("No pattern provided!")

        self.__uc_packet = packet
        self.__co_packet = []
        self.__bytes_received += len(packet)

        self.is_pattern_changed  = False
        self.is_pattern_violated = False

        pattern_changed = False
        if pattern != None:
            pattern_changed, converted_pattern = self.__interpret_pattern(pattern)
            self.__stored_pattern = converted_pattern

        if len(self.__stored_pattern) !=  len(self.__uc_packet):
            raise ValueError("Different packet sizes are not handled! (" + str(len(self.__stored_pattern))
                             + " vs. " + str(len(self.__uc_packet)) +")")

        # read packet
        tmp = self.__context_previous
        self.__context_previous = self.__context_current
        self.__context_current = tmp
        self.__context_current = []
        for i in range(len(self.__stored_pattern)):
            self.__context_current.append(self.__uc_packet[i])

        #print(self.__stored_pattern)

        # manage state machine
        # revert to non_seq at pattern change
        if pattern_changed:
            if self.debug_info_on:
                print("Pattern changed!")
                print("State change: * -> NON_SEQ")
                print("TODO")
            self.is_pattern_changed = True
            self.__state = self.STATE_NON_SEQ
        # go to non_seq for first packet
        elif self.__state == self.STATE_NONE:
            if self.debug_info_on:
                print("State change: NONE -> NON_SEQ")
            self.__state = self.STATE_NON_SEQ
        # go to seq if statics didn't change
        elif (self.__state == self.STATE_NON_SEQ) and (self.__assert_statics() == True):
            if self.debug_info_on:
                print("State change: NON_SEQ -> SEQ")
            self.__state = self.STATE_SEQ
        # go to back to non_seq if statics changed
        elif self.__assert_statics() == False:
            if self.debug_info_on:
                print("Pattern violation detected!")
                self.is_pattern_violated = True
                if self.__state != self.STATE_NON_SEQ:
                    print("State change: SEQ -> NON_SEQ")
            self.__state = self.STATE_NON_SEQ
        #elif self.__state == self.STATE_SEQ:
        #    if self.debug_info_on:
        #        print("State change: SEQ -> SEQ_DELTA")
        #    self.__state = self.STATE_SEQ_DELTA
        #    #self.__state = self.STATE_SEQ
        # default?
        #else:
            #if self.debug_info_on and (self.__state != self.STATE_SEQ):
            #    print("State change: NON_SEQ -> SEQ")
            #self.__state = self.STATE_SEQ
            #raise Exception("State transition error!")

        # build compressed packets
        if self.__state == self.STATE_NONE:
            raise Exception("There is no available compressor context!")
        # non-seq packet
        elif self.__state == self.STATE_NON_SEQ:
            if self.debug_info_on:
                print("Compressing NON_SEQ packet.")
            self.__assemble_non_seq_pkt()
        # seq packet
        elif self.__state == self.STATE_SEQ:
            if self.__check_delta_all() == False:
                self.__assemble_seq_pkt_2()
                #if self.debug_info_on:
                #    print("Compressing SEQ_2 packet.")
            elif self.__check_lsb4_all():
                if self.debug_info_on:
                    print("Deltas changed!")
                    print("Compressing SEQ_1 packet.")
                self.__assemble_seq_pkt_1()
            else:
                if self.debug_info_on:
                    print("Deltas changed & LSB4 test failed!")
                    print("Compressing SEQ_0 packet.")
                self.__assemble_seq_pkt_0()
        #elif self.__state == self.STATE_SEQ_DELTA:
        #    if self.__check_delta_all() == False:
        #        #if self.debug_info_on:
        #        #    print("Compressing SEQ_2 packet.")
        #            self.__assemble_seq_pkt_2()
        #    elif self.__check_lsb4_all():
        #        if self.debug_info_on:
        #            print("Deltas changed!")
        #            print("Compressing SEQ_1 packet.")
        #        self.__assemble_seq_pkt_1()
        #    else:
        #        if self.debug_info_on:
        #            print("Deltas changed & LSB4 test failed!")
        #            print("Compressing SEQ_0 packet.")
        #        self.__assemble_seq_pkt_0()
        else:
            raise ValueError("Unknown compressor state!")

        self.__bytes_sent   += len(self.__co_packet)
        self.__packets_sent += 1

        return self.__co_packet

    def print_contexts(self):
        print("Previous " + str(self.__context_previous))
        print("Current " + str(self.__context_current))

    def reset_gain(self):
        self.__bytes_sent       = 0
        self.__bytes_received   = 0

    def get_gain(self):
        return (1.0 - self.__bytes_sent / self.__bytes_received,
                self.__bytes_sent / self.__packets_sent, self.__bytes_received / self.__packets_sent)

    def __assert_statics(self):
        for i in range(len(self.__stored_pattern)):
            if self.__stored_pattern[i] == self.PATTERN_CO_STATIC:
                if self.__context_previous[i] != self.__context_current[i]:
                    return False
        return True

    def __compress_pattern(self):
        if self.debug_info_on:
            print("Pattern: " + str(self.__stored_pattern))

        # compress pattern
        #print(self.__stored_pattern)
        pattern = []
        p = 0x00
        i = 0
        # add static mode
        if self.__static_compression == True:
            offset = 2
            p = 0x02
            while i < len(self.__stored_pattern):
                # static
                if self.__stored_pattern[i] == self.PATTERN_CO_STATIC:
                    p |= 0x0
                # random
                elif self.__stored_pattern[i] == self.PATTERN_CO_RANDOM:
                    p |= 0x1
                else:
                    raise ValueError("Unknown pattern encountered!")
                #print("+" + str(hex(p)))

                if (offset + 1) % 8 == 0:
                    pattern.append(p)
                    p = 0x00
                else:
                    p <<= 1
                offset += 1
                i += 1

            if (offset + 1) % 8 != 0:
                p <<= (8 - ((offset + 1) % 8))
                pattern.append(p)
                p = 0x00
        else:
            offset = 1
            while i < len(self.__stored_pattern):
                #print("i " + str(i))
                #print("New iteration: " + str(hex(p)))
                # static
                if self.__stored_pattern[i] == self.PATTERN_CO_STATIC:
                    p |= 0x00
                # inc/dec
                elif self.__stored_pattern[i] == self.PATTERN_CO_SEQ:
                    p |= 0x01
                # random
                elif self.__stored_pattern[i] == self.PATTERN_CO_RANDOM:
                    p |= 0x02
                else:
                    raise ValueError("Unknown pattern encountered!")
                #print("+" + str(hex(p)))

                if (offset + 1) % 4 == 0:
                    pattern.append(p)
                    #print("Comitted: " + str(hex(p)))
                    p = 0x00
                else:
                    p <<= 2
                offset += 1

                # check repetition
                repetition = 0
                for r in range(i, len(self.__stored_pattern)):
                    if self.__stored_pattern[i] == self.__stored_pattern[r]:
                        repetition += 1
                    else:
                        break
                repetition -= 1

                if repetition > 4:
                    if self.debug_info_on:
                        print("Pattern repetition: " + str(repetition))
                    r_left = repetition
                    for k in range(0, math.ceil(repetition / 32)):
                        length = r_left if r_left <= 32 else 32
                        r_left -= length
                        #print(r_left)
                        # flag + reserved + length
                        byte = 0xc0 | (length-1)
                        #print(byte)

                        for o in range(0, 4):
                            #print("Before: " + str(p))
                            bits = (3 - o) * 2
                            pat_code = (byte >> bits) & 0x03
                            #print(pat_code)
                            p |= pat_code

                            #print("Between: " + str(p))
                            if (offset + o + 1) % 4 == 0:
                                #print("Comitted: " + str(hex(p)))
                                pattern.append(p)
                                p = 0x00
                            else:
                                p <<= 2
                            #print("After: " + str(p))
                    #print("a " + str(i))
                    i += repetition
                    offset += 4
                    #print("b " + str(i))

                if i == (len(self.__stored_pattern) - 1):
                    #print("1---" + str(p))
                    p <<= (4 - ((offset + 1) % 4)) * 2
                    #print(p)
                    #print("<< " + str((4 - ((i+1) % 4)) * 2))
                    pattern.append(p)
                    p = 0x00
                i += 1

        if self.debug_info_on:
            print("Compressed pattern: " + str(pattern))

        return pattern

    def __compress_static(self):
        buffer = []
        #c = 0
        for i in range(len(self.__stored_pattern)):
            if self.__stored_pattern[i] == self.PATTERN_CO_STATIC:
                #c += 1
                buffer.append(self.__uc_packet[i])
        #print("Static " + str(c))
        return buffer

    def __compress_sequential_as_random(self):
        buffer = []
        #c = 0
        for i in range(len(self.__stored_pattern)):
            if self.__stored_pattern[i] == self.PATTERN_CO_SEQ:
                #c += 1
                buffer.append(self.__context_current[i])
        #print("Seq " + str(c))
        return buffer

    def __check_lsb4_all(self):
        for i in range(len(self.__stored_pattern)):
            if self.__stored_pattern[i] == self.PATTERN_CO_SEQ:
                r_lsb = self.__lsb2_test(self.__context_previous[i], self.__context_current[i], 4, 1)
                if r_lsb == False:
                    if self.debug_info_on:
                        #print("LSB4 test failed!")
                        print("LSB Field " + str(i) + ": " + str(r_lsb))
                    return False
        return True

    def __check_delta_all(self):
        deltas_changed = False
        for i in range(len(self.__stored_pattern)):
            if self.__stored_pattern[i] == self.PATTERN_CO_SEQ:
                delta = self.__context_current[i] - self.__context_previous[i]
                # check for overflow
                #if ((self.__context_previous[i] + self.__context_deltas[i] > 255)
                #    and (((self.__context_previous[i] + self.__context_deltas[i]) % 256) == self.__context_current[i])):
                #    continue
                #el
                if delta != self.__context_deltas[i]:
                    if self.debug_info_on:
                        print("Delta " + str(delta) + " != " + str(self.__context_deltas[i]))
                    deltas_changed = True
                    self.__context_deltas[i] = delta
        return deltas_changed

    def __compress_sequential(self):
        buffer = []
        for i in range(len(self.__stored_pattern)):
            if self.__stored_pattern[i] == self.PATTERN_CO_SEQ:
                #r_lsb = self.__lsb2_test(self.__context_previous[i], self.__context_current[i], 4, 1)
                #if self.debug_info_on:
                #    print("LSB Field " + str(i) + ": " + str(r_lsb))
                buffer.append(self.__context_current[i])
        return buffer

    def __compress_sequential_lsb4(self):
        buffer = []
        current_byte = 0
        is_upper = True
        for i in range(len(self.__stored_pattern)):
            if self.__stored_pattern[i] == self.PATTERN_CO_SEQ:
                lsb = Compressor.__compress_lsb4(self.__context_previous[i], self.__context_current[i])
                if is_upper:
                    current_byte = lsb << 4
                    is_upper     = False
                else:
                    current_byte |= lsb
                    buffer.append(current_byte)
                    current_byte = 0
                    is_upper     = True
        if is_upper == False:
            buffer.append(current_byte)
        return buffer

    def __compress_random(self):
        buffer = []
        #c = 0
        for i in range(len(self.__stored_pattern)):
            if self.__stored_pattern[i] == self.PATTERN_CO_RANDOM:
                #c += 1
                buffer.append(self.__context_current[i])
        #print("Rnd " + str(c))
        return buffer

    def __assemble_non_seq_pkt(self):
        # flags
        flags = 0x00
        # reserve flags byte
        self.__co_packet.append(0x00)
        # pattern
        pattern = self.__compress_pattern()
        #print(len(pattern))
        if len(pattern) > 256:
            flags |= self.FLAG_LONG_PATTERN
            # add length upper
            self.__co_packet.append(len(pattern) >> 8)
        self.__co_packet.append(len(pattern) & 0xff)

        # fill in flags
        self.__co_packet[0] = flags
        #print(self.__co_packet)

        for k in pattern:
            self.__co_packet.append(k)

        # setup deltas
        for i in self.__stored_pattern:
            self.__context_deltas.append(0x00)

        # static
        for k in self.__compress_static():
            self.__co_packet.append(k)
        # sequential
        for k in self.__compress_sequential_as_random():
            self.__co_packet.append(k)
        # random
        for k in self.__compress_random():
            self.__co_packet.append(k)

    # sequential fields are sent as random
    def __assemble_seq_pkt_0(self):
        # flags
        flags = self.FLAG_SEQ_PKT_0
        # reserve flags byte
        self.__co_packet.append(flags)

        # sequential
        for k in self.__compress_sequential_as_random():
            self.__co_packet.append(k)
        # random
        for k in self.__compress_random():
            self.__co_packet.append(k)

    # all sequential fields are sent as lsb4
    def __assemble_seq_pkt_1(self):
        # flags
        flags = self.FLAG_SEQ_PKT_1
        # reserve flags byte
        self.__co_packet.append(flags)

        # sequential
        for k in self.__compress_sequential_lsb4():
            self.__co_packet.append(k)
        # random
        for k in self.__compress_random():
            self.__co_packet.append(k)

    # all sequential fields are assumed to progress with deltas
    def __assemble_seq_pkt_2(self):
        # flags
        flags = self.FLAG_SEQ_PKT_2
        # reserve flags byte
        self.__co_packet.append(flags)

        # sequential
        # omitted
        # random
        for k in self.__compress_random():
            self.__co_packet.append(k)

    def lsb_lower(v_ref, p):
        return v_ref - p
    def lsb_upper(v_ref, k, p):
        return v_ref + ((1 << k) - 1) - p

    def lsb_out_of_bounds(decodedVal, lowerBound, upperBound):
        return True if (decodedVal >= lowerBound) and (decodedVal <= upperBound) else False

    def __lsb2_test(self, v_ref, value, k, p):
        lower_bound = Compressor.lsb_lower(v_ref, p)
        upper_bound = Compressor.lsb_upper(v_ref, k, p)

        if lower_bound < 0:
            lower_bound += 1 << 32
        if upper_bound < 0:
            upper_bound += 1 << 32

        #print("+v_ref " + str(v_ref))
        #print("+value " + str(value))
        #print("+k " + str(k))

        #print("+lower_bound " + str(lower_bound))
        #print("+upper_bound " + str(upper_bound))

        # overflow
        if lower_bound > upper_bound:
            #print(1)
            # 0 <= x <= lower_bound or upper_bound <= x <
            return (value <= upper_bound) or ((value >= lower_bound) and (value < 0xffffffff))
        else:
            #print(2)
            return (value >= lower_bound) and (value <= upper_bound)

    def __compress_lsb4(v_ref, value, p=1):
        return value & Compressor.__lsb4_mask(v_ref, value)

    def __lsb4_mask(v_ref, value, p=1):
        i_min = 0
        i_max = 0
        k = 1

        return 0x0f

        """
        while True:
            if k == 4:
                return 0x0f

            i_min = Compressor.lsb_lower(v_ref, p)
            i_max = Compressor.lsb_upper(v_ref, k, p)

            if i_min < 0:
                i_min += 1 << 32
            if i_max < 0:
                i_max += 1 << 32

            if i_min <= i_max:
                if (i_min <= value) and (value <= i_max):
                    return (1 << k) - 1
            else:
                if (value <= i_max) and (i_min <= value):
                    return (1 << k) - 1
            k += 1
        """