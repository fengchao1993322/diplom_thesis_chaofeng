"""
                            11111111111111111111111111111111111111001111111111111111111111111
                            11111111111111111111111111111111111100011111111111111111111111111
                            11111111111111111111111111111111100001111111111111111111111111111
                            11111111111111111111111111111110000111111111111111111111111111111
                            11111111111111111111111111111000000111111111111111111111111111111
                            11111111111111111111111111100000011110001100000000000000011111111
                            11111111111111111100000000000000000000000000000000011111111111111
                            11111111111111110111000000000000000000000000000011111111111111111
                            11111111111111111111111000000000000000000000000000000000111111111
                            11111111111111111110000000000000000000000000000000111111111111111
                            11111111111111111100011100000000000000000000000000000111111111111
                            11111111111111100000110000000000011000000000000000000011111111111
                            11111111111111000000000000000100111100000000000001100000111111111
                            11111111110000000000000000001110111110000000000000111000011111111
                            11111111000000000000000000011111111100000000000000011110001111111
                            11111110000000011111111111111111111100000000000000001111100111111
                            11111111000001111111111111111111110000000000000000001111111111111
                            11111111110111111111111111111100000000000000000000000111111111111
                            11111111111111110000000000000000000000000000000000000111111111111
                            11111111111111111100000000000000000000000000001100000111111111111
                            11111111111111000000000000000000000000000000111100000111111111111
                            11111111111000000000000000000000000000000001111110000111111111111
                            11111111100000000000000000000000000000001111111110000111111111111
                            11111110000000000000000000000000000000111111111110000111111111111
                            11111100000000000000000001110000001111111111111110001111111111111
                            11111000000000000000011111111111111111111111111110011111111111111
                            11110000000000000001111111111111111100111111111111111111111111111
                            11100000000000000011111111111111111111100001111111111111111111111
                            11100000000001000111111111111111111111111000001111111111111111111
                            11000000000001100111111111111111111111111110000000111111111111111
                            11000000000000111011111111111100011111000011100000001111111111111
                            11000000000000011111111111111111000111110000000000000011111111111
                            11000000000000000011111111111111000000000000000000000000111111111
                            11001000000000000000001111111110000000000000000000000000001111111
                            11100110000000000001111111110000000000000000111000000000000111111
                            11110110000000000000000000000000000000000111111111110000000011111
                            11111110000000000000000000000000000000001111111111111100000001111
                            11111110000010000000000000000001100000000111011111111110000001111
                            11111111000111110000000000000111110000000000111111111110110000111
                            11111110001111111100010000000001111100000111111111111111110000111
                            11111110001111111111111110000000111111100000000111111111111000111
                            11111111001111111111111111111000000111111111111111111111111100011
                            11111111101111111111111111111110000111111111111111111111111001111
                            11111111111111111111111111111110001111111111111111111111100111111
                            11111111111111111111111111111111001111111111111111111111001111111
                            11111111111111111111111111111111100111111111111111111111111111111
                            11111111111111111111111111111111110111111111111111111111111111111


                                    CODE is far away from bug with Dragon protecting

"""

import scapy.all as scapy
import numpy as np
from sklearn.model_selection import train_test_split
import sys
import pandas as pd

from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn import tree, svm, neighbors
#from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.multiclass import OneVsRestClassifier, OneVsOneClassifier
from sklearn.preprocessing import label_binarize

from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import average_precision_score
from sklearn.metrics import f1_score

from imblearn.over_sampling import RandomOverSampler, SMOTE, ADASYN, BorderlineSMOTE


from collections import Counter
import warnings


def readPcap(path):
    pkts = scapy.rdpcap(path)

    for pktsIndex in range(len(pkts)):
        pkt = pkts[pktsIndex]
        pktBytes = bytes(pkt)
        if pktsIndex == 0:
            pktBytearray = bytearray(pktBytes)
        else:
            pktBytearrayAdding = bytearray(pktBytes)
            pktBytearray = np.vstack((pktBytearray, pktBytearrayAdding))

    pktsDataframe = pd.DataFrame(pktBytearray)
    pktsDataframeFloat = pktsDataframe.astype(float)
    return pktsDataframeFloat


def byteLabelPreOperation(pktsDataframe):
    resultPkts2dArray = np.empty([len(pktsDataframe), pktsDataframe.shape[1]])
    '''
    In the package data preparation,
    0 means static;
    1 means increment;
    2 means decrement;
    3 means random.
    '''
    for columnIndexOfPktsDataframe in range(pktsDataframe.shape[1]):
        NumOfIncr = 0
        NumOfDecr = 0
        NumOfCons = 0
        Incr = False
        Decr = False
        Cons = False
        # 'changingNum' means the byte info changing times, except the first packet.
        changingNum = 0
        for rowIndexOfPktsDataframe in range(len(pktsDataframe)):
            if rowIndexOfPktsDataframe == 0:
                pass
            else:
                if pktsDataframe.iat[
                        rowIndexOfPktsDataframe,
                        columnIndexOfPktsDataframe] == pktsDataframe.iat[
                            rowIndexOfPktsDataframe -
                            1, columnIndexOfPktsDataframe]:
                    if rowIndexOfPktsDataframe ==1:
                        resultPkts2dArray[rowIndexOfPktsDataframe-1,
                                      columnIndexOfPktsDataframe] = 0
                        NumOfCons += 1
                        Cons = True
                    resultPkts2dArray[rowIndexOfPktsDataframe,
                                      columnIndexOfPktsDataframe] = 0
                    NumOfCons += 1
                    Cons = True
                elif pktsDataframe.iat[
                        rowIndexOfPktsDataframe,
                        columnIndexOfPktsDataframe] > pktsDataframe.iat[
                            rowIndexOfPktsDataframe -
                            1, columnIndexOfPktsDataframe]:
                    if ((pktsDataframe.iat[rowIndexOfPktsDataframe,
                                           columnIndexOfPktsDataframe] == 255)
                            or
                        (pktsDataframe.iat[rowIndexOfPktsDataframe,
                                           columnIndexOfPktsDataframe] == 254)
                        ) and (pktsDataframe.iat[rowIndexOfPktsDataframe -
                                                 1, columnIndexOfPktsDataframe]
                               == 0):
                        if rowIndexOfPktsDataframe == 1:
                            resultPkts2dArray[rowIndexOfPktsDataframe-1,
                                          columnIndexOfPktsDataframe] = 2
                            NumOfDecr += 1
                            Decr = True
                        resultPkts2dArray[rowIndexOfPktsDataframe,
                                          columnIndexOfPktsDataframe] = 2
                        NumOfDecr += 1
                        Decr = True
                    else:
                        if rowIndexOfPktsDataframe == 1:
                            resultPkts2dArray[rowIndexOfPktsDataframe-1,
                                          columnIndexOfPktsDataframe] = 1
                            NumOfIncr += 1
                            Incr = True
                        resultPkts2dArray[rowIndexOfPktsDataframe,
                                          columnIndexOfPktsDataframe] = 1
                        NumOfIncr += 1
                        Incr = True
                else:
                    if (pktsDataframe.iat[rowIndexOfPktsDataframe,
                                          columnIndexOfPktsDataframe] == 0
                        ) and ((pktsDataframe.
                                iat[rowIndexOfPktsDataframe -
                                    1, columnIndexOfPktsDataframe] == 255) or
                               (pktsDataframe.
                                iat[rowIndexOfPktsDataframe -
                                    1, columnIndexOfPktsDataframe] == 254)):
                        if rowIndexOfPktsDataframe == 1:
                            resultPkts2dArray[rowIndexOfPktsDataframe-1,
                                          columnIndexOfPktsDataframe] = 1
                            NumOfIncr += 1
                            Incr = True
                        resultPkts2dArray[rowIndexOfPktsDataframe,
                                          columnIndexOfPktsDataframe] = 1
                        NumOfIncr += 1
                        Incr = True
                    else:
                        if rowIndexOfPktsDataframe == 1:
                            resultPkts2dArray[rowIndexOfPktsDataframe-1,
                                          columnIndexOfPktsDataframe] = 2
                            NumOfDecr += 1
                            Decr = True
                        resultPkts2dArray[rowIndexOfPktsDataframe,
                                          columnIndexOfPktsDataframe] = 2
                        NumOfDecr += 1
                        Decr = True

            if rowIndexOfPktsDataframe == 1:
                if resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe] == 0:
                    posiState = 0
                elif resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe] == 1:
                    posiState = 1
                elif resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe] == 2:
                    posiState = 2

            if rowIndexOfPktsDataframe > 1:
                if resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe] == posiState:
                    pass
                else:
                    posiState = resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe]
                    changingNum +=1
        '''
            'changingNum<=2' limits the continuity, if changingNum is big, then the byte position random
        '''
        if ((Incr and Decr) or (Incr and Cons) or (Cons and Decr))and(changingNum > 2):
            resultPkts2dArray[:, columnIndexOfPktsDataframe] = 3
            incrPercentage = float(NumOfIncr) / len(pktsDataframe)
            decrPercentage = float(NumOfDecr) / len(pktsDataframe)
            consPercentage = float(NumOfCons) / len(pktsDataframe)
            # print("random column's Index : ", columnIndexOfPktsDataframe,
            #       '; Total quantity of this column elements: ',
            #       len(pktsDataframe))
            # print('posiStateChangingTimes: ', changingNum)
            # print('Increment number: ', NumOfIncr, '; Percentage: ',
            #       incrPercentage)
            # print('Decrement number: ', NumOfDecr, '; Percentage: ',
            #       decrPercentage)
            # print('Constant number: ', NumOfCons, '; Percentage: ',
            #       consPercentage)

    # print("result Pkts 2d Array: ", resultPkts2dArray)
    return resultPkts2dArray

def addingPktsIndex(dataFrame, labelName, startIndex = 0,
                    insertingPosition = 0):
    posiList= range(startIndex, startIndex+len(dataFrame))
    newDataFrame = dataFrame.copy()
    newDataFrame.insert(insertingPosition, labelName, posiList)
    return newDataFrame.astype(float)

def pktLabelGeneration(resultPkts2dArray):
    indexArrayOfPktsDataframe = ["" for x in range(len(resultPkts2dArray))]
    #["" for x in range(len(pktsDataFloat))]
    #np.empty(len(pktsDataFloat), dtype = str)

    resultPkts2dArrayInt = resultPkts2dArray.astype(int)
    #print(resultPkts2dArrayInt)
    for i in range(resultPkts2dArrayInt.shape[0]):
        resultPkts2dArrayStr = [
            str(elemt) for elemt in resultPkts2dArrayInt[i]
        ]
        indexArrayOfPktsDataframe[i] = "".join(resultPkts2dArrayStr)

    return indexArrayOfPktsDataframe

def labels2Num(labels, arrayInput):

    labelsToNum = dict()
    for i, item in enumerate(labels):
        labelsToNum[item] = float(i)
    #print('labels2Num dict: ', labelsToNum)
    NUM_list = list()
    for j, j_item in enumerate(arrayInput):
        NUM_list.append(labelsToNum[j_item])
    array=np.array(NUM_list)
    return array

def trainTestSeparate(X, y, test_size, random_state=None):
	X_train, X_test, y_train, y_test = train_test_split(
		X, y, test_size=test_size, random_state=random_state)
	return X_train, X_test, y_train, y_test


def dataStandardization(X0, X1):
    '''
    from sklearn.preprocessing import StandardScaler

    data standardlized, compute the mean and standard deviation on a training set(X0)
    so as to be able to later reapply the same transformation on the testing set(X1).
    '''
    sc = StandardScaler()
    sc.fit(X0)
    X0_std = sc.transform(X0)
    X1_std = sc.transform(X1)
    return X0_std, X1_std


class overSamplingMethod():
    """over Sampling methods.

    Parameters
    ----------
    methodChosen:

    1. keyword 'naiveRandom' indicates Naive random over-sampling.
    2. keyword 'SMOTE' indicates SMOTE over-sampling.
    3. keyword 'ADASYN' indicates ADASYN over-sampling.
    4. keyword 'BorderlineSMOTE' indicates BorderlineSMOTE over-sampling.
    """

    def __init__(self, methodChosen='naiveRandom'):
        self.methodChosen = methodChosen

    def dataInput(self, X, y):
        if (self.methodChosen == 'naiveRandom'):
            return self.overSampNaiveRandom(X, y)
        elif (self.methodChosen == 'SMOTE'):
            return self.overSampSMOTE(X, y)
        elif (self.methodChosen == 'ADASYN'):
            return self.overSampADASYN(X, y)
        elif (self.methodChosen == 'BorderlineSMOTE'):
            return self.overSampBorderlineSMOTE(X, y)
        else:
            warnings.warn('Wrong chose, no such over sampling method!')
            sys.exit(0)

    def overSampNaiveRandom(self, X, y):
        X_resampled, y_resampled = RandomOverSampler().fit_resample(X, y)
        #print('original data: ', sorted(Counter(y).items()))
        #print('data over sampling by naive random: ',
        #      sorted(Counter(y_resampled).items()))
        return X_resampled, y_resampled

    def overSampSMOTE(self, X, y):
        X_resampled, y_resampled = SMOTE().fit_resample(X, y)
        #print('original data: ', sorted(Counter(y).items()))
        #print('data over sampling by SMOTE: ',
        #      sorted(Counter(y_resampled).items()))
        return X_resampled, y_resampled

    def overSampADASYN(self, X, y):
        X_resampled, y_resampled = ADASYN().fit_resample(X, y)
        #print('original data: ', sorted(Counter(y).items()))
        #print('data over sampling by ADASYN: ',
        #      sorted(Counter(y_resampled).items()))
        return X_resampled, y_resampled

    def overSampBorderlineSMOTE(self, X, y):
        X_resampled, y_resampled = BorderlineSMOTE().fit_resample(X, y)
        #print('original data: ', sorted(Counter(y).items()))
        #print('data over sampling by BorderlineSMOTE: ',
        #      sorted(Counter(y_resampled).items()))
        return X_resampled, y_resampled

def outputLabel(y):
    return sorted(Counter(y))

class classifierAndItsParameters():
    """ensure machine learning classification method and its corresponding parameters.

    Parameters
    ----------
    MlModel:
    1. Keyword 'LogisticRegression' indicates LogisticRegression.
            Correspondingly these parameters with head 'lr_' denote the inputs of model LogisticRegression,
            especially 'lr_sample_weight' belongs fit function.
    2. keyword 'LinearSVC' indicates svm.LinearSVC.
            Correspondingly these parameters with head 'lsvc_' denote the inputs of model LinearSVC,
            especially 'lsvc_sample_weight' belongs fit function.
    3. keyword 'SVC' indicates svm.SVC.
            Correspondingly these parameters with head 'svc_' denote the inputs of model SVC,
            especially 'svc_sample_weight' belongs fit function.
    4. keyword 'DecisionTreeClassifier' indicates tree.DecisionTreeClassifier.
            Correspondingly these parameters with head 'dt_' denote the inputs of model DecisionTreeClassifier,
            especially 'dt_sample_weight', 'dt_check_input', 'dt_X_idx_sorted' belong fit function.
    5. keyword 'RandomForestClassifier' indicates RandomForestClassifier.
            Correspondingly these parameters with head 'rf_' denote the inputs of model RandomForestClassifier,
            especially 'rf_sample_weight' belongs fit function.
    6. keyword 'GaussianNB' indicates Gaussian naive Bayes.
            Correspondingly these parameters with head 'gnb_' denote the inputs of model GaussianNB,
            especially 'gnb_sample_weight' belongs fit function.
    7. keyword 'KNeighborsClassifier' indicates neighbors.KNeighborsClassifier.
            Correspondingly these parameters with head 'knn_' denote the inputs of model KNeighborsClassifier,
            the fit function of KNeighborsClassifier doesn't need special input parameters.
    8. keyword 'NeuralNetwork' indicates neural network keras.Sequential.
            In 'NeuralNetwork' model, no parameters need to be inputted, just builds model keras.Sequential.
            Model configurations would be done in current py file by keras own command.

    """

    def __init__(self,
                 MlModel='LogisticRegression',
                 multiLabel=False,
                 multi_n_jobs=None,
                 lr_penalty='l2',
                 lr_dual=False,
                 lr_tol=1e-4,
                 lr_C=1.0,
                 lr_fit_intercept=True,
                 lr_intercept_scaling=1,
                 lr_class_weight=None,
                 lr_random_state=None,
                 lr_solver='warn',
                 lr_max_iter=100,
                 lr_multi_class='warn',
                 lr_verbose=0,
                 lr_warm_start=False,
                 lr_n_jobs=None,
                 lr_sample_weight=None,
                 lsvc_penalty='l2',
                 lsvc_loss='squared_hinge',
                 lsvc_dual=True,
                 lsvc_tol=1e-4,
                 lsvc_C=1.0,
                 lsvc_multi_class='ovr',
                 lsvc_fit_intercept=True,
                 lsvc_intercept_scaling=1,
                 lsvc_class_weight=None,
                 lsvc_verbose=0,
                 lsvc_random_state=None,
                 lsvc_max_iter=1000,
                 lsvc_sample_weight=None,
                 svc_C=1.0,
                 svc_kernel='rbf',
                 svc_degree=3,
                 svc_gamma='auto_deprecated',
                 svc_coef0=0.0,
                 svc_shrinking=True,
                 svc_probability=False,
                 svc_tol=0.001,
                 svc_cache_size=200,
                 svc_class_weight=None,
                 svc_verbose=False,
                 svc_max_iter=-1,
                 svc_decision_function_shape='ovr',
                 svc_random_state=None,
                 svc_sample_weight=None,
                 dt_criterion="gini",
                 dt_splitter="best",
                 dt_max_depth=None,
                 dt_min_samples_split=2,
                 dt_min_samples_leaf=1,
                 dt_min_weight_fraction_leaf=0.,
                 dt_max_features=None,
                 dt_random_state=None,
                 dt_max_leaf_nodes=None,
                 dt_min_impurity_decrease=0.,
                 dt_min_impurity_split=None,
                 dt_class_weight=None,
                 dt_presort=False,
                 dt_sample_weight=None,
                 dt_check_input=True,
                 dt_X_idx_sorted=None,
                 rf_n_estimators='warn',
                 rf_criterion="gini",
                 rf_max_depth=None,
                 rf_min_samples_split=2,
                 rf_min_samples_leaf=1,
                 rf_min_weight_fraction_leaf=0.,
                 rf_max_features="auto",
                 rf_max_leaf_nodes=None,
                 rf_min_impurity_decrease=0.,
                 rf_min_impurity_split=None,
                 rf_bootstrap=True,
                 rf_oob_score=False,
                 rf_n_jobs=None,
                 rf_random_state=None,
                 rf_verbose=0,
                 rf_warm_start=False,
                 rf_class_weight=None,
                 rf_sample_weight=None,
                 gnb_priors=None,
                 gnb_var_smoothing=1e-9,
                 gnb_sample_weight=None,
                 knn_n_neighbors=5,
                 knn_weights='uniform',
                 knn_algorithm='auto',
                 knn_leaf_size=30,
                 knn_p=2,
                 knn_metric='minkowski',
                 knn_metric_params=None,
                 knn_n_jobs=None):

        '''
            According parameter 'MlModel' selecting classification model
        '''
        self.MlModel = MlModel
        '''
            According parameter 'multiLabel' selecting binary or multi-label classification,
            'multi_n_jobs' is the unique additional parameter in multiclass.
        '''
        self.multiLabel = multiLabel
        self.multi_n_jobs = multi_n_jobs

        if self.MlModel == 'LogisticRegression':
            '''
                control the LogisticRegression model parameters
            '''
            self.lr_penalty = lr_penalty
            self.lr_dual = lr_dual
            self.lr_tol = lr_tol
            self.lr_C = lr_C
            self.lr_fit_intercept = lr_fit_intercept
            self.lr_intercept_scaling = lr_intercept_scaling
            self.lr_class_weight = lr_class_weight
            self.lr_random_state = lr_random_state
            self.lr_solver = lr_solver
            self.lr_max_iter = lr_max_iter
            self.lr_multi_class = lr_multi_class
            self.lr_verbose = lr_verbose
            self.lr_warm_start = lr_warm_start
            self.lr_n_jobs = lr_n_jobs
            '''
                control the parameters of fit function of LogisticRegression
            '''
            self.lr_sample_weight = lr_sample_weight

        elif self.MlModel == 'LinearSVC':
            '''
                control the LinearSVC model parameters
            '''
            self.lsvc_dual = lsvc_dual
            self.lsvc_tol = lsvc_tol
            self.lsvc_C = lsvc_C
            self.lsvc_multi_class = lsvc_multi_class
            self.lsvc_fit_intercept = lsvc_fit_intercept
            self.lsvc_intercept_scaling = lsvc_intercept_scaling
            self.lsvc_class_weight = lsvc_class_weight
            self.lsvc_verbose = lsvc_verbose
            self.lsvc_random_state = lsvc_random_state
            self.lsvc_max_iter = lsvc_max_iter
            self.lsvc_penalty = lsvc_penalty
            self.lsvc_loss = lsvc_loss
            '''
                control the parameters of fit function of LinearSVC
            '''
            self.lsvc_sample_weight = lsvc_sample_weight

        elif self.MlModel == 'SVC':
            '''
                control the SVC model parameters
            '''
            self.svc_C=svc_C
            self.svc_kernel=svc_kernel
            self.svc_degree=svc_degree
            self.svc_gamma=svc_gamma
            self.svc_coef0=svc_coef0
            self.svc_shrinking=svc_shrinking
            self.svc_probability=svc_probability
            self.svc_tol=svc_tol
            self.svc_cache_size=svc_cache_size
            self.svc_class_weight=svc_class_weight
            self.svc_verbose=svc_verbose
            self.svc_max_iter=svc_max_iter
            self.svc_decision_function_shape=svc_decision_function_shape
            self.svc_random_state=svc_random_state
            '''
                control the parameters of fit function of SVC
            '''
            self.svc_sample_weight=svc_sample_weight

        elif self.MlModel == 'DecisionTreeClassifier':
            '''
                control the DecisionTreeClassifier model parameters
            '''
            self.dt_criterion = dt_criterion
            self.dt_splitter = dt_splitter
            self.dt_max_depth = dt_max_depth
            self.dt_min_samples_split = dt_min_samples_split
            self.dt_min_samples_leaf = dt_min_samples_leaf
            self.dt_min_weight_fraction_leaf = dt_min_weight_fraction_leaf
            self.dt_max_features = dt_max_features
            self.dt_max_leaf_nodes = dt_max_leaf_nodes
            self.dt_class_weight = dt_class_weight
            self.dt_random_state = dt_random_state
            self.dt_min_impurity_decrease = dt_min_impurity_decrease
            self.dt_min_impurity_split = dt_min_impurity_split
            self.dt_presort = dt_presort
            '''
                control the parameters of fit function of DecisionTreeClassifier
            '''
            self.dt_sample_weight = dt_sample_weight
            self.dt_check_input = dt_check_input
            self.dt_X_idx_sorted = dt_X_idx_sorted

        elif self.MlModel == 'RandomForestClassifier':
            '''
                control the RandomForestClassifier model parameters
            '''
            """
            self.rf_n_estimators = rf_n_estimators
            self.rf_criterion = rf_criterion
            self.rf_max_depth = rf_max_depth
            self.rf_min_samples_split = rf_min_samples_split
            self.rf_min_samples_leaf = rf_min_samples_leaf
            self.rf_min_weight_fraction_leaf = rf_min_weight_fraction_leaf
            self.rf_max_features = rf_max_features
            self.rf_max_leaf_nodes = rf_max_leaf_nodes
            self.rf_min_impurity_decrease = rf_min_impurity_decrease
            self.rf_min_impurity_split = rf_min_impurity_split
            self.rf_bootstrap = rf_bootstrap
            self.rf_oob_score = rf_oob_score
            self.rf_n_jobs = rf_n_jobs
            self.rf_random_state = rf_random_state
            self.rf_verbose = rf_verbose
            self.rf_warm_start = rf_warm_start
            self.rf_class_weight = rf_class_weight
            '''
                control the parameters of fit function of RandomForestClassifier
            '''
            self.rf_sample_weight = rf_sample_weight
            """
        elif self.MlModel == 'GaussianNB':
            '''
                control the GaussianNB model parameters
            '''
            self.gnb_priors = gnb_priors
            self.gnb_var_smoothing = gnb_var_smoothing
            '''
                control the parameters of fit function of GaussianNB
            '''
            self.gnb_sample_weight = gnb_sample_weight

        elif self.MlModel == 'KNeighborsClassifier':
            '''
                control the KNeighborsClassifier model parameters
            '''
            self.knn_n_neighbors = knn_n_neighbors
            self.knn_weights = knn_weights
            self.knn_algorithm = knn_algorithm
            self.knn_leaf_size = knn_leaf_size
            self.knn_p = knn_p
            self.knn_metric = knn_metric
            self.knn_metric_params = knn_metric_params
            self.knn_n_jobs = knn_n_jobs

        elif self.MlModel == 'NeuralNetwork':
            pass

        else:
            warnings.warn(
                "The chosen ML model doesn't exist, check if it's wrong model name!"
            )
            exit()

    def buildingModelAndFittingTrainSet(self, X, y):
        '''
        When "self.MlModel == 'NeuralNetwork'", train set (X, y) is not necessary.
        So just directly set inputs (X, y) as (None, None) in buildingModelAndFittingTrainSet().
        '''
        if (self.MlModel == 'LogisticRegression'):
            return self.modelLogisticRegression(X, y)
        elif (self.MlModel == 'LinearSVC'):
            return self.modelLinearSVC(X, y)
        elif (self.MlModel == 'SVC'):
            return self.modelSVC(X, y)
        elif (self.MlModel == 'DecisionTreeClassifier'):
            return self.modelDecisionTreeClassifier(X, y)
        elif (self.MlModel == 'RandomForestClassifier'):
            return self.modelRandomForestClassifier(X, y)
        elif (self.MlModel == 'GaussianNB'):
            return self.modelGaussianNB(X, y)
        elif (self.MlModel == 'KNeighborsClassifier'):
            return self.modelKNeighborsClassifier(X, y)
        elif (self.MlModel == 'NeuralNetwork'):
            return self.modelNeuralNetwork()
        else:
            warnings.warn(
                "The chosen ML model doesn't exist, wrong input model name!")
            sys.exit(0)

    def modelLogisticRegression(self, X, y):
        model = LogisticRegression(
                penalty=self.lr_penalty,
                dual=self.lr_dual,
                tol=self.lr_tol,
                C=self.lr_C,
                fit_intercept=self.lr_fit_intercept,
                intercept_scaling=self.lr_intercept_scaling,
                class_weight=self.lr_class_weight,
                random_state=self.lr_random_state,
                solver=self.lr_solver,
                max_iter=self.lr_max_iter,
                multi_class=self.lr_multi_class,
                verbose=self.lr_verbose,
                warm_start=self.lr_warm_start,
                n_jobs=self.lr_n_jobs)

        if (self.multiLabel == False):
            return model.fit(X, y, sample_weight=self.lr_sample_weight)
        else :
            model_multiLabel = OneVsRestClassifier(model, n_jobs= self.multi_n_jobs)
            return model_multiLabel.fit(X, y)

    def modelLinearSVC(self, X, y):
        model = svm.LinearSVC(
            penalty=self.lsvc_penalty,
            loss=self.lsvc_loss,
            dual=self.lsvc_dual,
            tol=self.lsvc_tol,
            C=self.lsvc_C,
            multi_class=self.lsvc_multi_class,
            fit_intercept=self.lsvc_fit_intercept,
            intercept_scaling=self.lsvc_intercept_scaling,
            class_weight=self.lsvc_class_weight,
            verbose=self.lsvc_verbose,
            random_state=self.lsvc_random_state,
            max_iter=self.lsvc_max_iter)

        if (self.multiLabel == False):
            return model.fit(X, y, sample_weight=self.lsvc_sample_weight)
        else :
            model_multiLabel = OneVsRestClassifier(model, n_jobs= self.multi_n_jobs)
            return model_multiLabel.fit(X, y)

    def modelSVC(self, X, y):
        model = svm.SVC(
            C=self.svc_C,
            kernel=self.svc_kernel,
            degree=self.svc_degree,
            gamma=self.svc_gamma,
            coef0=self.svc_coef0,
            shrinking=self.svc_shrinking,
            probability=self.svc_probability,
            tol=self.svc_tol,
            cache_size=self.svc_cache_size,
            class_weight=self.svc_class_weight,
            verbose=self.svc_verbose,
            max_iter=self.svc_max_iter,
            decision_function_shape=self.svc_decision_function_shape,
            random_state=self.svc_random_state
        )
        if (self.multiLabel == False):
            return model.fit(X, y, sample_weight=self.svc_sample_weight)
        else :
            model_multiLabel = OneVsRestClassifier(model, n_jobs= self.multi_n_jobs)
            return model_multiLabel.fit(X, y)

    def modelDecisionTreeClassifier(self, X, y):
        model = tree.DecisionTreeClassifier(
            criterion=self.dt_criterion,
            splitter=self.dt_splitter,
            max_depth=self.dt_max_depth,
            min_samples_split=self.dt_min_samples_split,
            min_samples_leaf=self.dt_min_samples_leaf,
            min_weight_fraction_leaf=self.dt_min_weight_fraction_leaf,
            max_features=self.dt_max_features,
            max_leaf_nodes=self.dt_max_leaf_nodes,
            class_weight=self.dt_class_weight,
            random_state=self.dt_random_state,
            min_impurity_decrease=self.dt_min_impurity_decrease,
            min_impurity_split=self.dt_min_impurity_split,
            presort=self.dt_presort)

        if (self.multiLabel == False):
            return model.fit(
                X,
                y,
                sample_weight=self.dt_sample_weight,
                check_input=self.dt_check_input,
                X_idx_sorted=self.dt_X_idx_sorted)
        else:
            model_multiLabel = OneVsRestClassifier(model, n_jobs= self.multi_n_jobs)
            return model_multiLabel.fit(X, y)

    def modelRandomForestClassifier(self, X, y):
        model = RandomForestClassifier(
            n_estimators=self.rf_n_estimators,
            criterion=self.rf_criterion,
            max_depth=self.rf_max_depth,
            min_samples_split=self.rf_min_samples_split,
            min_samples_leaf=self.rf_min_samples_leaf,
            min_weight_fraction_leaf=self.rf_min_weight_fraction_leaf,
            max_features=self.rf_max_features,
            max_leaf_nodes=self.rf_max_leaf_nodes,
            min_impurity_decrease=self.rf_min_impurity_decrease,
            min_impurity_split=self.rf_min_impurity_split,
            bootstrap=self.rf_bootstrap,
            oob_score=self.rf_oob_score,
            n_jobs=self.rf_n_jobs,
            random_state=self.rf_random_state,
            verbose=self.rf_verbose,
            warm_start=self.rf_warm_start,
            class_weight=self.rf_class_weight)

        if (self.multiLabel == False):
            return model.fit(X, y, sample_weight=self.rf_sample_weight)
        else:
            model_multiLabel = OneVsRestClassifier(model, n_jobs= self.multi_n_jobs)
            return model_multiLabel.fit(X, y)

    def modelGaussianNB(self, X, y):
        model = GaussianNB(
            priors=self.gnb_priors,
            var_smoothing=self.gnb_var_smoothing,
        )
        if (self.multiLabel == False):
            return model.fit(X, y, sample_weight=self.gnb_sample_weight)
        else:
            model_multiLabel = OneVsRestClassifier(model, n_jobs= self.multi_n_jobs)
            return model_multiLabel.fit(X, y)

    def modelKNeighborsClassifier(self, X, y):
        model = neighbors.KNeighborsClassifier(
            n_neighbors=self.knn_n_neighbors,
            weights=self.knn_weights,
            algorithm=self.knn_algorithm,
            leaf_size=self.knn_leaf_size,
            p=self.knn_p,
            metric=self.knn_metric,
            metric_params=self.knn_metric_params,
            n_jobs=self.knn_n_jobs)

        if (self.multiLabel == False):
            return model.fit(X, y)
        else:
            model_multiLabel = OneVsRestClassifier(model, n_jobs= self.multi_n_jobs)
            return model_multiLabel.fit(X, y)

    # check multi-label later
    def modelNeuralNetwork(self):
        model = keras.Sequential()
        return model

def Cal_ROC_curve_parameters(classifier,classifierLabel,x_input,
                        y_true, multiFlag=False, targetIndexInPredict=1):

    if (classifierLabel == 'LogisticRegression') or (
            classifierLabel == 'LinearSVC') or (classifierLabel == 'SVC'):
        y_score = classifier.decision_function(x_input)
    elif (classifierLabel == 'DecisionTreeClassifier') or classifierLabel == (
            'RandomForestClassifier') or (classifierLabel == 'GaussianNB') or (
                classifierLabel == 'KNeighborsClassifier'):
        if (not multiFlag):
            y_score = classifier.predict_proba(
                x_input)[:, targetIndexInPredict]
        else:
            y_score = classifier.predict_proba(x_input)
    else:
        warnings.warn('Wrong chose, no such over classifier label!')
        sys.exit(0)

    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    if (not multiFlag):
        posLabel = classifier.classes_[targetIndexInPredict]
        # Compute average ROC curve and ROC area
        fpr["micro"], tpr["micro"], _ = roc_curve(y_true, y_score, pos_label=posLabel)
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
    else:

        # Compute average ROC curve and ROC area
        fpr["micro"], tpr["micro"], _ = roc_curve(y_true.ravel(),
                                                  y_score.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    return fpr["micro"], tpr["micro"], roc_auc["micro"]

def Cal_PR_curve_parameters(classifier,
                       classifierLabel,
                       x_input,
                       y_true,
                       multiFlag=False,
                       targetIndexInPredict=1):

    if (classifierLabel == 'LogisticRegression') or (
            classifierLabel == 'LinearSVC') or (classifierLabel == 'SVC'):
        y_score = classifier.decision_function(x_input)
    elif (classifierLabel == 'DecisionTreeClassifier') or classifierLabel == (
            'RandomForestClassifier') or (classifierLabel == 'GaussianNB') or (
                classifierLabel == 'KNeighborsClassifier'):
        if (not multiFlag):
            y_score = classifier.predict_proba(
                x_input)[:, targetIndexInPredict]
        else:
            y_score = classifier.predict_proba(x_input)
    else:
        warnings.warn('Wrong chose, no such over classifier label!')
        sys.exit(0)

    y_pred = classifier.predict(x_input)

    # For each class
    precision = dict()
    recall = dict()
    average_precision = dict()
    f1Score = dict()

    if (not multiFlag):
        posLabel = classifier.classes_[targetIndexInPredict]
        precision["micro"], recall["micro"], _ = precision_recall_curve(
            y_true, y_score, pos_label=posLabel)
        average_precision["micro"] = average_precision_score(
            y_true, y_score, pos_label=posLabel)
        f1Score["micro"] = f1_score(y_true, y_pred, pos_label=posLabel)
    else:

        # A "micro-average": quantifying score on all classes jointly
        precision["micro"], recall["micro"], _ = precision_recall_curve(
            y_true.ravel(), y_score.ravel())
        average_precision["micro"] = average_precision_score(
            y_true, y_score, average="micro")
        f1Score["micro"] = f1_score(y_true, y_pred, average='micro')

    return precision["micro"], recall["micro"], average_precision["micro"], f1Score["micro"]

def Cal_nn_ROC_parameters(classifier,x_input,
                        y_true, multiFlag=False):
    '''
        Notice: binary classification or multi classification

        y_true must be binarized.
    '''
    if(not multiFlag):
        predictions = classifier.predict(x_input)[:,1]
    else:
        predictions = classifier.predict(x_input)

    # print(y_true)
    # print(predictions)

    fpr = dict()
    tpr = dict()
    roc_auc = dict()

    # Compute average ROC curve and ROC area
    fpr["micro"], tpr["micro"], _ = roc_curve(y_true.ravel(),
                                                predictions.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    return fpr["micro"], tpr["micro"], roc_auc["micro"]

def Cal_nn_PR_parameters(classifier, x_input, y_true, labels, multiFlag=False):
    '''
        Notice: binary classification or multi classification

        y_true must be binarized.
    '''

    if(not multiFlag):
        predictions = classifier.predict(x_input)[:,1]
    else:
        predictions = classifier.predict(x_input)

    # For each class
    precision = dict()
    recall = dict()
    average_precision = dict()
    f1Score = dict()

    # A "micro-average": quantifying score on all classes jointly
    precision["micro"], recall["micro"], _ = precision_recall_curve(
        y_true.ravel(), predictions.ravel())
    average_precision["micro"] = average_precision_score(
        y_true, predictions, average="micro")

    y_pred = list()
    for i in range(len(predictions)):
        y_pred.append(np.argmax(predictions[i]))

    # Use label_binarize to be multi-label like settings
    y_pred_binarized = label_binarize(y_pred, classes=labels)

    f1Score["micro"] = f1_score(y_true, y_pred_binarized, average='micro')

    return precision["micro"], recall["micro"], average_precision["micro"], f1Score["micro"]
