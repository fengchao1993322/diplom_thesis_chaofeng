import itertools
import numpy as np
import matplotlib.pyplot as plt
import sys
import warnings
from itertools import cycle

from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import average_precision_score
from sklearn.metrics import f1_score


def plot_confusion_matrix(cm,
                          classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        # print("Normalized confusion matrix")
    else:
        pass
        # print('Confusion matrix, without normalization')

    # print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(
            j,
            i,
            format(cm[i, j], fmt),
            horizontalalignment="center",
            color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    #plt.tight_layout()


def confusion_matrix_Generator(classifier,
                               classifierLabel,
                               x_input,
                               y_true,
                               classesNameInput=None,
                               normalizationLabel=False):

    y_pred = classifier.predict(x_input)
    if classesNameInput != None:
        classesName = classesNameInput
    else:
        classesName = classifier.classes_

    # print('Class index: ')
    # for index, item in enumerate(classesName):
    #     print('%d  %s: %s' % (index, item, classifier.classes_[index]))

    # Compute confusion matrix
    cnf_matrix = confusion_matrix(y_true, y_pred)
    #np.set_printoptions(precision=2)

    if normalizationLabel != True:
        # Plot non-normalized confusion matrix
        plot_confusion_matrix(
            cnf_matrix,
            classes=classesName,
            title='Confusion matrix of %s' % classifierLabel)
    else:
        # Plot normalized confusion matrix
        plot_confusion_matrix(
            cnf_matrix,
            classes=classesName,
            normalize=True,
            title='Normalized confusion matrix of %s' % classifierLabel)


def confusion_matrix_For_NN(classifier,
                            x_input,
                            y_true,
                            classesNameInput=None,
                            normalizationLabel=False):

    predictions = classifier.predict(x_input)

    y_pred = list()
    for i in range(len(predictions)):
        y_pred.append(np.argmax(predictions[i]))

    y_pred = np.array(y_pred)

    # Compute confusion matrix
    cnf_matrix = confusion_matrix(y_true, y_pred)
    #np.set_printoptions(precision=2)

    if normalizationLabel != True:
        # Plot non-normalized confusion matrix
        plot_confusion_matrix(
            cnf_matrix,
            classes=classesNameInput,
            title='Confusion matrix of neural network')
    else:
        # Plot normalized confusion matrix
        plot_confusion_matrix(
            cnf_matrix,
            classes=classesNameInput,
            normalize=True,
            title='Normalized confusion matrix of neural network')


def ROC_curve_Generator(classifier,
                        classifierLabel,
                        x_input,
                        y_true,
                        multiFlag=False,
                        targetIndexInPredict=1,
                        n_classes=2):
    '''
    Parameter 'multiFlag' must be input to define the binary or multi classification.
    When multiFlag = False, and the classifierLabel is 'DecisionTreeClassifier', 
    'RandomForestClassifier', 'GaussianNB' or 'KNeighborsClassifier', the function 
    'predict_proba' will be used to calculate y_score, then the targetIndexInPredict 
    must be given in order to choose the target class in roc_curve, 'n_classes' is not necessary.

    When multiFlag = True, the 'classifier' and 'y_true' must use the multi-model
    (e.g. clf1_m, y_test_m), meanwhile, needing paramter 'n_classes'. The targetIndexInPredict 
    is not necessary.
    '''

    if (classifierLabel == 'LogisticRegression') or (
            classifierLabel == 'LinearSVC') or (classifierLabel == 'SVC'):
        y_score = classifier.decision_function(x_input)
    elif (classifierLabel == 'DecisionTreeClassifier') or classifierLabel == (
            'RandomForestClassifier') or (classifierLabel == 'GaussianNB') or (
                classifierLabel == 'KNeighborsClassifier'):
        if (not multiFlag):
            y_score = classifier.predict_proba(
                x_input)[:, targetIndexInPredict]
        else:
            y_score = classifier.predict_proba(x_input)
    else:
        warnings.warn('Wrong chose, no such over classifier label!')
        sys.exit(0)

    if (not multiFlag):
        posLabel = classifier.classes_[targetIndexInPredict]
        fpr, tpr, _ = roc_curve(y_true, y_score, pos_label=posLabel)
        roc_auc = auc(fpr, tpr)
    else:
        fpr = dict()
        tpr = dict()
        roc_auc = dict()

        for i in range(n_classes):
            fpr[i], tpr[i], _ = roc_curve(y_true[:, i], y_score[:, i])
            roc_auc[i] = auc(fpr[i], tpr[i])

        # Compute average ROC curve and ROC area
        fpr["micro"], tpr["micro"], _ = roc_curve(y_true.ravel(),
                                                  y_score.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    # Plot
    lw = 1.5
    if (not multiFlag):
        plt.plot(
            fpr,
            tpr,
            color='red',
            lw=lw,
            label='ROC curve of %s'%(classifierLabel))
        plt.plot([0, 1], [0, 1], color='k', lw=lw, linestyle='--')
        plt.title('ROC curve of %s to binary class: area = %0.2f' % (classifierLabel, roc_auc))

    else:
        plt.plot(
            fpr["micro"],
            tpr["micro"],
            label='micro-average ROC curve (area = {0:0.2f})'
            ''.format(roc_auc["micro"]),
            color='red',
            ls=':',
            linewidth=2)

        colors = [
             'navy', 'darkorchid', 'gold', 'chartreuse', 'seagreen','deepskyblue', 'blue', 'violet'  
        ]
        for i, color in zip(range(n_classes), colors):
            plt.plot(
                fpr[i],
                tpr[i],
                color=color,
                lw=lw,
                label='ROC curve of class {0} (area = {1:0.2f})'
                ''.format(i, roc_auc[i]))

        plt.plot([0, 1], [0, 1], 'k--', lw=lw)
        plt.title('ROC curve of %s to multi-class' % classifierLabel)
        plt.legend(loc="lower right")

    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend(loc="lower right")


def PR_curve_Generator(classifier,
                       classifierLabel,
                       x_input,
                       y_true,
                       multiFlag=False,
                       targetIndexInPredict=1,
                       n_classes=2):
    '''
    Parameter 'multiFlag' must be input to define the binary or multi classification.
    When multiFlag = False, and the classifierLabel is 'DecisionTreeClassifier', 
    'RandomForestClassifier', 'GaussianNB' or 'KNeighborsClassifier', the function 
    'predict_proba' will be used to calculate y_score, then the targetIndexInPredict 
    must be given in order to choose the target class in roc_curve, 'n_classes' is not necessary.

    When multiFlag = True, the 'classifier' and 'y_true' must use the multi-model
    (e.g. clf1_m, y_test_m), meanwhile, needing paramter 'n_classes'. The targetIndexInPredict 
    is not necessary.
    '''

    if (classifierLabel == 'LogisticRegression') or (
            classifierLabel == 'LinearSVC') or (classifierLabel == 'SVC'):
        y_score = classifier.decision_function(x_input)
    elif (classifierLabel == 'DecisionTreeClassifier') or classifierLabel == (
            'RandomForestClassifier') or (classifierLabel == 'GaussianNB') or (
                classifierLabel == 'KNeighborsClassifier'):
        if (not multiFlag):
            y_score = classifier.predict_proba(
                x_input)[:, targetIndexInPredict]
        else:
            y_score = classifier.predict_proba(x_input)
    else:
        warnings.warn('Wrong chose, no such over classifier label!')
        sys.exit(0)

    y_pred = classifier.predict(x_input)
    if (not multiFlag):
        posLabel = classifier.classes_[targetIndexInPredict]
        precision, recall, _ = precision_recall_curve(
            y_true, y_score, pos_label=posLabel)
        average_precision = average_precision_score(
            y_true, y_score, pos_label=posLabel)
        f1Score = f1_score(y_true, y_pred, pos_label=posLabel)
    else:
        # For each class
        precision = dict()
        recall = dict()
        average_precision = dict()
        f1Score = dict()

        for i in range(n_classes):
            precision[i], recall[i], _ = precision_recall_curve(
                y_true[:, i], y_score[:, i])
            average_precision[i] = average_precision_score(
                y_true[:, i], y_score[:, i])
            f1Score[i] = f1_score(y_true[:, i], y_pred[:, i])

        # A "micro-average": quantifying score on all classes jointly
        precision["micro"], recall["micro"], _ = precision_recall_curve(
            y_true.ravel(), y_score.ravel())
        average_precision["micro"] = average_precision_score(
            y_true, y_score, average="micro")
        f1Score["micro"] = f1_score(y_true, y_pred, average='micro')

    # Plot
    lw = 1.5
    if (not multiFlag):
        plt.plot(recall, precision, color='red', lw=lw, label='PR curve of %s' %classifierLabel)
        f_scores = np.linspace(0.2, 0.8, num=4)
        for f_score in f_scores:
            x = np.linspace(0.01, 1)
            y = f_score * x / (2 * x - f_score)
            plt.plot(x[y >= 0], y[y >= 0], color='gray', alpha=0.2)
            plt.annotate('f1={0:0.1f}'.format(f_score), xy=(0.9, y[45] + 0.02))

        plt.title('PR curve of %s to binary class: AP=%.3f, f1 Score=%.3f' %
                  (classifierLabel, average_precision, f1Score))
        plt.legend(loc = 'lower right')
    else:
        # setup plot details
        colors = [
             'navy', 'darkorchid', 'gold', 'chartreuse', 'seagreen','deepskyblue', 'blue', 'violet'  
        ]

        f_scores = np.linspace(0.2, 0.8, num=4)
        lines = []
        labels = []
        for f_score in f_scores:
            x = np.linspace(0.01, 1)
            y = f_score * x / (2 * x - f_score)
            l, = plt.plot(x[y >= 0], y[y >= 0], color='gray', alpha=0.2)
            plt.annotate('f1={0:0.1f}'.format(f_score), xy=(0.9, y[45] + 0.02))

        lines.append(l)
        labels.append('iso-f1 curves')
        l, = plt.plot(
            recall["micro"],
            precision["micro"],
            color='red',
            linestyle=':',
            lw=2)
        lines.append(l)
        labels.append(
            'micro-average Precision-recall (area = %.3f, f1 Score=%.3f)' %
            (average_precision["micro"], f1Score["micro"]))

        for i, color in zip(range(n_classes), colors):
            l, = plt.plot(recall[i], precision[i], color=color, lw=lw)
            lines.append(l)
            labels.append(
                'Precision-recall for class {0} (area = {1:0.3f}, f1 Score = {1:0.3f})'
                ''.format(i, average_precision[i], f1Score[i]))

        fig = plt.gcf()
        fig.subplots_adjust(bottom=0.25)
        plt.title('PR curve of %s to multi-class' % classifierLabel)
        plt.legend(lines, labels, loc=(0, -.38), prop=dict(size=14))

    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.0])


def plot_Acc_Loss(Acc, Loss):
    epoch = list()
    for i in range(len(Acc)):
        epoch.append(i+1)

    lw = 1.5
    plt.plot(
                epoch,
                Acc,
                color='r',
                marker = 'x',
                lw=lw,
                label='Accuracy curve')

    plt.plot(
                epoch,
                Loss,
                color='b',
                marker = '.',
                lw=lw,
                label='Loss curve')

    plt.plot([0, 50], [1, 1], color='k', lw=1, linestyle='--', label = 'Rate = 1.0')
    # plt.plot([20, 20], [0, 1], color='k', lw=1, linestyle='--', label = 'Epoch = 20')

    plt.title('Acc and Loss curve of neural network')
    plt.ylim([0.0, 1.05])
    plt.xlim([1, len(Acc)])
    plt.xlabel('Epoch')
    plt.ylabel('Rate')
    plt.legend(loc="best")

def plot_parallel_histogram(size, n, name_list, object_name, a, b):
    '''
        'size' means classifiers;
        'n' means object's quantity.
    '''
    x = np.arange(size)
    
    total_width= 0.8
    width = total_width / n
    x = x - (total_width - width) / (size -1)

    plt.bar(x, a, width=width, label=object_name[0],fc = 'y')
    plt.bar(x + width, b, width=width, tick_label = name_list, label=object_name[1],fc = 'r')
    plt.title('Accuracy comparison between classifiers')
    plt.xlabel('Classifiers')
    plt.ylabel('Accuracy')
    plt.legend(loc="lower right")
