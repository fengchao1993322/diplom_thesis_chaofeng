import numpy as np
import sys
import pandas as pd

import summaryFunc as sFunc

loadingPath = "/home/student/Documents/aa_scripter/test0.pcap"
pktsDataFloat = sFunc.readPcap(loadingPath)

'''
    only detect byte static(0) or dynamic(1)
'''
resultPkts2dArray = np.empty([len(pktsDataFloat), pktsDataFloat.shape[1]])

for rowIndexOfPktsDataframe in range(len(pktsDataFloat)):
    for columnIndexOfPktsDataframe in range(pktsDataFloat.shape[1]):
        if rowIndexOfPktsDataframe == 0: 
            if (pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe]-0) == 0:
                resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe]=0
            else:
                resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe]=1
        else:
            byteDeviationInColumn = pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe]-pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe]
            if byteDeviationInColumn == 0:
                resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe]=0
            else:
                resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe]=1
            

pktsDataFloat.index = sFunc.pktLabelGeneration(resultPkts2dArray)

###################################################################################
testSize = 0.2
X_train, X_test, y_train, y_test = sFunc.trainTestSeparate(pktsDataFloat, pktsDataFloat.index, testSize)

X_train_std, X_test_std = sFunc.dataStandardization(X_train, X_test)
_, pktsDataframe_std = sFunc.dataStandardization(X_train, pktsDataFloat)


from sklearn.linear_model import LogisticRegression 

lr = LogisticRegression(C=100000.0, random_state=0, class_weight='balanced', solver='liblinear', multi_class= 'auto') 
lr.fit(X_train_std, y_train) 

print('Test accuracy:', lr.score(X_test_std, y_test))
print(lr.classes_)

#print(lr.predict_proba(X_train_std)[0,:]) # check the first train sample's probality
print('pktsDataframe_Std[0,:]: ', lr.predict_proba(pktsDataframe_std)[0,:])
print('pktsDataframe_Std[1,:]: ', lr.predict_proba(pktsDataframe_std)[1,:])
print('pktsDataframe_Std[2,:]: ', lr.predict_proba(pktsDataframe_std)[2,:])
print('pktsDataframe_Std[3,:]: ', lr.predict_proba(pktsDataframe_std)[3,:])
print('pktsDataframe_Std[4,:]: ', lr.predict_proba(pktsDataframe_std)[4,:])
print('pktsDataframe_Std[5,:]: ', lr.predict_proba(pktsDataframe_std)[5,:])
print('pktsDataframe_Std[6,:]: ', lr.predict_proba(pktsDataframe_std)[6,:])