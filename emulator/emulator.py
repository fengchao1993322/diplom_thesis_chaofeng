from o2sc import compressor
from o2sc import decompressor
from o2sc.classification import model

import scapy.all as scapy
import numpy as np
import scipy.stats as st

import socket

import urwid
import config

import os
import sys
import optparse
import warnings

if not sys.warnoptions:
    warnings.simplefilter("ignore")
    os.environ["PYTHONWARNINGS"] = "ignore" # Also affect subprocesses


parser = optparse.OptionParser(usage="usage: emulator.py [options]")

parser.add_option("-d", "--decompressor",
                action="store_true", dest="is_decompressor", default=False,
                help="run as decompressor instead of compressor.")
parser.add_option("-a", "--address",
                action="store", dest="ip", default="127.0.0.1",
                help="IP address of the socket.")
parser.add_option("-p", "--port",
                action="store", dest="port", default="65432",
                help="port number of the socket.")
parser.add_option("-o", "--oracle",
                action="store", dest="samples", default="-1",
                help="number of samples to train the oracle on.")
parser.add_option("-D", "--docker",
                action="store_true", dest="docker", default=False,
                help="enables docker support.")
parser.add_option("-s", "--source",
                action="store_true", dest="is_source", default=False,
                help="enables UDP packet tranmission.")
parser.add_option("-S", "--source_port",
                action="store", dest="source_port", default="50000",
                help="enables UDP packet tranmission source port.")
parser.add_option("-f", "--flow_idx",
                action="store", dest="flow_idx", default="0",
                help="flow index of source transmission.")
parser.add_option("-u", "--dummy_sources",
                action="store_true", dest="dummy_sources", default=False,
                help="dummy source for UDP transmission.")

(opts, args) = parser.parse_args(sys.argv)

use_all_app_streams = False
pcap_path = [(config.PATH_DOCKER_ROOT if opts.docker else "") + '../pcaps/streams/asterisk_rtp_downstream_240_test_100_1000.pcap']

palette = [
    ('reversed', 'standout', ''),
    ('banner', 'dark gray', 'white', 'black'),
    ('bg background','light gray', 'black'),
    ('bg 1',         'black',      'white', 'standout'),
    ('bg 1 smooth',  'dark blue',  'black'),
    ('bg 2',         'black',      'dark cyan', 'standout'),
    ('bg 2 smooth',  'dark cyan',  'black')
    ]

status_txt_string = ""
status_txt = urwid.Text(status_txt_string)
stats_txt_string = ""
stats_txt = urwid.Text(stats_txt_string)

pbar = urwid.ProgressBar('bg background', 'banner')

samples = int(opts.samples)
oversampling = True
static_compression = False
online = False
learn_every_pkt = False


grid_search_hdr_flows = []
grid_search_app_flows = []
grid_search_clfs      = []
grid_search_samps     = []
grid_search_oversampling = False
grid_online = False

def set_bar_progress(progress):
    global pbar
    pbar.done = 100
    pbar.set_completion(progress + 1)

def set_progress(progress):
    set_bar_progress(int(progress * 100))

def item_chosen_main(button):
    global source
    global samples
    #if button.label == config.LBL_CONNECT:
    #    source.connect()
    if button.label == config.LBL_ORACLE:
        oracle_selection()
    #elif button.label == config.LBL_CLF:
    #    clf_selection()
    #elif button.label == config.LBL_HDRS:
    #    headers_selection()
    elif button.label == config.LBL_FLOW:
        flow_selection()
    elif button.label == config.LBL_SETUP:
        config_selection()
    #elif button.label == config.LBL_SAMP:
    #    samples_selection()
    #elif button.label == config.LBL_TRAIN:
    #    source.load_pcap(pcap_path)
    #    source.train_oracle(samples)
    #elif button.label == config.LBL_SEND_UC:
    #    clear_gains()
    #    source.send_uncompressed(collect_gains, set_progress)
    #    set_bar_progress(100)
    elif button.label == config.LBL_SEND:
        send_selection()
    elif button.label == config.LBL_GRID:
        grid_search()
    elif button.label == config.LBL_EXIT:
        source.disconnect()
        urwid.ExitMainLoop()
        sys.exit(0)

def menu(title, choices, selection_handler):
    body = [urwid.Divider(), urwid.Text(title), urwid.Divider()]
    for c in choices:
        button = urwid.Button(c)
        urwid.connect_signal(button, 'click', selection_handler)
        body.append(urwid.AttrMap(button, None, focus_map='reversed'))
    return urwid.ListBox(urwid.SimpleFocusListWalker(body))

def print_to_status(string, to_stats_bar=False):
    global status_txt_string
    global stats_txt_string
    if to_stats_bar == False:
        status_txt_string += string + "\n"
        last_lines = status_txt_string.split('\n')[-8:]
        to_print = ""
        for i in last_lines:
            to_print += i + '\n'
        status_txt.set_text(to_print)
        loop.draw_screen()
    else:
        stats_txt.set_text(string)
        loop.draw_screen()

if opts.is_source:
    print("UDP source")
    source = config.UDPOutgress(opts.ip, int(opts.port), int(opts.flow_idx), opts.docker)
    source.connect()
    source.send()

elif not opts.is_decompressor:
    source = config.Source(opts.ip, int(opts.port), opts.docker, print_to_status, True)

    # layout
    pile = urwid.Pile([urwid.Divider(), urwid.Divider(), status_txt])
    tt = urwid.Filler(pile, valign='top')

    choices_main = [config.LBL_ORACLE, config.LBL_FLOW, config.LBL_SETUP,
        config.LBL_SEND, config.LBL_GRID, config.LBL_EXIT]
    main = urwid.Padding(menu(config.LBL_HDR_EMU, choices_main,
        item_chosen_main), left=2, right=10)

    packet_graph = urwid.BarGraph(['bg background','bg 1','bg 2'])
    t = urwid.Pile([urwid.Columns([(45, main), tt]),
        ('pack', urwid.Padding(urwid.Pile([stats_txt, urwid.Divider("-")]),
            left=2, right=2)),
            urwid.Padding(packet_graph, left=2, right=2),
            ('pack', urwid.Padding(urwid.Pile([urwid.Divider("-"), pbar,urwid.Divider()]), left=2, right=2))
        ])

    top = urwid.Overlay(t, urwid.SolidFill(u'\N{MEDIUM SHADE}'),
        align='center', width=('relative', 60),
        valign='middle', height=('relative', 80),
        min_width=80, min_height=25)

    loop = urwid.MainLoop(top, palette=palette)



    def set_graph_gain(gains):
        global packet_graph
        # convert to displayable format
        bardata = []
        for i in gains:
            bardata.append((i if i >= 0 else 0,))
        #print_to_status(str(bardata))
        packet_graph.set_data(bardata, 100)
        loop.draw_screen()

    gain_list = []
    def clear_gains():
        global gain_list
        gain_list = []
    def collect_gains(gain):
        global gain_list
        #print_to_status(str(gain))
        gain_list.append(int(gain * 100.0))
        if len(gain_list) <= 100:
            set_graph_gain(gain_list[0:100])
        else:
            set_graph_gain(gain_list[len(gain_list)-101:len(gain_list)])

    def return_to_main(button=None):
        main.original_widget = menu(config.LBL_HDR_EMU, choices_main, item_chosen_main)

    def item_chosen_clf(button):
        global source
        global grid_search_clfs
        if button.label == config.LBL_LREGR:
            source.set_clf(source.Clf.LREGR)
            oracle_selection()
        elif button.label == config.LBL_LIN_SVC:
            source.set_clf(source.Clf.LIN_SVC)
            oracle_selection()
        elif button.label == config.LBL_SVC:
            source.set_clf(source.Clf.SVC)
            oracle_selection()
        elif button.label == config.LBL_DTREE:
            source.set_clf(source.Clf.DTREE)
            oracle_selection()
        elif button.label == config.LBL_RFOREST:
            source.set_clf(source.Clf.RFOREST)
            oracle_selection()
        elif button.label == config.LBL_BAYES:
            source.set_clf(source.Clf.BAYES)
            oracle_selection()
        elif button.label == config.LBL_KNN:
            source.set_clf(source.Clf.KNN)
            oracle_selection()
        elif button.label == config.LBL_CMT_GRD:
            grid_search_clfs.append(source.Clf.LREGR)
            print_to_status("Lin. regression committed to grid.")
            grid_search_clfs.append(source.Clf.LIN_SVC)
            print_to_status("Lin. SVC committed to grid.")
            grid_search_clfs.append(source.Clf.DTREE)
            print_to_status("Decision tree committed to grid.")
            grid_search_clfs.append(source.Clf.RFOREST)
            print_to_status("Random Forest committed to grid.")
            grid_search_clfs.append(source.Clf.BAYES)
            print_to_status("Bayes committed to grid.")
            grid_search_clfs.append(source.Clf.KNN)
            print_to_status("K-nearest Neighbours committed to grid.")

            oracle_selection()
        elif button.label == config.LBL_UCMT_GRD:
            grid_search_clfs = []
            print_to_status("Classifiers removed from grid search.")
            oracle_selection()
        elif button.label == config.LBL_BACK:
            oracle_selection()

    def clf_selection():
        choices_clf = [config.LBL_LREGR, config.LBL_LIN_SVC, config.LBL_SVC,
            config.LBL_DTREE, config.LBL_RFOREST, config.LBL_BAYES, config.LBL_KNN,
            config.LBL_CMT_GRD if len(grid_search_clfs) == 0 else config.LBL_UCMT_GRD,
            config.LBL_BACK]
        clfs = menu(config.LBL_HDR_CLF, choices_clf, item_chosen_clf)
        main.original_widget = clfs

    def item_chosen_samps(button):
        global samples
        global grid_search_samps
        if button.label == config.LBL_16:
            samples = 16
            print_to_status("Using 16 samples")
            oracle_selection()
        elif button.label == config.LBL_32:
            samples = 32
            print_to_status("Using 32 samples")
            oracle_selection()
        elif button.label == config.LBL_64:
            samples = 64
            print_to_status("Using 64 samples")
            oracle_selection()
        elif button.label == config.LBL_128:
            samples = 128
            print_to_status("Using 128 samples")
            oracle_selection()
        elif button.label == config.LBL_ALL:
            samples = -1
            print_to_status("Using all samples")
            oracle_selection()
        elif button.label == config.LBL_CMT_GRD:
            grid_search_samps.append(16)
            print_to_status("16 samples committed to grid.")
            grid_search_samps.append(32)
            print_to_status("32 samples committed to grid.")
            grid_search_samps.append(64)
            print_to_status("64 samples committed to grid.")
            grid_search_samps.append(128)
            print_to_status("128 samples committed to grid.")
            grid_search_samps.append(-1)
            print_to_status("All to sample committed to grid.")

            oracle_selection()
        elif button.label == config.LBL_UCMT_GRD:
            grid_search_samps = []
            print_to_status("Samples removed from grid search.")
            oracle_selection()
        elif button.label == config.LBL_BACK:
            oracle_selection()

    def samples_selection():
        choices_samp = [config.LBL_16, config.LBL_32, config.LBL_64,
            config.LBL_128, config.LBL_ALL,
            config.LBL_CMT_GRD if len(grid_search_samps) == 0 else config.LBL_UCMT_GRD,
            config.LBL_BACK]
        samps = menu(config.LBL_HDR_SAMP, choices_samp, item_chosen_samps)
        main.original_widget = samps

    def item_chosen_oversamps(button):
        global oversampling
        global grid_search_oversampling
        if button.label == config.LBL_ON:
            print_to_status("Using oversampling")
            oversampling = True
            oracle_selection()
        elif button.label == config.LBL_OFF:
            print_to_status("Not using oversampling")
            oversampling = False
            oracle_selection()
        elif button.label == config.LBL_CMT_GRD:
            grid_search_oversampling = True
            print_to_status("Oversampling committed to grid.")

            oracle_selection()
        elif button.label == config.LBL_UCMT_GRD:
            grid_search_oversampling = False
            print_to_status("Oversampling removed from grid search.")
            oracle_selection()
        elif button.label == config.LBL_BACK:
            oracle_selection()

    def oversampling_selection():
        choices_osamp = [config.LBL_ON, config.LBL_OFF,
            config.LBL_CMT_GRD if grid_search_oversampling == False else config.LBL_UCMT_GRD,
            config.LBL_BACK]
        osamps = menu(config.LBL_OVERSAMP, choices_osamp, item_chosen_oversamps)
        main.original_widget = osamps

    def item_chosen_static(button):
        global static_compression
        if button.label == config.LBL_ON:
            print_to_status("Using static compression")
            static_compression = True
            config_selection()
        elif button.label == config.LBL_OFF:
            print_to_status("Not using static compression")
            static_compression = False
            config_selection()
        elif button.label == config.LBL_BACK:
            config_selection()

    def static_selection():
        if learn_every_pkt:
            choices_static = [config.LBL_ON, config.LBL_OFF, config.LBL_BACK]
        else:
            choices_static = [config.LBL_ON, config.LBL_OFF, config.LBL_BACK]
        static = menu(config.LBL_STATIC, choices_static, item_chosen_static)
        main.original_widget = static

    def item_chosen_config(button):
        if button.label == config.LBL_STATIC:
            static_selection()
        elif button.label == config.LBL_BACK:
            return_to_main()

    def config_selection():
        choices_config = [config.LBL_STATIC, config.LBL_BACK]
        configs = menu(config.LBL_SETUP, choices_config, item_chosen_config)
        main.original_widget = configs

    def item_chosen_online(button):
        global online
        global grid_online
        global learn_every_pkt
        if button.label == config.LBL_ON:
            print_to_status("Using online learning")
            online = True
            oracle_selection()
        elif button.label == config.LBL_OFF:
            print_to_status("Not using online learning")
            online = False
            oracle_selection()
        elif button.label == config.LBL_EACH_PKT:
            print_to_status("Learning with each packet")
            learn_every_pkt = True
            oracle_selection()
        elif button.label == config.LBL_LIMIT_PKT:
            print_to_status("Learning with after every power 2 packet")
            learn_every_pkt = False
            oracle_selection()
        elif button.label == config.LBL_CMT_GRD:
            grid_online = True
            print_to_status("Online learning added to grid search.")
            oracle_selection()
        elif button.label == config.LBL_UCMT_GRD:
            grid_online = False
            print_to_status("Online learning removed from grid search.")
            oracle_selection()
        elif button.label == config.LBL_BACK:
            oracle_selection()

    def online_selection():
        global learn_every_pkt
        global grid_online
        if learn_every_pkt:
            choices_online = [config.LBL_ON, config.LBL_OFF,
                config.LBL_LIMIT_PKT,
                config.LBL_CMT_GRD if grid_online == False else config.LBL_UCMT_GRD,
                config.LBL_BACK]
        else:
            choices_online = [config.LBL_ON, config.LBL_OFF,
                config.LBL_EACH_PKT,
                config.LBL_CMT_GRD if grid_online == False else config.LBL_UCMT_GRD,
                config.LBL_BACK]
        online = menu(config.LBL_ONLINE, choices_online, item_chosen_online)
        main.original_widget = online

    def item_chosen_oracle(button):
        if button.label == config.LBL_CLF:
            clf_selection()
        elif button.label == config.LBL_SAMP:
            samples_selection()
        elif button.label == config.LBL_OVERSAMP:
            oversampling_selection()
        elif button.label == config.LBL_ONLINE:
            online_selection()
        elif button.label == config.LBL_BACK:
            return_to_main()

    def oracle_selection():
        choices_setup = [config.LBL_CLF, config.LBL_SAMP, config.LBL_OVERSAMP,
            config.LBL_ONLINE, config.LBL_BACK]
        setup = menu(config.LBL_ORACLE, choices_setup, item_chosen_oracle)
        main.original_widget = setup

    def item_chosen_hdrs(button):
        global pcap_path
        global grid_search_hdr_flows
        if button.label == config.LBL_IP_V4:
            pcap_path = [(config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_IP_V4]
            print_to_status("Using " + config.PATH_IP_V4)
            flow_selection()
        elif button.label == config.LBL_IP_V6:
            pcap_path = [(config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_IP_V6]
            print_to_status("Using " + config.PATH_IP_V6)
            flow_selection()
        elif button.label == config.LBL_UDP_V4:
            pcap_path = [(config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_UDP_V4]
            print_to_status("Using " + config.PATH_UDP_V4)
            flow_selection()
        elif button.label == config.LBL_UDP_V6:
            pcap_path = [(config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_UDP_V6]
            print_to_status("Using " + config.PATH_UDP_V6)
            flow_selection()
        elif button.label == config.LBL_RTP_EASY_V4:
            pcap_path = [(config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_RTP_EASY_V4]
            print_to_status("Using " + config.PATH_RTP_EASY_V4)
            flow_selection()
        elif button.label == config.LBL_RTP_EASY_V6:
            pcap_path = [(config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_RTP_EASY_V6]
            print_to_status("Using " + config.PATH_RTP_EASY_V6)
            flow_selection()
        elif button.label == config.LBL_RTP_HARD_V4:
            pcap_path = [(config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_RTP_HARD_V4]
            print_to_status("Using " + config.PATH_RTP_HARD_V4)
            flow_selection()
        elif button.label == config.LBL_RTP_HARD_V6:
            pcap_path = [(config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_RTP_HARD_V6]
            print_to_status("Using " + config.PATH_RTP_HARD_V6)
            flow_selection()
        elif button.label == config.LBL_CMT_GRD:
            grid_search_hdr_flows.append((config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_IP_V4)
            print_to_status(config.PATH_IP_V4 + " committed to grid.")
            grid_search_hdr_flows.append((config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_IP_V6)
            print_to_status(config.PATH_IP_V6 + " committed to grid.")
            grid_search_hdr_flows.append((config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_UDP_V4)
            print_to_status(config.PATH_UDP_V4 + " committed to grid.")
            grid_search_hdr_flows.append((config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_UDP_V6)
            print_to_status(config.PATH_UDP_V6 + " committed to grid.")
            grid_search_hdr_flows.append((config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_RTP_EASY_V4)
            print_to_status(config.PATH_RTP_EASY_V4 + " committed to grid.")
            grid_search_hdr_flows.append((config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_RTP_EASY_V6)
            print_to_status(config.PATH_RTP_EASY_V6 + " committed to grid.")
            grid_search_hdr_flows.append((config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_RTP_HARD_V4)
            print_to_status(config.PATH_RTP_HARD_V4 + " committed to grid.")
            grid_search_hdr_flows.append((config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_RTP_HARD_V6)
            print_to_status(config.PATH_RTP_HARD_V6 + " committed to grid.")

            use_all_app_streams = False
            flow_selection()
        elif button.label == config.LBL_UCMT_GRD:
            grid_search_hdr_flows = []
            print_to_status("Header streams removed from grid search.")
            use_all_app_streams = False
            flow_selection()
        elif button.label == config.LBL_BACK:
            flow_selection()

    def headers_selection():
        choices_hdr = [config.LBL_IP_V4, config.LBL_IP_V6, config.LBL_UDP_V4,
            config.LBL_UDP_V6, config.LBL_RTP_EASY_V4, config.LBL_RTP_EASY_V6,
            config.LBL_RTP_HARD_V4, config.LBL_RTP_HARD_V6,
            config.LBL_CMT_GRD if len(grid_search_hdr_flows) == 0 else config.LBL_UCMT_GRD,
            config.LBL_BACK]
        hdrs = menu(config.LBL_HDR_HDRS, choices_hdr, item_chosen_hdrs)
        main.original_widget = hdrs

    def item_chosen_streams(button):
        global pcap_path
        global grid_search_app_flows
        if button.label == config.LBL_FRANKA:
            pcap_path = [(config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_FRANKA]
            use_all_app_streams = False
            print_to_status("Using " + config.PATH_FRANKA)
            flow_selection()
        elif button.label == config.LBL_ASTERISK:
            pcap_path = [(config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_ASTERISK]
            use_all_app_streams = False
            print_to_status("Using " + config.PATH_ASTERISK)
            flow_selection()
        elif button.label == config.LBL_MIKI:
            pcap_path = [(config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_MIKI]
            use_all_app_streams = False
            print_to_status("Using " + config.PATH_MIKI)
            flow_selection()
        elif button.label == config.LBL_VLC:
            pcap_path = [(config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_VLC]
            use_all_app_streams = False
            print_to_status("Using " + config.PATH_VLC)
            flow_selection()
        elif button.label == config.LBL_EKIGA:
            pcap_path = [(config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_EKIGA]
            use_all_app_streams = False
            print_to_status("Using " + config.PATH_EKIGA)
            flow_selection()
        elif button.label == config.LBL_COAP:
            pcap_path = [(config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_COAP]
            use_all_app_streams = False
            print_to_status("Using " + config.PATH_COAP)
            flow_selection()
        elif button.label == config.LBL_CMT_GRD:
            grid_search_app_flows.append((config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_FRANKA)
            print_to_status(config.PATH_FRANKA + " committed to grid.")

            grid_search_app_flows.append((config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_ASTERISK)
            print_to_status(config.PATH_ASTERISK + " committed to grid.")

            grid_search_app_flows.append((config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_MIKI)
            print_to_status(config.PATH_MIKI + " committed to grid.")

            grid_search_app_flows.append((config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_VLC)
            print_to_status(config.PATH_VLC + " committed to grid.")

            grid_search_app_flows.append((config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_EKIGA)
            print_to_status(config.PATH_EKIGA + " committed to grid.")

            use_all_app_streams = False
            flow_selection()
        elif button.label == config.LBL_UCMT_GRD:
            grid_search_app_flows = []
            print_to_status("Application streams removed from grid search.")
            use_all_app_streams = False
            flow_selection()
        elif button.label == config.LBL_BACK:
            flow_selection()

    def streams_selection():
        choices_strms = [config.LBL_FRANKA, config.LBL_ASTERISK, config.LBL_MIKI,
            config.LBL_VLC, config.LBL_EKIGA, config.LBL_COAP,
            config.LBL_CMT_GRD if len(grid_search_app_flows) == 0 else config.LBL_UCMT_GRD,
            config.LBL_ALL, config.LBL_BACK,]
        strms = menu(config.LBL_STRMS, choices_strms, item_chosen_streams)
        main.original_widget = strms

    def item_chosen_multi(button):
        global pcap_path
        if button.label == config.LBL_ALL:
            use_all_app_streams = True
            print_to_status("Using all streams")
            pcap_path = [
                (config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_FRANKA,
                (config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_ASTERISK,
                (config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_MIKI,
                (config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_VLC,
                (config.PATH_DOCKER_ROOT if opts.docker else "") + config.PATH_EKIGA]
            flow_selection()
        elif button.label == config.LBL_BACK:
            flow_selection()

    def multi_selection():
        choices_multi = [config.LBL_ALL, config.LBL_BACK]
        multi = menu(config.LBL_MULTI, choices_multi, item_chosen_multi)
        main.original_widget = multi

    def item_chosen_flow(button):
        if button.label == config.LBL_HDRS:
            headers_selection()
        if button.label == config.LBL_STRMS:
            streams_selection()
        if button.label == config.LBL_MULTI:
            multi_selection()
        if button.label == config.LBL_BACK:
            return_to_main()

    def flow_selection():
        choices_flow = [config.LBL_HDRS, config.LBL_STRMS, config.LBL_MULTI, config.LBL_BACK]
        flows = menu(config.LBL_FLOW, choices_flow, item_chosen_flow)
        main.original_widget = flows

    def item_chosen_send(button):
        global pcap_path
        global oversampling
        global static_compression
        global online
        global learn_every_pkt
        if button.label == config.LBL_SEND_UC:
            source.send_uncompressed(collect_gains, set_progress)
            set_bar_progress(100)
            return_to_main()
        if button.label == config.LBL_SEND_CO:
            print_to_status("This might take a while...", True)
            if online == False:
                source.train_oracle(samples, oversampling, static_compression)
            print_to_status("", True)
            if online == False:
                source.send_compressed(collect_gains, set_progress)
            else:
                source.send_adaptive(learn_every_pkt, oversampling, static_compression, collect_gains, set_progress)
            set_bar_progress(100)
            return_to_main()
        if button.label == config.LBL_SEND_L:
            source.send_on_demand(learn_every_pkt, oversampling, static_compression,
                                  int(opts.source_port), opts.dummy_sources, collect_gains)
            return_to_main()
        if button.label == config.LBL_BACK:
            return_to_main()

    def send_selection():
        choices_send = [config.LBL_SEND_UC, config.LBL_SEND_CO,
            config.LBL_SEND_L, config.LBL_BACK]
        send = menu(config.LBL_SEND, choices_send, item_chosen_send)
        main.original_widget = send
        source.connect()
        print_to_status("This might take a while...", True)
        source.load_pcap(*pcap_path)
        print_to_status("", True)
        clear_gains()
        set_bar_progress(-1)

    def grid_search():
        global pcap_path

        # determine streams
        sources = grid_search_app_flows + grid_search_hdr_flows
        if len(sources) < 1:
            sources = pcap_path

        clfs = grid_search_clfs if len(grid_search_clfs) > 1 else [source.get_clf()]
        samps = grid_search_samps if len(grid_search_samps) > 1 else [samples]
        ovs = [False, True] if grid_search_oversampling == True else [oversampling]
        modes = 1 if grid_online else 0 #2 if grid_online else 0

        span = (len(sources) * len(clfs) * len(samps) * len(ovs)
                + modes * len(sources) * len(clfs) * len(ovs))

        if span <= 1:
            print_to_status("Nothing committed to grid search!")
        else:
            source.connect()

            import csv
            with open('grid_search_results.csv', 'w') as csvfile:
                c = csv.writer(csvfile, delimiter=',')
                c.writerows([['stream', 'gain', 'conf_gain_low', 'conf_gain_high',
                              'uc_sz', 'co_sz', 'conf_co_sz_low', 'conf_co_sz_high',
                              'clf', 'dt_train', 'conf_dt_train_low', 'conf_dt_train_high', 'dt_pred',
                              'conf_dt_pred_low', 'conf_dt_pred_high',
                              'samples', 'oversampling', 'classes', 'conf_classes_low',
                              'conf_classes_high', 'mode']])
                k = 0
                for c_ovs in ovs:
                    for c_clf in clfs:
                        source.set_clf(c_clf)

                        for c_samp in samps:
                            for i in range(0, len(sources)):
                                print_to_status("Grid Search " + str(k+1) + "/" + str(span), True)

                                pcap_path = [sources[i]]
                                source.load_pcap(*pcap_path)

                                for m in range(0, modes+1): #3):
                                    print_to_status(str(m), False)
                                    if m == 0:
                                        print_to_status("Offline phase", False)
                                        dt_trains = []
                                        classeses = []
                                        for t in range(0, 10):
                                            print_to_status("Training " + str(t+1) + "/10", False)
                                            dt_train, classes = source.train_oracle(c_samp, c_ovs, static_compression)
                                            dt_trains.append(dt_train)
                                            classeses.append(classes)
                                        #print_to_status("Training iterations finished...", False)

                                        conf_dt_train = st.t.interval(0.95, len(dt_trains),
                                                                      loc=np.mean(dt_trains), scale=st.sem(dt_trains))
                                        conf_classes = [0, 0]

                                        x1 = classeses[0]
                                        all_eq = True
                                        for x in classeses:
                                            if x1 != x:
                                                all_eq = False
                                                break
                                        conf_classes = [classeses[0], classeses[0]]
                                        if not all_eq:
                                            conf_classes = st.t.interval(0.95, len(classeses),
                                                                         loc=np.mean(classeses), scale=st.sem(classeses))

                                        r = source.send_compressed(collect_gains, set_progress,
                                                                   surpress_status_print=True)
                                        gain            = r[0]
                                        conf_gain       = r[1]
                                        uc_sz           = r[2]
                                        co_sz           = r[3]
                                        conf_co_sz      = r[4]
                                        dt_pred         = r[5]
                                        conf_dt_pred    = r[6]

                                        stream_name = os.path.splitext(os.path.basename(sources[i]))[0]
                                        c.writerows([[stream_name, str(gain), conf_gain[0],
                                                    conf_gain[1], str(uc_sz), str(co_sz),
                                                    conf_co_sz[0], conf_co_sz[1],
                                                    c_clf.value,
                                                    np.mean(dt_trains), conf_dt_train[0], conf_dt_train[1],
                                                    dt_pred, conf_dt_pred[0], conf_dt_pred[1],
                                                    c_samp, c_ovs, np.mean(classeses),
                                                    conf_classes[0], conf_classes[1], 'offline']])
                                    elif m == 1:
                                        # online learning doesn't use samples
                                        if c_samp == samps[0]:
                                            print_to_status("Online phase", False)
                                            r = source.send_adaptive(False, c_ovs, static_compression,
                                                                     collect_gains, set_progress,
                                                                     surpress_status_print=True)
                                            dt_train        = r[0]
                                            conf_dt_train   = r[1]
                                            classes         = r[2]
                                            conf_classes    = r[3]
                                            gain            = r[4]
                                            conf_gain       = r[5]
                                            uc_sz           = r[6]
                                            co_sz           = r[7]
                                            conf_co_sz      = r[8]
                                            dt_pred         = r[9]
                                            conf_dt_pred    = r[10]

                                            c.writerows([[stream_name, str(gain), conf_gain[0],
                                                    conf_gain[1], str(uc_sz), str(co_sz),
                                                    conf_co_sz[0], conf_co_sz[1],
                                                    c_clf.value,
                                                    dt_train, conf_dt_train[0], conf_dt_train[1],
                                                    dt_pred, conf_dt_pred[0], conf_dt_pred[1],
                                                    -1, c_ovs, classes,
                                                    conf_classes[0], conf_classes[1], 'online']])
                                    else:
                                        if c_samp == samps[0]:
                                            print_to_status("Packet-by-packet phase", False)
                                            r = source.send_adaptive(True, c_ovs, static_compression,
                                                                     collect_gains, set_progress,
                                                                     surpress_status_print=True)
                                            dt_train        = r[0]
                                            conf_dt_train   = r[1]
                                            classes         = r[2]
                                            conf_classes    = r[3]
                                            gain            = r[4]
                                            conf_gain       = r[5]
                                            uc_sz           = r[6]
                                            co_sz           = r[7]
                                            conf_co_sz      = r[8]
                                            dt_pred         = r[9]
                                            conf_dt_pred    = r[10]

                                            c.writerows([[stream_name, str(gain), conf_gain[0],
                                                    conf_gain[1], str(uc_sz), str(co_sz),
                                                    conf_co_sz[0], conf_co_sz[1],
                                                    c_clf.value,
                                                    dt_train, conf_dt_train[0], conf_dt_train[1],
                                                    dt_pred, conf_dt_pred[0], conf_dt_pred[1],
                                                    -1, c_ovs, classes,
                                                    conf_classes[0], conf_classes[1], 'packet-by-packet']])
                                k += 1

            print_to_status("", True)
            clear_gains()
            set_bar_progress(-1)
        return_to_main()

    loop.run()
else:
    sink = config.Sink(opts.ip, int(opts.port))
    sink.connect_and_receive()
