import os
import sys
import time
import select
import random
import socket
import threading
from enum import Enum
import scapy.all as scapy
import pandas as pd
import numpy as np
import scipy.stats as st

from o2sc import compressor
from o2sc import decompressor
from o2sc.classification import model

from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier

#from sklearn.utils.testing import ignore_warnings
#from sklearn.exceptions import ConvergenceWarning
#import warnings


LBL_HDR_CLF = "Choose a classifier"
LBL_LREGR   = "Log. Regression"
LBL_LIN_SVC = "Linear SVC"
LBL_SVC     = "SVC"
LBL_DTREE   = "Decision Tree"
LBL_RFOREST = "Random Forest"
LBL_BAYES   = "Naive Bayes"
LBL_KNN     = "K Nearest Neigh."

LBL_HDR_SAMP= "Training size"
LBL_16      = "16"
LBL_32      = "32"
LBL_64      = "64"
LBL_128     = "128"
LBL_ALL     = "All"

LBL_OVERSAMP= "Oversampling"
LBL_ON      = "On"
LBL_OFF     = "Off"

LBL_ONLINE      = "Online learning"
LBL_ON          = "On"
LBL_OFF         = "Off"
LBL_EACH_PKT    = "Learn by packet"
LBL_LIMIT_PKT   = "Learn sparsely"

LBL_HDR_HDRS    = "Choose headers"
LBL_IP_V4       = "IPv4"
LBL_IP_V6       = "IPv6"
LBL_UDP_V4      = "UDP IPv4"
LBL_UDP_V6      = "UDP IPv6"
LBL_RTP_EASY_V4 = "Easy RTP IPv4"
LBL_RTP_EASY_V6 = "Easy RTP IPv6"
LBL_RTP_HARD_V4 = "Hard RTP IPv4"
LBL_RTP_HARD_V6 = "Hard RTP IPv6"

LBL_HDR_EMU = "O2SC Compressor"
LBL_CONNECT = "Connect"
LBL_CLF     = "Classifier"
LBL_FLOW    = "Flow Setup"
LBL_HDRS    = "Header Streams"
LBL_STRMS   = "Application Streams"
LBL_MULTI   = "Multiplexed Streams"
LBL_SAMP    = "Samples"
LBL_TRAIN   = "Train"
LBL_ORACLE  = "Oracle Setup"
LBL_SETUP   = "Compressor Configuration"
LBL_SEND_UC = "Send Uncompressed"
LBL_SEND_CO = "Send Compressed"
LBL_SEND_L  = "Listen for packets"
LBL_SEND    = "Send"
LBL_GRID    = "Grid Search"
LBL_EXIT    = "Exit"
LBL_BACK    = "Back"

LBL_STATIC  = "Static Compression"

LBL_FRANKA  = "Robotic Arm"
LBL_ASTERISK= "VoIP"
LBL_MIKI    = "TCP Acknowledgment"
LBL_VLC     = "Audio Streaming"
LBL_EKIGA   = "Audio Conferencing"
LBL_COAP    = "IoT"

LBL_CMT_GRD = "Commit to Grid"
LBL_UCMT_GRD= "Uncommit from Grid"

UC_STREAM_ID = 0x00
CO_STREAM_ID = 0x01
STOP_ID      = 0xff

PATH_IP_V4          = "../pcaps/headers/easyIp_noPayl.pcap"
PATH_IP_V6          = "../pcaps/headers/easyIpv6_noPayl.pcap"
PATH_UDP_V4         = "../pcaps/headers/easyUdp_noPayl.pcap"
PATH_UDP_V6         = "../pcaps/headers/easyUdpv6_noPayl.pcap"
PATH_RTP_EASY_V4    = "../pcaps/headers/easyRtpNoCsrc_noPayl.pcap"
PATH_RTP_EASY_V6    = "../pcaps/headers/easyRtpv6NoCsrc_noPayl.pcap"
PATH_RTP_HARD_V4    = "../pcaps/headers/hardRtpNoCsrc_noPayl_randTs.pcap"
PATH_RTP_HARD_V6    = "../pcaps/headers/hardRtpv6NoCsrc_noPayl_randTs.pcap"

PATH_FRANKA         = "../pcaps/streams/franka_2000.pcap"
PATH_ASTERISK       = "../pcaps/streams/asterisk_rtp_downstream_240.pcap"
PATH_MIKI           = "../pcaps/streams/radio_miki_acks_66_no_options_0_ip_id_fix_wnd_sz.pcap"
PATH_VLC            = "../pcaps/streams/vlc_v6_mts_gsm_s16l_ts_rtp_2_fixed_pkt_size_1_2000.pcap"
PATH_EKIGA          = "../pcaps/streams/Ekiga_test_rtp_only_downstream_filtered.pcap"
PATH_COAP           = "../pcaps/streams/coap_ack.pcap"

PATH_DOCKER_ROOT    = "/root/o2sc/emulator/"

class Source:
    __print_func    = None

    __ip            = None
    __port          = None
    __tcp_socket    = None
    __packets       = None
    __is_dummy      = None
    __docker_root   = False

    __oracle = model.BasicModel()
    __comp   = compressor.Compressor()

    def __init__(self, ip, port, docker_root=False, print_func=print, is_dummy=False):
        self.__print_func   = print_func
        self.__ip           = ip
        self.__port         = port
        self.__is_dummy     = is_dummy
        self.__docker_root  = docker_root

    def connect(self):
        if self.__is_dummy == False:
            self.__tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.__print_func("Connecting to " + self.__ip + ":" + str(self.__port))
            self.__tcp_socket.connect((self.__ip, self.__port))
            self.__print_func("Connected")

    def disconnect(self):
        if self.__is_dummy == False and self.__tcp_socket != None:
            print("Closing connection")
            if self.__tcp_socket.sendall(bytearray(list([STOP_ID]))) != None:
                self.print_func("Network error!")
                #raise "Network error!"
            self.__tcp_socket.close()
            self.__tcp_socket = None
        self.__print_func("Disconnected")

    def load_pcap(self, *pcap_paths):
        try:
            for path in pcap_paths:
                self.__print_func("Reading " + str(path))
                self.__packets = scapy.rdpcap(path)
        except:
            self.__print_func("Pcap not found!")

    class Clf(Enum):
        LREGR   = 1
        LIN_SVC = 2
        SVC     = 3
        DTREE   = 4
        RFOREST = 5
        BAYES   = 6
        KNN     = 7

    __selected_clf = Clf.LREGR
    def set_clf(self, clf):
        if clf == self.Clf.LREGR:
            self.__oracle = model.BasicModel(LogisticRegression(solver='lbfgs'))
            self.__print_func("Using sklearn.linear_model.LogisticRegression")
            self.__selected_clf = self.Clf.LREGR
        elif clf == self.Clf.LIN_SVC:
            self.__oracle = model.BasicModel(LinearSVC())
            self.__print_func("Using sklearn.svm.LinearSVC")
            self.__selected_clf = self.Clf.LIN_SVC
        elif clf == self.Clf.SVC:
            self.__oracle = model.BasicModel(SVC())
            self.__print_func("Using sklearn.svm.SVC")
            self.__selected_clf = self.Clf.SVC
        elif clf == self.Clf.DTREE:
            self.__oracle = model.BasicModel(DecisionTreeClassifier())
            self.__print_func("Using sklearn.tree.DecisionTreeClassifier")
            self.__selected_clf = self.Clf.DTREE
        elif clf == self.Clf.RFOREST:
            self.__oracle = model.BasicModel(RandomForestClassifier())
            self.__print_func("Using sklearn.ensemble.RandomForestClassifier")
            self.__selected_clf = self.Clf.RFOREST
        elif clf == self.Clf.BAYES:
            self.__oracle = model.BasicModel(GaussianNB())
            self.__print_func("Using sklearn.naive_bayes.GaussianNB")
            self.__selected_clf = self.Clf.BAYES
        elif clf == self.Clf.KNN:
            self.__oracle = model.BasicModel(KNeighborsClassifier())
            self.__print_func("Using sklearn.neighbors.KNeighborsClassifier")
            self.__selected_clf = self.Clf.KNN

    def get_clf(self):
        return self.__selected_clf

    #@ignore_warnings(category=ConvergenceWarning)
    def train_oracle(self, samples=-1, oversample=True, static_compression=False, reset_compressor=True, non_pcap_source=False):
        # reset compressor
        if reset_compressor:
            self.__comp = compressor.Compressor(static_compression=static_compression)
        self.__print_func("Training oracle with " + ("all" if samples == -1 else str(samples)) + " packet(s)")

        if samples > 0:
            if non_pcap_source == False:
                pkts = self.__oracle.convert_pcap_to_df(self.__packets)[0:samples]
            else:
                pkts = pd.DataFrame(self.__packets)[0:samples]
        else:
            if non_pcap_source == False:
                pkts = self.__oracle.convert_pcap_to_df(self.__packets)
            else:
                pkts = pd.DataFrame(self.__packets)[0:samples]
        st = int(round(time.time() * 1000))
        #with warnings.catch_warnings():
        #    warnings.filterwarnings("ignore", category=ConvergenceWarning)
        self.__oracle.train(pkts, oversample=oversample, split=False)
        dt = int(round(time.time() * 1000)) - st
        self.__print_func("Oracle trained in " + str(dt) + "ms w/ " + str(self.__oracle.get_class_count()) + " class(es)")
        return [dt, self.__oracle.get_class_count()]

    def send_uncompressed(self, packet_gain_callback=None, progress_callback=None):
        self.__print_func("Sending uncompressed stream...")
        try:
            for pktsIndex in range(0, len(self.__packets)):
                uc_pkt  = list(bytearray(bytes(self.__packets[pktsIndex])))
                if packet_gain_callback != None:
                    packet_gain_callback(1.0 - len(uc_pkt) / len(uc_pkt))
                if progress_callback != None:
                    progress_callback(pktsIndex / len(self.__packets))
                uc_pkt.insert(0, UC_STREAM_ID)
                if self.__is_dummy == False:
                    if self.__tcp_socket.sendall(bytearray(uc_pkt)) != None:
                        self.__print_func("Network error!")
            self.__print_func("Transmission finished")
            stats = "Compression gain: 0.0"
            stats += " | Uncompressed packet size: " + str(len(uc_pkt))
            self.__print_func(stats, to_stats_bar=True)
        except:
            self.__print_func("Transmission error!")

    def send_compressed(self, packet_gain_callback=None, progress_callback=None,
                        surpress_status_print=False):
        self.__print_func("Sending compressed stream...")
        self.__comp.reset_gain()
        co_pkts_lens = 0
        uc_pkts_lens_arr = []
        co_pkts_lens_arr = []
        dt_predicts_arr  = []

        try:
            for pktsIndex in range(0, len(self.__packets)):
                #os.system('sleep 0.2')
                uc_pkt  = list(bytearray(bytes(self.__packets[pktsIndex])))
                strt = int(round(time.time() * 1000))
                pattern = self.__oracle.predict(uc_pkt)
                dt_predicts_arr.append(int(round(time.time() * 1000)) - strt)
                co_pkt  = self.__comp.compress(uc_pkt, pattern)
                if packet_gain_callback != None:
                    packet_gain_callback(1.0 - len(co_pkt) / len(uc_pkt))
                if progress_callback != None:
                    progress_callback(pktsIndex / len(self.__packets))
                co_pkts_lens += len(co_pkt)
                uc_pkts_lens_arr.append(len(uc_pkt))
                co_pkts_lens_arr.append(len(co_pkt))
                co_pkt.insert(0, CO_STREAM_ID)
                if self.__is_dummy == False:
                    if self.__tcp_socket.sendall(bytearray(co_pkt)) != None:
                        self.__print_func("Network error!")
            self.__print_func("Transmission finished")
        except Exception as e:
            self.__print_func(str(e))
            self.__print_func("Transmission error!")

        stats = "Compression gain: " + str(self.__comp.get_gain()[0])[0:4]
        stats += " | Uncompressed packet size: " + str(len(uc_pkt))
        stats += " | Average compressed packet size: " + str(co_pkts_lens // len(self.__packets))
        if not surpress_status_print:
            self.__print_func(stats, to_stats_bar=True)

        gains = 1.0 - np.divide(co_pkts_lens_arr, uc_pkts_lens_arr)

        conf_gain = st.t.interval(0.95, len(gains),
                                  loc=np.mean(gains), scale=st.sem(gains))
        conf_co_sz = st.t.interval(0.95, len(co_pkts_lens_arr),
                                   loc=np.mean(co_pkts_lens_arr), scale=st.sem(co_pkts_lens_arr))
        conf_dt_pred = (st.t.interval(0.95, len(dt_predicts_arr),
                                   loc=np.mean(dt_predicts_arr), scale=st.sem(dt_predicts_arr))
                                   if np.mean(dt_predicts_arr) != 0 else [0, 0])

        return [self.__comp.get_gain()[0], conf_gain, len(uc_pkt),
                co_pkts_lens // len(self.__packets), conf_co_sz,
                np.mean(dt_predicts_arr), conf_dt_pred]

    def __learn_online(self, packet_index, learn_each=False, oversample=True,
                       static_compression=False, non_pcap_source=False):
        if learn_each == False:
            for power in [2**8, 2**7, 2**6, 2**5, 2**4, 2**3, 2**2, 2**1, 2**0]:
                if packet_index+1 == power:
                    for i in range(packet_index+1, 0, -1):
                        try:
                            self.train_oracle(i, oversample=oversample,
                                static_compression=static_compression,
                                reset_compressor=(True if packet_index == 0 else False),
                                non_pcap_source=non_pcap_source)
                            break
                        except Exception as e:
                            if i == 1:
                                raise Exception("Training failed!")
                            else:
                                continue
        else:
            for i in range(packet_index+1, 0, -1):
                try:
                    self.train_oracle(i, oversample=oversample,
                        static_compression=static_compression,
                        reset_compressor=(True if packet_index == 0 else False),
                        non_pcap_source=non_pcap_source)
                    break
                except Exception as e:
                    if i == 1:
                        raise Exception("Training failed!")
                    else:
                        continue

    def send_adaptive(self, learn_each=False, oversample=True, static_compression=False,
                      packet_gain_callback=None, progress_callback=None, surpress_status_print=False):
        self.__print_func("Sending adaptively compressed stream...")
        self.__comp.reset_gain()
        co_pkts_lens = 0
        uc_pkts_lens_arr = []
        co_pkts_lens_arr = []
        dt_predicts_arr  = []
        dt_trains_arr  = []
        dt_classes_arr  = []
        try:
            for pktsIndex in range(0, len(self.__packets)):
                #os.system('sleep 0.2')

                strt = int(round(time.time() * 1000))
                self.__learn_online(pktsIndex, learn_each, oversample, static_compression)
                dt_trains_arr.append(int(round(time.time() * 1000)) - strt)
                dt_classes_arr.append(self.__oracle.get_class_count())

                uc_pkt  = list(bytearray(bytes(self.__packets[pktsIndex])))
                strt = int(round(time.time() * 1000))
                pattern = self.__oracle.predict(uc_pkt)
                dt_predicts_arr.append(int(round(time.time() * 1000)) - strt)
                co_pkt  = self.__comp.compress(uc_pkt, pattern)
                if packet_gain_callback != None:
                    packet_gain_callback(1.0 - len(co_pkt) / len(uc_pkt))
                if progress_callback != None:
                    progress_callback(pktsIndex / len(self.__packets))
                co_pkts_lens += len(co_pkt)
                uc_pkts_lens_arr.append(len(uc_pkt))
                co_pkts_lens_arr.append(len(co_pkt))
                co_pkt.insert(0, CO_STREAM_ID)
                if self.__is_dummy == False:
                    if self.__tcp_socket.sendall(bytearray(co_pkt)) != None:
                        self.__print_func("Network error!")
            self.__print_func("Transmission finished")
        except Exception as e:
            self.__print_func("Transmission error: " + str(str(e)))

        stats = "Compression gain: " + str(self.__comp.get_gain()[0])[0:4]
        stats += " | Average uncompressed packet size: " + str(len(uc_pkt))
        stats += " | Average compressed packet size: " + str(co_pkts_lens // len(self.__packets))
        if not surpress_status_print:
            self.__print_func("bla", to_stats_bar=True)

        gains = 1.0 - np.divide(co_pkts_lens_arr, uc_pkts_lens_arr)
        conf_gain = st.t.interval(0.95, len(gains),
                                  loc=np.mean(gains), scale=st.sem(gains))
        conf_co_sz = st.t.interval(0.95, len(co_pkts_lens_arr),
                                   loc=np.mean(co_pkts_lens_arr), scale=st.sem(co_pkts_lens_arr))
        conf_dt_pred = (st.t.interval(0.95, len(dt_predicts_arr),
                                   loc=np.mean(dt_predicts_arr), scale=st.sem(dt_predicts_arr))
                                   if np.mean(dt_predicts_arr) != 0 else [0, 0])
        conf_dt_train = st.t.interval(0.95, len(dt_trains_arr),
                                   loc=np.mean(dt_trains_arr), scale=st.sem(dt_trains_arr))
        conf_classes = st.t.interval(0.95, len(dt_classes_arr),
                                     loc=np.mean(dt_classes_arr), scale=st.sem(dt_classes_arr))

        return [np.mean(dt_trains_arr), conf_dt_train, np.mean(dt_classes_arr),
                conf_classes, self.__comp.get_gain()[0], conf_gain, len(uc_pkt),
                co_pkts_lens // len(self.__packets), conf_co_sz,
                np.mean(dt_predicts_arr), conf_dt_pred]

    __packets_demultiplexed = {}
    __oracles = {}
    __compressors = {}
    def __store_packet(self, packet):
        if len(packet) in self.__packets_demultiplexed:
            self.__packets_demultiplexed[len(packet)].append(packet)
        else:
            self.__packets_demultiplexed[len(packet)] = [packet]
        self.__packets = self.__packets_demultiplexed[len(packet)]
        return len(self.__packets_demultiplexed[len(packet)])-1

    def __fetch_context(self, packet):
        if len(packet) not in self.__oracles:
            self.__print_func("Creating new context...")
            #os.system('sleep 5')
            self.__oracles[len(packet)]     = model.BasicModel()
            self.__compressors[len(packet)] = compressor.Compressor()
        self.__oracle   = self.__oracles[len(packet)]
        self.__comp     = self.__compressors[len(packet)]

    def __persist_context(self, packet):
        self.__oracles[len(packet)]     = self.__oracle
        self.__compressors[len(packet)] = self.__comp

    def send_on_demand(self, learn_each=False, oversample=True, static_compression=False,
                       source_port=50000, dummy_sources=False, packet_gain_callback=None):
        self.__print_func("Waiting for packets...")
        self.__comp.reset_gain()
        co_pkts_lens = 0
        uc_pkts_lens = 0

        try:
            udp_source = UDPIngress(source_port, docker_root=self.__docker_root,
                                    dummy=dummy_sources, print_func=self.__print_func)
            udp_source.connect()

            while True:
                uc_pkt = udp_source.receive()
                if uc_pkt is None:
                    break

                pkt_index = self.__store_packet(uc_pkt)
                uc_pkts_lens += len(uc_pkt)

                self.__fetch_context(uc_pkt)

                self.__learn_online(pkt_index, learn_each, oversample,
                                    static_compression, non_pcap_source=True)

                #self.__print_func(str(len(uc_pkt)))

                #self.__print_func(str(len(self.__packets)))

                #self.__oracle
                pattern = self.__oracle.predict(uc_pkt)
                self.__persist_context(uc_pkt)
                co_pkt  = self.__comp.compress(uc_pkt, pattern)

                if packet_gain_callback != None:
                    packet_gain_callback(1.0 - len(co_pkt) / len(uc_pkt))

                co_pkts_lens += len(co_pkt)
                co_pkt.insert(0, CO_STREAM_ID)
                if self.__is_dummy == False:
                    if self.__tcp_socket.sendall(bytearray(co_pkt)) != None:
                        self.__print_func("Network error!")

            if uc_pkts_lens > 0:
                stats = "Compression gain: " + str(self.__comp.get_gain()[0])[0:4]
                stats += " | Uncompressed packet size: " + str(uc_pkts_lens // len(self.__packets))
                stats += " | Average compressed packet size: " + str(co_pkts_lens // len(self.__packets))
                self.__print_func(stats, to_stats_bar=True)
            else:
                stats = "No packets received!"
                self.__print_func(stats, to_stats_bar=True)
        except Exception as e:
            #self.__print_func("Transmission error: " + str(str(e)))
            raise e

class UDPOutgress:
    __print_func    = None

    __ip            = None
    __port          = None
    __udp_socket    = None
    __connection    = None
    __sources       = []
    __docker_root   = False
    __flow_idx      = 0

    def __init__(self, ip, port, flow_idx=0, docker_root=False, print_func=print):
        self.__print_func   = print_func
        self.__ip           = ip
        self.__port         = port
        self.__docker_root  = docker_root
        self.__flow_idx     = flow_idx

    def __load_pcaps(self, *pcap_paths):
        try:
            self.__packets = []
            for path in pcap_paths:
                self.__print_func("Reading " + str(path))
                x = scapy.rdpcap(path)
                self.__sources.append({'pkts' : x, 'cnt' : 0})
        except Exception as e:
            self.__print_func(str(e))

    def connect(self):
        self.__udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
        self.__udp_socket.connect((self.__ip, int(self.__port)))

        pcap_path = [
            (PATH_DOCKER_ROOT if self.__docker_root else "") + PATH_FRANKA,
            (PATH_DOCKER_ROOT if self.__docker_root else "") + PATH_ASTERISK,
            (PATH_DOCKER_ROOT if self.__docker_root else "") + PATH_MIKI,
            (PATH_DOCKER_ROOT if self.__docker_root else "") + PATH_VLC,
            (PATH_DOCKER_ROOT if self.__docker_root else "") + PATH_EKIGA]
        self.__load_pcaps(pcap_path[self.__flow_idx])

    def send(self):
        for i in self.__sources[0]['pkts']:
            os.system('sleep 0.1')
            self.__print_func("Sending " + str(len(i)) + " bytes")
            #try:
            self.__udp_socket.send(bytearray(bytes(i)))
            #except:
        self.__udp_socket.close()

class UDPIngress:
    __print_func    = None
    __port          = None
    __udp_socket    = None
    __dummy         = None
    __dummy_sources = []
    __docker_root   = False
    __buffer        = []
    __end           = False
    __initial_wait  = True

    def __init__(self, port, docker_root=False, dummy=False, print_func=print):
        self.__print_func   = print_func
        self.__port         = port
        self.__dummy        = dummy
        self.__docker_root  = docker_root

    def __load_pcaps(self, *pcap_paths):
        try:
            self.__packets = []
            for path in pcap_paths:
                self.__print_func("Reading " + str(path))
                x = scapy.rdpcap(path)
                self.__dummy_sources.append({'pkts' : x, 'cnt' : 0})
        except:
            self.__print_func("Pcap not found!")

    def background_receive(self):
        #data, addr = self.__udp_socket.recvfrom(1024)
        while True:
            ready = select.select([self.__udp_socket], [], [], (60 if self.__initial_wait else 10))
            if ready[0]:
                data, addr = self.__udp_socket.recvfrom(1400)
            else:
                self.__end = True
                break
            #self.__print_func('Received ' + str(len(data)) + " byte(s) from " + str(addr))
            #os.system('sleep 0.01')
            #return list(bytearray(bytes(data)))
            self.__buffer.append(list(bytearray(bytes(data))))

    def connect(self):
        if self.__dummy == False:
            self.__udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.__udp_socket.bind(('', int(self.__port)))
            self.__print_func("Listening on " + str(self.__port))

            if self.__dummy == False:
                thread = threading.Thread(target=self.background_receive, args=[])
                thread.start()
        else:
            pcap_path = [
                (PATH_DOCKER_ROOT if self.__docker_root else "") + PATH_FRANKA,
                (PATH_DOCKER_ROOT if self.__docker_root else "") + PATH_ASTERISK,
                (PATH_DOCKER_ROOT if self.__docker_root else "") + PATH_MIKI,
                (PATH_DOCKER_ROOT if self.__docker_root else "") + PATH_VLC,
                (PATH_DOCKER_ROOT if self.__docker_root else "") + PATH_EKIGA]
            self.__load_pcaps(*pcap_path)

    def receive(self):
        if self.__dummy == False:
            pkt = None
            if self.__end == False:
                while len(self.__buffer) == 0:
                    if self.__end:
                        return None
                    pass
                pkt = self.__buffer[0]
                self.__buffer.pop(0)
                return pkt
            else:
                return None
        else:
            os.system('sleep 0.01')
            while len(self.__dummy_sources) > 0:
                k = random.randint(0, len(self.__dummy_sources)-1)
                if self.__dummy_sources[k]['cnt'] >= len(self.__dummy_sources[k]['pkts']):
                    del self.__dummy_sources[k]
                    continue
                else:
                    i = self.__dummy_sources[k]['cnt']
                    p = list(bytearray(bytes(self.__dummy_sources[k]['pkts'][i])))
                    self.__dummy_sources[k]['cnt'] += 1
                    return p
            else:
                return None

class Sink:
    __print_func    = None

    __ip            = None
    __port          = None
    __tcp_socket    = None
    __connection    = None

    __decomp    = decompressor.Decompressor()

    def __init__(self, ip, port, print_func=print):
        self.__print_func = print_func
        self.__ip = ip
        self.__port = port

    def connect_and_receive(self):
        self.__tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__tcp_socket.bind((self.__ip, int(self.__port)))
        self.__print_func("Listening on " + self.__ip + ":" + str(self.__port))
        self.__tcp_socket.listen()
        self.__connection, address = self.__tcp_socket.accept()
        self.__print_func('Connection establised', address)

        with self.__connection:
            self.__print_func('Receiving uncompressed stream...')
            is_co_stream = False
            while True:
                data = self.__connection.recv(1500)
                if data:
                    self.__print_func(data[0])
                    if is_co_stream == False and data[0] == CO_STREAM_ID:
                        is_co_stream = True
                        self.__print_func('Receiving compressed stream...')
                    elif data[0] == STOP_ID:
                        self.disconnect()
                        sys.exit(0)

    def disconnect(self):
        if self.__tcp_socket != None:
            print("Closing connection")
            self.__tcp_socket.close()
            self.__tcp_socket = None
        self.__print_func("Disconnected")
