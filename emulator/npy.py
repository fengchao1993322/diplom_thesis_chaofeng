

#I'm playing around with the urwid library and it's been pretty great so far. But i can't get the Progressbar to work. I wrote a simple test program like this:

import os
import urwid

palette = [
    ('banner', 'dark gray', 'white', 'black')]

    # a function that takes some times to complete
def dosomething(steps, bar):
    bar.done = steps
    for i in range(steps + 1):
        bar.set_completion(i)
        os.system('sleep 0.2')
        loop.draw_screen()


    # function called when the task button is clicked
def on_task_clicked(button):
    dosomething(10, pbar)


btn2 = urwid.Button('Button >')
task_btn = urwid.Button('Task >')
urwid.connect_signal(task_btn, 'click', on_task_clicked)

pbar = urwid.ProgressBar('bg', 'banner')
pile = urwid.Pile([task_btn, urwid.Padding(pbar, 'left', 40)])
top = urwid.Filler(pile, valign='top')

loop = urwid.MainLoop(top, palette)
loop.run()