 #! /bin/bash

AS_SRC=0
NODE=1
DECOMP=0
PATH_EMU="o2sc/emulator/emulator.py"
FLAGS_COMP="-S 50000 -D"
FLAGS_SRC="-sD 50000 -a 10.0.0.1 -p 50000 -f"

# process command line arguments
OPTIND=1
while getopts "n:dh" opt; do
    case $opt in
    n) NODE=$OPTARG;AS_SRC=1;;
    d) DECOMP=1;;
    h) printf "Usage: node.sh [OPTIONS...]\n\n";
       exit;;
    esac
done

shift $(($OPTIND - 1))

if [[ $# -gt 0 ]]; then
    args=1
    arg=$@
fi

if [[ $AS_SRC -gt 0 ]]; then
    echo "Starting source node #"$NODE
	cmd="python3 "$PATH_EMU" "$FLAGS_SRC" "$NODE
    $($cmd)
else
    if [[ $DECOMP -gt 0 ]]; then
        echo "Starting as decompressor"
    else
        echo "Starting as compressor"
	    cmd="python3 "$PATH_EMU" "$FLAGS_COMP
        echo $cmd
        $($cmd)
    fi
fi
