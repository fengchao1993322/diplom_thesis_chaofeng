 #! /bin/python3

import os
import sys
import optparse


parser = optparse.OptionParser(usage="usage: node.py [options]")

parser.add_option("-n", "--node",
                action="store", dest="node", default="0",
                help="source node number.")
parser.add_option("-d", "--decompressor",
                action="store_true", dest="decomp", default=False,
                help="start as decompressor.")


AS_SRC=0
PATH_EMU="o2sc/emulator/emulator.py"
FLAGS_COMP="-S 50000 -D"
FLAGS_SRC="-sD 50000 -a 10.0.0.1 -p 50000 -f"

(opts, args) = parser.parse_args(sys.argv)

if int(opts.node) > 0: 
    print("Starting source node #" + opts.node)
    os.system("python3 " + PATH_EMU + " " + FLAGS_SRC + " " + opts.node)
else:
    if opts.decomp:
        print("Starting as decompressor")
    else:
        print("Starting as compressor")
        os.system("python3 " + PATH_EMU + " " + FLAGS_COMP)
