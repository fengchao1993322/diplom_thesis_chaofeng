from o2sc.classification import model
from test import tester

import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import scapy.all as scapy

class TestStaticHardRTP():
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        packets_train = scapy.rdpcap('pcaps/headers/hardRtpNoCsrc_noPayl_randTs_train.pcap')
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle, static_compression=True)

    def test_gain_ge_76(self):
        packets_test = scapy.rdpcap('pcaps/headers/hardRtpNoCsrc_noPayl_randTs_test.pcap')
        g = self.tester.get_gain(packets_test, is_array=True)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.76