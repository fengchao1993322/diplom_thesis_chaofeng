from o2sc import compressor
from o2sc import decompressor
from o2sc import oracle
from o2sc.classification import model

import pandas as pd
import scapy.all as scapy
import random

from test import tester

packets = [
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x00, 0xf0],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x00, 0xfa],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x00, 0xfb],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x00, 0xfc],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x00, 0xfd],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x00, 0xfe],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x00, 0xff],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x01],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x02],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x03],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x04],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x05],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x06],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x07],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x08],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x09],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x0a],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x0b],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x0c],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x0d],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x0e],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x0f]]

class OneClassOracle(oracle.Oracle):
    def train(self, packets=None):
        pass

    def predict(self, packet=None):
        if packet == None:
            raise "No packet specified"
        return '003111'

class TestOneClassPattern:
    oracle = OneClassOracle()
    tester = None

    def setup(self):
        self.oracle.train(pd.DataFrame(packets))
        self.tester = tester.Tester(self.oracle)

    def test_one_class_pattern(self):
        g = self.tester.get_gain(pd.DataFrame(packets))
        print("Gain = " + str(g[0][0]))
        print("Co Packets : " + str(g[2]))
        assert g[0][0] >= -1.0

class TwoClassOracle(oracle.Oracle):
    prevPacket = None
    def train(self, packets=None):
        pass

    def predict(self, packet=None):
        if packet == None:
            raise "No packet specified"
        pattern = ''
        if self.prevPacket == None or packet[4] == self.prevPacket[4]:
            pattern = '003001'
        else:
            pattern = '003011'
        self.prevPacket = packet
        return pattern

class TestTwoClassPattern:
    oracle = TwoClassOracle()
    tester = None

    def setup(self):
        self.oracle.train(pd.DataFrame(packets))
        self.tester = tester.Tester(self.oracle)

    def test_two_class_pattern(self):
        g = self.tester.get_gain(pd.DataFrame(packets))
        print("Gain = " + str(g[0][0]))
        print("Co Packets : " + str(g[2]))
        assert g[0][0] >= -1.0

class RandomOracle(oracle.Oracle):
    def train(self, packets=None):
        pass

    def predict(self, packet=None):
        if packet == None:
            raise "No packet specified"
        return  ''.join(tuple('3' for _ in range(len(packet))))

class TestRandomPattern:
    oracle = RandomOracle()
    tester = None

    def setup(self):
        self.oracle.train(pd.DataFrame(packets))
        self.tester = tester.Tester(self.oracle)

    def test_random_pattern(self):
        g = self.tester.get_gain(pd.DataFrame(packets))
        print("Gain = " + str(g[0][0]))
        print("Co Packets : " + str(g[2]))
        assert g[0][0] >= -1.0

class SequentialOracle(oracle.Oracle):
    def train(self, packets=None):
        pass

    def predict(self, packet=None):
        if packet == None:
            raise "No packet specified"
        return  ''.join(tuple('1' for _ in range(len(packet))))

class TestSequentialPattern:
    oracle = SequentialOracle()
    tester = None

    def setup(self):
        self.oracle.train(pd.DataFrame(packets))
        self.tester = tester.Tester(self.oracle)

    def test_sequential_pattern(self):
        g = self.tester.get_gain(pd.DataFrame(packets))
        print("Gain = " + str(g[0][0]))
        print("Co Packets : " + str(g[2]))
        assert g[0][0] >= -1.0

class StaticOracle(oracle.Oracle):
    def train(self, packets=None):
        pass

    def predict(self, packet=None):
        if packet == None:
            raise "No packet specified"
        return  ''.join(tuple('0' for _ in range(len(packet))))

class TestStaticPattern:
    oracle = StaticOracle()
    tester = None

    def setup(self):
        self.oracle.train(pd.DataFrame(packets))
        self.tester = tester.Tester(self.oracle)

    def test_static_pattern(self):
        g = self.tester.get_gain(pd.DataFrame(packets))
        print("Gain = " + str(g[0][0]))
        print("Co Packets : " + str(g[2]))
        assert g[0][0] >= -1.0

class TestDecompression:
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        self.oracle.train(pd.DataFrame(packets))
        self.tester = tester.Tester(self.oracle)

    def test_decompression(self):
        g = self.tester.get_gain(pd.DataFrame(packets))
        print("Gain = " + str(g[0][0]))
        print("Co Packets : " + str(g[2]))
        assert g[0][0] >= -1.0
