from o2sc.classification import model
from test import tester

import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import scapy.all as scapy


class TestOutOfByteRange():
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        packets_train = scapy.rdpcap('pcaps/streams/asterisk_rtp_downstream_240_test_100_1000.pcap')
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train)[0:10])
        self.tester = tester.Tester(self.oracle)

    def test_out_of_byte(self):
        pkts_test = scapy.rdpcap('pcaps/streams/asterisk_rtp_downstream_240_test_100_1000.pcap')
        g = self.tester.get_gain(pkts_test, is_array=True)
        print("Gain = " + str(g[0][0]))

class TestFrankaNoOversampling16():
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        packets_train = scapy.rdpcap('pcaps/streams/franka_2000.pcap')
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train)[0:16],
                          oversample=False, split=False)
        self.tester = tester.Tester(self.oracle)

    def test_funcitioning(self):
        pkts_test = scapy.rdpcap('pcaps/streams/franka_2000.pcap')
        g = self.tester.get_gain(pkts_test, is_array=True)
        print("Gain = " + str(g[0][0]))