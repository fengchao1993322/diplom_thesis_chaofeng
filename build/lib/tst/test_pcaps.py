from tst import pcap_tester

import numpy as np
import matplotlib.pyplot as plt
import scipy.stats

class TestEasyRtp():
    tester = None

    def setup(self):
        self.tester = pcap_tester.PcapTester('pcaps/easyRtp_train_100.pcap', 'pcaps/easyRtp_test_100_1000.pcap')
        self.tester.build_model()

    def test_gain_ge_93(self):
        g = self.tester.get_gain()
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.93

class TestHardRtp():
    tester = None

    def setup(self):
        self.tester = pcap_tester.PcapTester('pcaps/hardRtpNoCsrcFixedPayl_train_100.pcap', 'pcaps/hardRtpNoCsrcFixedPayl_test_100_1000.pcap')
        self.tester.build_model()

    def test_gain_ge_90(self):
        g = self.tester.get_gain()
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.9

class TestFranka:
    tester  = None

    def setup(self):
        self.tester = pcap_tester.PcapTester('pcaps/franka_train_50.pcap', 'pcaps/franka_test_100_1100.pcap')
        self.tester.build_model()

    def test_gain_ge_34(self):
        g = self.tester.get_gain()
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.34

class TestAsterisk():
    tester = None

    def setup(self):
        self.tester = pcap_tester.PcapTester('pcaps/asterisk_rtp_downstream_240_train_100.pcap', 'pcaps/asterisk_rtp_downstream_240_test_100_1000.pcap')
        self.tester.build_model()

    def test_gain_ge_48(self):
        g = self.tester.get_gain()
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        self.tester.oracle.score()
        assert g[0][0] >= 0.48

class TestRadioMiki():
    tester = None

    def setup(self):
        self.tester = pcap_tester.PcapTester('pcaps/radio_miki_acks_66_no_options_0_ip_id_fix_wnd_sz_train_100.pcap', 'pcaps/radio_miki_acks_66_no_options_0_ip_id_fix_wnd_sz_test_100.pcap')
        self.tester.build_model()

    def test_gain_ge_84(self):
        g = self.tester.get_gain()
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.84

class TestVlc():
    tester = None

    def setup(self):
        self.tester = pcap_tester.PcapTester('pcaps/vlc_v6_mts_gsm_s16l_ts_rtp_2_fixed_pkt_size_train_99.pcap', 'pcaps/vlc_v6_mts_gsm_s16l_ts_rtp_2_fixed_pkt_size_test_100.pcap')
        self.tester.build_model()

    def test_gain_ge_16(self):
        g = self.tester.get_gain()
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.16

class TestEkiga():
    tester = None

    def setup(self):
        self.tester = pcap_tester.PcapTester('pcaps/Ekiga_test_rtp_only_downstream_filtered_train_100.pcap', 'pcaps/Ekiga_test_rtp_only_downstream_filtered_test_100.pcap')
        self.tester.build_model()

    def test_gain_ge_11(self):
        g = self.tester.get_gain()
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.115


def setup_module(module):
    # figure size
    plt.rcParams['figure.figsize'] = (15.0, 8.0)
    font = {#'family' : 'latex',
            #'weight' : 'bold',
            'size'   : 20}
    #plt.rcParams['text.usetex'] = True
    plt.rc('font', **font)

def teardown_module(module):
    plot_gain()
    plot_size()

def mean_confidence_interval(data, confidence=0.95):
    a = 1.0 * np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)
    return m, h, h

gains = []
def plot_gain():
    fig, ax = plt.subplots()

    plt.ylim([-0,1])

    #plt.xlabel(r"Compression Gain")
    plt.ylabel(r"Gain")
    plt.grid()

    gs = [gains[0][0][0], gains[1][0][0], gains[2][0][0], gains[3][0][0], gains[4][0][0], gains[5][0][0], gains[6][0][0]]

    xAxis = [ 0, 1, 2, 3, 4, 5, 6 ]
    plt.xticks(xAxis, (r'Easy RTP', r'Hard RTP', r'Franka', r'Asterisk', r'Radio', r'VLC', r'Ekiga'))

    plt.bar(xAxis, gs, color='red')

    plt.savefig('plt/o2sc_gains.eps', bbox_inches='tight')
    plt.savefig('plt/o2sc_gains.pdf', bbox_inches='tight')
    plt.savefig('plt/o2sc_gains.png', bbox_inches='tight')

def plot_size():
    fig, ax = plt.subplots()

    plt.grid()

    #plt.xlabel(r"Compression Gain")
    plt.ylabel(r"Packet Size (byte)")

    bar_width = 0.35
    plt.rcParams['hatch.linewidth'] = 2.0

    #plt.xticks(xAxis + bar_width*1.5, ip_labels)

    co = [gains[0][0][1], gains[1][0][1], gains[2][0][1], gains[3][0][1], gains[4][0][1], gains[5][0][1], gains[6][0][1]]
    co_co = [mean_confidence_interval(gains[0][2])[1], mean_confidence_interval(gains[1][2])[1],
        mean_confidence_interval(gains[2][2])[1], mean_confidence_interval(gains[3][2])[1],
        mean_confidence_interval(gains[4][2])[1], mean_confidence_interval(gains[5][2])[1],
        mean_confidence_interval(gains[6][2])[1]]
    #[mean_confidence_interval(gain_franka[2])[1], mean_confidence_interval(gain_asterisk[2])[1], mean_confidence_interval(gain_radio[2])[1], mean_confidence_interval(gain_vlc[2])[1], mean_confidence_interval(gain_Ekiga[2])[1]]
    uc = [gains[0][0][2], gains[1][0][2], gains[2][0][2], gains[3][0][2], gains[4][0][2], gains[5][0][2], gains[6][0][2]]
    uc_co = [mean_confidence_interval(gains[0][1])[2], mean_confidence_interval(gains[1][1])[2],
        mean_confidence_interval(gains[2][1])[2], mean_confidence_interval(gains[3][1])[2],
        mean_confidence_interval(gains[4][1])[2], mean_confidence_interval(gains[5][1])[2],
        mean_confidence_interval(gains[6][1])[2]]
    #[mean_confidence_interval(gain_franka[1])[1], mean_confidence_interval(gain_asterisk[1])[1], mean_confidence_interval(gain_radio[1])[1], mean_confidence_interval(gain_vlc[1])[1], mean_confidence_interval(gain_Ekiga[1])[1]]
    mean_confidence_interval
    xAxis = np.arange(len(co))

    plt.xticks(xAxis + bar_width*0.5, (r'Easy RTP', r'Hard RTP', r'Franka', r'Asterisk', r'Radio', r'VLC', r'Ekiga'))

    plt.bar(xAxis, uc, bar_width, color='red', label='Uncompressed', yerr=uc_co)
    plt.bar(xAxis + bar_width, co, bar_width, color='green', hatch='o', label='Compressed', yerr=co_co)

    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.10), frameon=False,
            fancybox=False, shadow=False, ncol=2, markerscale=1, numpoints=1)

    plt.savefig('plt/o2sc_bytes.eps', bbox_inches='tight')
    plt.savefig('plt/o2sc_bytes.pdf', bbox_inches='tight')
    plt.savefig('plt/o2sc_bytes.png', bbox_inches='tight')
