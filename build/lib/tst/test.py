from o2sc import oracle
from o2sc import compressor
from o2sc import decompressor

import scapy.all as scapy

class TestCompression:
    m = None
    pcap_train = 'pcaps/easyRtp_train_100.pcap'
    pcap_test  = 'pcaps/easyRtp_test_100_1000.pcap'

    def setup(self):
        self.m = oracle.Model(self.pcap_train)



    def test_1(self):
        assert self.get_gain(self.pcap_test, self.m)

    def get_gain(self, pcap_test, model, debug=False):
        c = compressor.Compressor()
        d = decompressor.Decompressor()
        c.debug_info_on = debug
        d.debug_info_on = debug

        uc_packet_lens = []
        co_packet_lens = []
        pattern_changes    = 0
        pattern_violations = 0

        pkts_test = scapy.rdpcap(pcap_test)
        for pktsIndex in range(0, len(pkts_test)):
            if debug:
                print(pktsIndex)
            uc_pkt = list(bytearray(bytes(pkts_test[pktsIndex])))
            if debug:
                print("Uncompressed: " + str(uc_pkt))
            pattern = model.predict(uc_pkt)
            #print(pattern)
            co_pkt = c.compress(uc_pkt, pattern)
            pattern_changes += 1 if c.is_pattern_changed else 0
            pattern_violations += 1 if c.is_pattern_violated else 0
            uc_packet_lens.append(len(uc_pkt))
            co_packet_lens.append(len(co_pkt))
            if debug:
                print("Compressed: " + str(co_pkt))
            de_pkt = d.decompress(co_pkt)
            if debug:
                print("Decompressed: " + str(de_pkt))
            bytes_different = 0
            for i in range(len(de_pkt)):
                if uc_pkt[i] != de_pkt[i]:
                    print(str(i) + " = " + str(uc_pkt[i]) + " vs. " + str(de_pkt[i]))
                    bytes_different += 1

            if bytes_different > 0:
                print("--------------------------------------------------------")
                #print(uc_pkt)
                #print(de_pkt)
                c.print_contexts()
                print("Difference " + str(bytes_different))
                break

        print("Gain: " + str(c.get_gain()))
        print("Solve for delta overflow!!")
        #return [c.get_gain(), uc_packet_lens, co_packet_lens, pattern_changes, pattern_violations]
        return True

#test_numbers_3_4()