from o2sc import compressor
from o2sc import decompressor
from o2sc.classification import model

import scapy.all as scapy

class PcapTester:
    oracle       = model.BasicModel()
    pcap_train  = None
    pcap_test   = None
    comp        = None
    decomp      = None
    debug       = False

    def __init__(self, pcap_train, pcap_test, debug=False):
        if pcap_train == None or pcap_test == None:
            raise Exception("Pcaps need to be specified!!")
        self.pcap_train = pcap_train
        self.pcap_test  = pcap_test

        self.comp   = compressor.Compressor()
        self.decomp = decompressor.Decompressor()

        self.comp.debug_info_on     = debug
        self.decomp.debug_info_on   = debug

    def build_model(self):
        self.oracle.train(self.pcap_train)

    def get_gain(self):
        if self.oracle == None:
            raise Exception("Train the model first!")

        uc_packet_lens = []
        co_packet_lens = []
        pattern_changes    = 0
        pattern_violations = 0

        pkts_test = scapy.rdpcap(self.pcap_test)
        for pktsIndex in range(0, len(pkts_test)):
            if self.debug:
                print(pktsIndex)
            uc_pkt = list(bytearray(bytes(pkts_test[pktsIndex])))
            if self.debug:
                print("Uncompressed: " + str(uc_pkt))
            pattern = self.oracle.predict(uc_pkt)
            #print(pattern)
            co_pkt = self.comp.compress(uc_pkt, pattern)
            pattern_changes += 1 if self.comp.is_pattern_changed else 0
            pattern_violations += 1 if self.comp.is_pattern_violated else 0
            uc_packet_lens.append(len(uc_pkt))
            co_packet_lens.append(len(co_pkt))
            if self.debug:
                print("Compressed: " + str(co_pkt))
            de_pkt = self.decomp.decompress(co_pkt)
            if self.debug:
                print("Decompressed: " + str(de_pkt))
            bytes_different = 0
            for i in range(len(de_pkt)):
                if uc_pkt[i] != de_pkt[i]:
                    print(str(i) + " = " + str(uc_pkt[i]) + " vs. " + str(de_pkt[i]))
                    bytes_different += 1

            if bytes_different > 0:
                print("--------------------------------------------------------")
                #print(uc_pkt)
                #print(de_pkt)
                self.comp.print_contexts()
                print("Difference " + str(bytes_different))
                break

        return [self.comp.get_gain(), uc_packet_lens, co_packet_lens, pattern_changes, pattern_violations]
