from o2sc import oracle

import scapy.all as scapy
import numpy as np
import pandas as pd
from collections import Counter

import o2sc.classification.summaryFunc as sFunc


class PcapProcessingOracle(oracle.Oracle):

    def transform_to_y(self, x, y_prime):
        # get dictionary of classes
        classes = self.get_classes(y_prime)

        # check if classification is needed
        if len(classes) > 1:
            # convert to processable class IDs
            y = self.to_class_ids(classes, y_prime)
            return y, classes
            # multi-classification
            #y_binarized = sklearn.preprocessing.label_binarize(y_prime, classes=self.__classes)
        else:
            return tuple(0 for _ in range(len(x))), classes

    def oversample(self, x, y_prime):
        # create input pattern
        x_out = x
        y_prime_out = y_prime
        osm = sFunc.overSamplingMethod(methodChosen='naiveRandom')
        try:
            x_out, y_prime_out = osm.dataInput(x, y_prime)
        except:
            # oversampling not possible
            pass

        return x_out, y_prime_out

    def get_classes(self, y_prime):
        return sorted(Counter(y_prime))

    def to_class_ids(self, labels, y_prime):
        return sFunc.labels2Num(labels, y_prime)

    def split(self, x, y, ratio, random_state=0):
        # split to traning and testing sets
        x_train, x_test, y_train, y_test = sFunc.trainTestSeparate(
                x, y, ratio, random_state=random_state)
        #_, _, y_train_m, y_test_m = sFunc.trainTestSeparate(
        #    x, y_binarized, ratio, random_state=random_state)

        #X_train_std, X_test_std = sFunc.dataStandardization(x_train, x_test)
        #_, pktsDataframe_std = sFunc.dataStandardization(x_train, x)
        return x_train, x_test, y_train, y_test

    def convert_pcap_to_df(self, pkts):
        for pktsIndex in range(len(pkts)):
            pkt = pkts[pktsIndex]
            pktBytes = bytes(pkt)
            if pktsIndex == 0:
                pktBytearray = bytearray(pktBytes)
            else:
                pktBytearrayAdding = bytearray(pktBytes)
                pktBytearray = np.vstack((pktBytearray, pktBytearrayAdding))

        pktsDataframe = pd.DataFrame(pktBytearray)
        pktsDataframeFloat = pktsDataframe.astype(float)
        return pktsDataframe

    def transform_to_y_prime(self, pktsDataframe):
        resultPkts2dArray = np.empty([len(pktsDataframe), pktsDataframe.shape[1]])
        '''
        In the package data preparation,
        0 means static;
        1 means increment;
        2 means decrement;
        3 means random.
        '''
        for columnIndexOfPktsDataframe in range(pktsDataframe.shape[1]):
            NumOfIncr = 0
            NumOfDecr = 0
            NumOfCons = 0
            Incr = False
            Decr = False
            Cons = False
            # 'changingNum' means the byte info changing times, except the first packet.
            changingNum = 0
            for rowIndexOfPktsDataframe in range(len(pktsDataframe)):
                if rowIndexOfPktsDataframe == 0:
                    pass
                else:
                    if pktsDataframe.iat[
                            rowIndexOfPktsDataframe,
                            columnIndexOfPktsDataframe] == pktsDataframe.iat[
                                rowIndexOfPktsDataframe -
                                1, columnIndexOfPktsDataframe]:
                        if rowIndexOfPktsDataframe ==1:
                            resultPkts2dArray[rowIndexOfPktsDataframe-1,
                                        columnIndexOfPktsDataframe] = 0
                            NumOfCons += 1
                            Cons = True
                        resultPkts2dArray[rowIndexOfPktsDataframe,
                                        columnIndexOfPktsDataframe] = 0
                        NumOfCons += 1
                        Cons = True
                    elif pktsDataframe.iat[
                            rowIndexOfPktsDataframe,
                            columnIndexOfPktsDataframe] > pktsDataframe.iat[
                                rowIndexOfPktsDataframe -
                                1, columnIndexOfPktsDataframe]:
                        if ((pktsDataframe.iat[rowIndexOfPktsDataframe,
                                            columnIndexOfPktsDataframe] == 255)
                                or
                            (pktsDataframe.iat[rowIndexOfPktsDataframe,
                                            columnIndexOfPktsDataframe] == 254)
                            ) and (pktsDataframe.iat[rowIndexOfPktsDataframe -
                                                    1, columnIndexOfPktsDataframe]
                                == 0):
                            if rowIndexOfPktsDataframe == 1:
                                resultPkts2dArray[rowIndexOfPktsDataframe-1,
                                            columnIndexOfPktsDataframe] = 2
                                NumOfDecr += 1
                                Decr = True
                            resultPkts2dArray[rowIndexOfPktsDataframe,
                                            columnIndexOfPktsDataframe] = 2
                            NumOfDecr += 1
                            Decr = True
                        else:
                            if rowIndexOfPktsDataframe == 1:
                                resultPkts2dArray[rowIndexOfPktsDataframe-1,
                                            columnIndexOfPktsDataframe] = 1
                                NumOfIncr += 1
                                Incr = True
                            resultPkts2dArray[rowIndexOfPktsDataframe,
                                            columnIndexOfPktsDataframe] = 1
                            NumOfIncr += 1
                            Incr = True
                    else:
                        if (pktsDataframe.iat[rowIndexOfPktsDataframe,
                                            columnIndexOfPktsDataframe] == 0
                            ) and ((pktsDataframe.
                                    iat[rowIndexOfPktsDataframe -
                                        1, columnIndexOfPktsDataframe] == 255) or
                                (pktsDataframe.
                                    iat[rowIndexOfPktsDataframe -
                                        1, columnIndexOfPktsDataframe] == 254)):
                            if rowIndexOfPktsDataframe == 1:
                                resultPkts2dArray[rowIndexOfPktsDataframe-1,
                                            columnIndexOfPktsDataframe] = 1
                                NumOfIncr += 1
                                Incr = True
                            resultPkts2dArray[rowIndexOfPktsDataframe,
                                            columnIndexOfPktsDataframe] = 1
                            NumOfIncr += 1
                            Incr = True
                        else:
                            if rowIndexOfPktsDataframe == 1:
                                resultPkts2dArray[rowIndexOfPktsDataframe-1,
                                            columnIndexOfPktsDataframe] = 2
                                NumOfDecr += 1
                                Decr = True
                            resultPkts2dArray[rowIndexOfPktsDataframe,
                                            columnIndexOfPktsDataframe] = 2
                            NumOfDecr += 1
                            Decr = True

                if rowIndexOfPktsDataframe == 1:
                    if resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe] == 0:
                        posiState = 0
                    elif resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe] == 1:
                        posiState = 1
                    elif resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe] == 2:
                        posiState = 2

                if rowIndexOfPktsDataframe > 1:
                    if resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe] == posiState:
                        pass
                    else:
                        posiState = resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe]
                        changingNum +=1
            '''
                'changingNum<=2' limits the continuity, if changingNum is big, then the byte position random
            '''
            if ((Incr and Decr) or (Incr and Cons) or (Cons and Decr))and(changingNum > 2):
                resultPkts2dArray[:, columnIndexOfPktsDataframe] = 3
                incrPercentage = float(NumOfIncr) / len(pktsDataframe)
                decrPercentage = float(NumOfDecr) / len(pktsDataframe)
                consPercentage = float(NumOfCons) / len(pktsDataframe)
                # print("random column's Index : ", columnIndexOfPktsDataframe,
                #       '; Total quantity of this column elements: ',
                #       len(pktsDataframe))
                # print('posiStateChangingTimes: ', changingNum)
                # print('Increment number: ', NumOfIncr, '; Percentage: ',
                #       incrPercentage)
                # print('Decrement number: ', NumOfDecr, '; Percentage: ',
                #       decrPercentage)
                # print('Constant number: ', NumOfCons, '; Percentage: ',
                #       consPercentage)

        # print("result Pkts 2d Array: ", resultPkts2dArray)
        indexArrayOfPktsDataframe = ["" for x in range(len(resultPkts2dArray))]
        #["" for x in range(len(pktsDataFloat))]
        #np.empty(len(pktsDataFloat), dtype = str)

        resultPkts2dArrayInt = resultPkts2dArray.astype(int)
        #print(resultPkts2dArrayInt)
        for i in range(resultPkts2dArrayInt.shape[0]):
            resultPkts2dArrayStr = [
                str(elemt) for elemt in resultPkts2dArrayInt[i]
            ]
            indexArrayOfPktsDataframe[i] = "".join(resultPkts2dArrayStr)
        return indexArrayOfPktsDataframe