class Oracle:
    def __init__(self):
        pass

    def train(self, packets=None):
        raise Exception("train() not implemented!")

    def score(self):
        raise Exception("score() not implemented!")

    def predict(self, packet=None):
        raise Exception("predict() not implemented!")