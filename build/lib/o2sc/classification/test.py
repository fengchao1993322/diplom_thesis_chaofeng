"""
comparison MAE, MSE and R Square between non-online and online machine learning, data based on bytes
"""

# Authors: Chao Feng <friedrich1993322@163.com>

import sys
import os
import timeit
import psutil
import gc
import scapy.all as scapy
import numpy as np
from sklearn.model_selection import train_test_split
import sys
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib

from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from sklearn.preprocessing import label_binarize
import csv
from itertools import cycle

# from . import summaryFunc as sFunc
# from . import synthesizedPlot as sPlot
import functions_chao as cFunc
import summaryFunc as sFunc
import synthesizedPlot as sPlot





import math



if __name__ == "__main__":

    #loadingPath = "/home/chao/Desktop/o2sc/pcaps/streams/vlc_v6_mts_gsm_s16l_ts_rtp_2_fixed_pkt_size.pcap"
    #loadingPath = "/home/chao/Desktop/o2sc/pcaps/streams/easyRtp_test_100_1000.pcap"
    loadingPath = "/home/chao/Desktop/o2sc/pcaps/headers/hardRtpv6NoCsrc_noPayl.pcap"
    #pktsDataFloat = sFunc.readPcap(loadingPath)

    pkts = scapy.rdpcap(loadingPath)
    print(pkts)
    print(len(pkts))
    for pktsIndex in range(len(pkts)):
        pkt = pkts[pktsIndex]

        pktBytes = bytes(pkt)
        #print(pktBytes)
        if pktsIndex == 0:
            pktBytearray = bytearray(pktBytes)
            print(pktBytearray)
            print('-------------------------------')
        else:
            pktBytearrayAdding = bytearray(pktBytes)
            pktBytearray = np.vstack((pktBytearray, pktBytearrayAdding))

    print(pktBytearray)
    print('---------------------------------------------------------------')
    pktsDataframe = pd.DataFrame(pktBytearray)
    print(pktsDataframe)
    print('-------------------------')
    pktsDataFloat = pktsDataframe.astype(float)
    print(pktsDataFloat)
    print('-------------------------')
    '''
    6 features of one byte(package index meanless due to probabily the packages arrive disorderedly):
        feature 0: byte position;
        feature 1: package index;(removed)
        feature 2: byte value;
        feature 3: value of its previous byte in column parallelly;
        feature 4: value of its next byte in column parallelly;
        feature 5: value of its last byte horizontally in the same package;
        feature 6: value of its next byte horizontally in the same package;
    '''
    featuresOfOneByte = 6
    byteInfoArray=np.zeros(shape=(len(pktsDataFloat)*pktsDataFloat.shape[1], featuresOfOneByte))

    LengthofWord = 32

    NumberofBytes = LengthofWord/8

    Bytevalue = 256

    for rowIndexOfPktsDataframe in range(len(pktsDataFloat)):
        for columnIndexOfPktsDataframe in range(math.ceil(pktsDataFloat.shape[1]/NumberofBytes)):
            if rowIndexOfPktsDataframe == 0:
                if columnIndexOfPktsDataframe ==0:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,4*columnIndexOfPktsDataframe]+pktsDataFloat.iat[rowIndexOfPktsDataframe,4*columnIndexOfPktsDataframe+1]+pktsDataFloat.iat[rowIndexOfPktsDataframe,4*columnIndexOfPktsDataframe+2]+pktsDataFloat.iat[rowIndexOfPktsDataframe,4*columnIndexOfPktsDataframe+3], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe+1,4*columnIndexOfPktsDataframe]+pktsDataFloat.iat[rowIndexOfPktsDataframe+1,4*columnIndexOfPktsDataframe+1]+pktsDataFloat.iat[rowIndexOfPktsDataframe+1,4*columnIndexOfPktsDataframe+2]+pktsDataFloat.iat[rowIndexOfPktsDataframe+1,4*columnIndexOfPktsDataframe+3], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,4*(columnIndexOfPktsDataframe+1)]+pktsDataFloat.iat[rowIndexOfPktsDataframe,4*(columnIndexOfPktsDataframe+1)+1]+pktsDataFloat.iat[rowIndexOfPktsDataframe,4*(columnIndexOfPktsDataframe+1)+2]+pktsDataFloat.iat[rowIndexOfPktsDataframe,4*(columnIndexOfPktsDataframe+1)+3]]
                elif columnIndexOfPktsDataframe ==math.ceil(pktsDataFloat.shape[1]/NumberofBytes)-1:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], 0 ,pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], 0]
                else:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], 0 ,pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
            elif rowIndexOfPktsDataframe == len(pktsDataFloat)-1:
                if columnIndexOfPktsDataframe ==0:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], 0, 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
                elif columnIndexOfPktsDataframe == pktsDataFloat.shape[1]-1:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], 0]
                else:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
            else:
                if columnIndexOfPktsDataframe ==0:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
                elif columnIndexOfPktsDataframe == pktsDataFloat.shape[1]-1:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], 0]
                else:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
    print(byteInfoArray)

    byteLabel2dArray = sFunc.byteLabelPreOperation(pktsDataFloat)




    #
    # labels = sFunc.outputLabel(byteLabel2dArray.ravel())
    # # if len(labels) <= 2:
    # #     print('Binary classification.')
    # #     print('original labels: ', labels)
    # #     # the most important label which controls if it's multi-classification:
    # #     multiFlag=False
    # # else:
    # #     print('Multiple classification.')
    # #     print('original labels: ', labels)
    # #     # the most important label which controls if it's multi-classification:
    # #     multiFlag=True
    #
    # # Split into training and test
    # testSize = 0.5
    # randomState = np.random.randint(0,100)
    # y_Num = sFunc.labels2Num(labels, byteLabel2dArray.ravel())
    #
    #
    #
    # X_train, X_test, y_train, y_test = sFunc.trainTestSeparate(
    #         byteInfoArray, y_Num, testSize, random_state=randomState)

#     print(byteInfoArray)
#     print(byteInfoArray.shape[0])
#     print(byteInfoArray.shape[1])
#     print('--------------------------------------------')
#     print(byteLabel2dArray)
#     print(byteLabel2dArray.shape[0])
#     print(byteLabel2dArray.shape[1])
#     print('--------------------------------------------')
#     print(byteLabel2dArray.ravel())
#     print(byteLabel2dArray.ravel())
#     print(y_Num)
#     print(y_Num.shape[0])
# #    print(y_Num.shape[1])

























    # byteInfoArray1 = np.zeros(shape=(len(pktsDataFloat) * pktsDataFloat.shape[1]))
    #
    # print(byteInfoArray)
    # print(byteInfoArray.shape[0])
    # print(byteInfoArray.shape[1])
    # print('------------------------------------------')
    # print(byteInfoArray1)
    # print(len(byteInfoArray1))
    # print('###########################################')
    # print( pktsDataFloat)
    # print(range(len(pktsDataFloat)))
    # print(range(pktsDataFloat.shape[1]))