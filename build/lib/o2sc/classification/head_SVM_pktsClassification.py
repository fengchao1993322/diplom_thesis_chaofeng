import sys
from binascii import hexlify
import matplotlib.pyplot as plt

import numpy as np
import pandas as pd
from sklearn import svm
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import f1_score

import summaryFunc as sFunc 
import synthesizedPlot as sPlot

loadingPath = "/home/student/Documents/aa_scripter/test3.pcap"
pktsDataFloat0 = sFunc.readPcap(loadingPath)
pktsDataFloat1= sFunc.addingPktsIndex(pktsDataFloat0, 'pktsIndex')

resultPkts2dArray = sFunc.byteLabelPreOperation(pktsDataFloat1)
pktsDataFloat1.index = sFunc.pktLabelGeneration(resultPkts2dArray)

###################################################################################
osm = sFunc.overSamplingMethod(methodChosen='naiveRandom')
X_sampled, y_sampled = osm.dataInput(pktsDataFloat1, pktsDataFloat1.index)

testSize = 0.95
X_train, X_test, y_train, y_test = sFunc.trainTestSeparate(
    X_sampled, y_sampled, testSize)

X_train_std, X_test_std = sFunc.dataStandardization(X_train, X_test)
_, pktsDataframe_std = sFunc.dataStandardization(X_train, pktsDataFloat1)

mlModel = sFunc.classifierAndItsParameters(MlModel = 'LinearSVC', lsvc_C=1.0, lsvc_max_iter=100000, lsvc_class_weight='balanced' )
lsvc = mlModel.buildingModelAndFittingTrainSet(X_train_std, y_train)
y_pred = lsvc.predict(X_test_std)

print('Test accuracy:', lsvc.score(X_test_std, y_test))
print(lsvc.classes_)
classNames= ['pkt0 Over Sampling', 'rest Pkts'] 

# Compute confusion matrix
cnf_matrix = confusion_matrix(y_test, y_pred)
#np.set_printoptions(precision=2)

# Plot non-normalized confusion matrix
plt.figure()
sPlot.plot_confusion_matrix(cnf_matrix, classes=classNames,
                      title='Confusion matrix, without normalization')

# Plot normalized confusion matrix
plt.figure()
sPlot.plot_confusion_matrix(cnf_matrix, classes=classNames, normalize=True,
                      title='Normalized confusion matrix')


##########################################################################################################

y_score = lsvc.decision_function(X_test_std)

fpr, tpr, _ = roc_curve(y_test, y_score, pos_label='101000300203300000000')
roc_auc = auc(fpr, tpr)

plt.figure()
sPlot.plot_ROC(fpr, tpr, roc_auc)


#################################################################################################

from sklearn.metrics import average_precision_score
average_precision = average_precision_score(y_test, y_score, pos_label='101000300203300000000')

print('Average precision-recall score: {0:0.2f}'.format(
      average_precision))

from sklearn.metrics import precision_recall_curve
import matplotlib.pyplot as plt
from sklearn.utils.fixes import signature

precision, recall, _ = precision_recall_curve(y_test, y_score, pos_label='101000300203300000000')

f1Score= f1_score(y_test, y_pred, pos_label='101000300203300000000')
print("f1_score: ", f1Score)
plt.figure()
# In matplotlib < 1.5, plt.fill_between does not have a 'step' argument
# step_kwargs = ({'step': 'post'}
#                if 'step' in signature(plt.fill_between).parameters
#                else {})

sPlot.plot_PR_curve(precision, recall, average_precision)
plt.show()
plt.pause(3)
plt.close()
