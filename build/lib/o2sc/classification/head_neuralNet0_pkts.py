import sys
from binascii import hexlify
import summaryFunc as sFunc
import synthesizedPlot as sPlot
# from . import summaryFunc as sFunc
# from . import synthesizedPlot as sPlot

import numpy as np
import pandas as pd
import scapy.all as scapy
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
# optimalize the ML, make it standardlized
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import label_binarize

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":

    loadingPath = "/home/student/Documents/aa_scripter/test5.pcap"
    pktsDataFloat = sFunc.readPcap(loadingPath)

    byteLabel2dArray = sFunc.byteLabelPreOperation(pktsDataFloat)
    pktsDataFloat.index = sFunc.pktLabelGeneration(byteLabel2dArray)

    osm = sFunc.overSamplingMethod(methodChosen='naiveRandom')
    X_sampled, y_sampled = osm.dataInput(pktsDataFloat, pktsDataFloat.index.ravel())

    labels = sFunc.outputLabel(y_sampled)
    print('original labels: ', labels)

    y_neural = sFunc.labels2Num(labels, y_sampled)

    # Split into training and test
    testSize = 0.5
    X_train, X_test, y_train, y_test = sFunc.trainTestSeparate(
            X_sampled, y_neural, testSize)

    X_train_std, X_test_std = sFunc.dataStandardization(X_train, X_test)
    _, pktsDataframe_std = sFunc.dataStandardization(X_train, pktsDataFloat)


    mp = sFunc.classifierAndItsParameters(MlModel = 'NeuralNetwork')

    '''
        For neural network it doesn't need input train set here, only builds model in function 'buildingModelAndFittingTrainSet()'.
        Configurations use keras own commands. 
    '''
    neuralnet = mp.buildingModelAndFittingTrainSet(None, None)
    neuralnet.add(keras.layers.Dense(pktsDataFloat.shape[1], activation=tf.nn.relu))
    neuralnet.add(keras.layers.Dense(2*pktsDataFloat.shape[1], activation=tf.nn.selu))
    neuralnet.add(keras.layers.Dense(4*pktsDataFloat.shape[1], activation=tf.nn.selu))
    neuralnet.add(keras.layers.Dense(2*pktsDataFloat.shape[1], activation=tf.nn.relu))
    neuralnet.add(keras.layers.Dense(2*len(labels), activation=tf.nn.relu))
    neuralnet.add(keras.layers.Dense(1, activation=tf.nn.softmax))


    neuralnet.compile(optimizer='adam',
              loss='mean_squared_error',
              metrics=['accuracy'])


    H=neuralnet.fit(X_train_std, y_train, epochs=50)
    test_loss, test_acc = neuralnet.evaluate(X_test_std, y_test)

    print('Test accuracy:', test_acc)

    predictions = neuralnet.predict(X_test)

    plt.figure()
    sPlot.plot_Acc_Loss(H.history['acc'],H.history['loss'])
    plt.show()
    plt.pause(3)
    plt.close()

