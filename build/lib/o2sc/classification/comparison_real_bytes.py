'''
    comparison between different classifiers, data based on bytes
'''

import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import auc, confusion_matrix, f1_score, roc_curve
from sklearn.preprocessing import label_binarize
import csv
from itertools import cycle

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# from . import summaryFunc as sFunc 
# from . import synthesizedPlot as sPlot
import summaryFunc as sFunc
import synthesizedPlot as sPlot

#if __name__ == "__main__":
def comparison():
    #loadingPath = "/home/student/Documents/PacketVisualiser/dumps/asterisk_rtp_downstream_240.pcap"
    #loadingPath = "/home/student/Documents/PacketVisualiser/dumps/radio_miki_acks_66_no_options_0_ip_id_fix_wnd_sz.pcap"
    loadingPath = "/home/chao/Desktop/o2sc/pcaps/headers/easyIp_noPayl.pcap"
    #loadingPath = "/home/student/Documents/PacketVisualiser/dumps/franka_ip_only_10000.pcap"
    #loadingPath = "/home/student/Documents/aa_scripter/test5.pcap"
    pktsDataFloat = sFunc.readPcap(loadingPath)

    '''
    6 features of one byte(package index meanless due to probabily the packages arrive disorderedly):
        feature 0: byte position;
        feature 1: package index;(removed)
        feature 2: byte value;
        feature 3: value of its previous byte in column parallelly;
        feature 4: value of its next byte in column parallelly;
        feature 5: value of its last byte horizontally in the same package; 
        feature 6: value of its next byte horizontally in the same package;
    '''
    featuresOfOneByte = 6
    byteInfoArray=np.zeros(shape=(len(pktsDataFloat)*pktsDataFloat.shape[1], featuresOfOneByte))

    for rowIndexOfPktsDataframe in range(len(pktsDataFloat)):
        for columnIndexOfPktsDataframe in range(pktsDataFloat.shape[1]):
            if rowIndexOfPktsDataframe == 0: 
                if columnIndexOfPktsDataframe ==0:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
                elif columnIndexOfPktsDataframe ==pktsDataFloat.shape[1]-1:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], 0 ,pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], 0]
                else:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], 0 ,pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
            elif rowIndexOfPktsDataframe == len(pktsDataFloat)-1:
                if columnIndexOfPktsDataframe ==0:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], 0, 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
                elif columnIndexOfPktsDataframe == pktsDataFloat.shape[1]-1:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], 0]
                else:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
            else:
                if columnIndexOfPktsDataframe ==0:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
                elif columnIndexOfPktsDataframe == pktsDataFloat.shape[1]-1:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], 0]
                else:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
    print(byteInfoArray)
    print(byteInfoArray.shape)

    byteLabel2dArray = sFunc.byteLabelPreOperation(pktsDataFloat)

    labels = sFunc.outputLabel(byteLabel2dArray.ravel())
    if len(labels) <= 2:
        print('Binary classification.')
        print('original labels: ', labels)
        # the most important label which controls if it's multi-classification:
        multiFlag=False
    else:
        print('Multiple classification.')
        print('original labels: ', labels)
        # the most important label which controls if it's multi-classification:
        multiFlag=True

    # Split into training and test
    testSize = 0.5
    randomState = np.random.randint(0,100)
    y_Num = sFunc.labels2Num(labels, byteLabel2dArray.ravel())

    # Use label_binarize to be multi-label like settings
    y_binarized = label_binarize(byteLabel2dArray.ravel(), classes=labels)
    #print(y_binarized)
    #n_classes = y_binarized.shape[1]

    X_train, X_test, y_train, y_test = sFunc.trainTestSeparate(
            byteInfoArray, y_Num, testSize, random_state=randomState)
    
    '''
    y_train_m, y_test_m are the separate part from y_binarized.
    For nn, model needs y_train and y_test, but on calculating roc, pr 
    the plot needs y_train_m and y_test_m
    '''
    _, _, y_train_m, y_test_m = sFunc.trainTestSeparate(
        byteInfoArray, y_binarized, testSize, random_state=randomState)

    # print("number represented labels of 'y_test': \n", y_test)
    # print("binarized class labels of 'y_test': \n", y_test_m)

    X_train_std, X_test_std = sFunc.dataStandardization(X_train, X_test)
    _, pktsDataframe_std = sFunc.dataStandardization(X_train, byteInfoArray)

    bytes_accuracy_list = list()

    clf_list = list()
    model_list = list()

    mlModel0 = sFunc.classifierAndItsParameters(MlModel = 'LogisticRegression', lr_C=10.0, lr_class_weight= 'balanced', lr_solver='liblinear', lr_multi_class= 'auto' )
    clf0 = mlModel0.buildingModelAndFittingTrainSet(X_train_std, y_train)
    print('current input Num labels: ', clf0.classes_)
    print('Test accuracy of Logistic Regession:', clf0.score(X_test_std, y_test))
    bytes_accuracy_list.append(clf0.score(X_test_std, y_test))
    clf_list.append(clf0)
    model_list.append('LogisticRegression')

    mlModel1 = sFunc.classifierAndItsParameters(MlModel = 'LinearSVC', lsvc_C=1.0, lsvc_max_iter=10000, lsvc_class_weight='balanced' )
    clf1 = mlModel1.buildingModelAndFittingTrainSet(X_train_std, y_train)
    print('Test accuracy of Linear SVM:', clf1.score(X_test_std, y_test))
    bytes_accuracy_list.append(clf1.score(X_test_std, y_test))
    clf_list.append(clf1)
    model_list.append('LinearSVC')
    
    mlModel2 = sFunc.classifierAndItsParameters(MlModel = 'SVC', svc_kernel= 'rbf', svc_C=1.0, svc_gamma = 'scale', svc_class_weight='balanced' )
    clf2 = mlModel2.buildingModelAndFittingTrainSet(X_train_std, y_train)
    print('Test accuracy of SVM:', clf2.score(X_test_std, y_test))
    bytes_accuracy_list.append(clf2.score(X_test_std, y_test))
    clf_list.append(clf2)
    model_list.append('SVC')

    mlModel3 = sFunc.classifierAndItsParameters(MlModel = 'DecisionTreeClassifier', dt_class_weight='balanced', dt_criterion='entropy' )
    clf3 = mlModel3.buildingModelAndFittingTrainSet(X_train_std, y_train)
    print('Test accuracy of Decision Tree:', clf3.score(X_test_std, y_test))
    bytes_accuracy_list.append(clf3.score(X_test_std, y_test))
    clf_list.append(clf3)
    model_list.append('DecisionTreeClassifier')

    mlModel4 = sFunc.classifierAndItsParameters(MlModel = 'RandomForestClassifier', rf_n_estimators=20, rf_class_weight='balanced')
    clf4 = mlModel4.buildingModelAndFittingTrainSet(X_train_std, y_train)
    print('Test accuracy of Random Forest:', clf4.score(X_test_std, y_test))
    bytes_accuracy_list.append(clf4.score(X_test_std, y_test))
    clf_list.append(clf4)
    model_list.append('RandomForestClassifier')

    mlModel5 = sFunc.classifierAndItsParameters(MlModel = 'GaussianNB')
    clf5 = mlModel5.buildingModelAndFittingTrainSet(X_train_std, y_train)
    print('Test accuracy of Gaussian Naive Bayes:', clf5.score(X_test_std, y_test))
    bytes_accuracy_list.append(clf5.score(X_test_std, y_test))
    clf_list.append(clf5)
    model_list.append('GaussianNB')

    mlModel6 = sFunc.classifierAndItsParameters(MlModel = 'KNeighborsClassifier', knn_algorithm='auto')
    clf6 = mlModel6.buildingModelAndFittingTrainSet(X_train_std, y_train)
    print('Test accuracy of K Nearest Neighbors Classifier:', clf6.score(X_test_std, y_test))
    bytes_accuracy_list.append(clf6.score(X_test_std, y_test))
    clf_list.append(clf6)
    model_list.append('KNeighborsClassifier')

    '''
    ####### model: neural network #######
    mp = sFunc.classifierAndItsParameters(MlModel = 'NeuralNetwork')
    
        For neural network it doesn't need input train set here, only builds model in function 'buildingModelAndFittingTrainSet()'.
        Configurations use keras own commands. 
    
    neuralnet = mp.buildingModelAndFittingTrainSet(None, None)
    neuralnet.add(keras.layers.Dense(6, activation=tf.nn.relu))
    neuralnet.add(keras.layers.Dense(24, activation=tf.nn.relu))
    neuralnet.add(keras.layers.Dense(12, activation=tf.nn.relu))
    neuralnet.add(keras.layers.Dense(len(labels), activation=tf.nn.softmax))

    neuralnet.compile(optimizer='adam',
            loss='sparse_categorical_crossentropy',
            metrics=['accuracy'])

    neuralnet.fit(X_train_std, y_train, batch_size=20, epochs=50)
    
    test_loss, test_acc = neuralnet.evaluate(X_test_std, y_test)

    print('Test accuracy of neural network:', test_acc)
    bytes_accuracy_list.append(test_acc)

    # save to csv file
    csvfile = open('acc.csv', 'a+')
    writer = csv.writer(csvfile)
    writer.writerow(bytes_accuracy_list)
    csvfile.close()

    ##---neural network plot---##
    plt.figure(figsize=(10, 8))
    classesName= labels
    sPlot.confusion_matrix_For_NN(neuralnet, X_test_std, y_test, classesName)

    #####################################
    '''
    clf_m_list = list()

    if (multiFlag == True):
        mlModel0_m = sFunc.classifierAndItsParameters(MlModel = 'LogisticRegression',multiLabel=multiFlag, lr_C=10.0, lr_class_weight= 'balanced', lr_solver='liblinear', lr_multi_class= 'auto' )
        clf0_m = mlModel0_m.buildingModelAndFittingTrainSet(X_train_std, y_train_m)
        print('current input labels in OnevsRest: ', clf0_m.classes_)
        print('Test accuracy of Logistic Regession in OnevsRest:', clf0_m.score(X_test_std, y_test_m))
        clf_m_list.append(clf0_m)

        mlModel1_m = sFunc.classifierAndItsParameters(MlModel = 'LinearSVC',multiLabel=multiFlag, lsvc_C=1.0, lsvc_max_iter=10000, lsvc_class_weight='balanced' )
        clf1_m = mlModel1_m.buildingModelAndFittingTrainSet(X_train_std, y_train_m)
        print('Test accuracy of Linear SVM in OnevsRest:', clf1_m.score(X_test_std, y_test_m))
        clf_m_list.append(clf1_m)

        mlModel2_m = sFunc.classifierAndItsParameters(MlModel = 'SVC',multiLabel=multiFlag, svc_kernel= 'rbf', svc_C=1.0, svc_gamma = 'scale', svc_class_weight='balanced' )
        clf2_m = mlModel2_m.buildingModelAndFittingTrainSet(X_train_std, y_train_m)
        print('Test accuracy of SVM in OnevsRest:', clf2_m.score(X_test_std, y_test_m))
        clf_m_list.append(clf2_m)

        mlModel3_m = sFunc.classifierAndItsParameters(MlModel = 'DecisionTreeClassifier',multiLabel=multiFlag, dt_class_weight='balanced', dt_criterion='entropy' )
        clf3_m = mlModel3_m.buildingModelAndFittingTrainSet(X_train_std, y_train_m)
        print('Test accuracy of Decision Tree in OnevsRest:', clf3_m.score(X_test_std, y_test_m))
        clf_m_list.append(clf3_m)

        mlModel4_m = sFunc.classifierAndItsParameters(MlModel = 'RandomForestClassifier',multiLabel=multiFlag, rf_n_estimators=20, rf_class_weight='balanced')
        clf4_m = mlModel4_m.buildingModelAndFittingTrainSet(X_train_std, y_train_m)
        print('Test accuracy of Random Forest in OnevsRest:', clf4_m.score(X_test_std, y_test_m))
        clf_m_list.append(clf4_m)

        mlModel5_m = sFunc.classifierAndItsParameters(MlModel = 'GaussianNB',multiLabel=multiFlag)
        clf5_m = mlModel5_m.buildingModelAndFittingTrainSet(X_train_std, y_train_m)
        print('Test accuracy of Gaussian Naive Bayes in OnevsRest:', clf5_m.score(X_test_std, y_test_m))
        clf_m_list.append(clf5_m)

        mlModel6_m = sFunc.classifierAndItsParameters(MlModel = 'KNeighborsClassifier',multiLabel=multiFlag, knn_algorithm='auto')
        clf6_m = mlModel6_m.buildingModelAndFittingTrainSet(X_train_std, y_train_m)
        print('Test accuracy of K Nearest Neighbors Classifier in OnevsRest:', clf6_m.score(X_test_std, y_test_m))
        clf_m_list.append(clf6_m)


    #################################################  PLOT  #################################################
    plt.figure(figsize=(10, 8))
    sPlot.confusion_matrix_Generator(clf6,'KNeighborsClassifier', X_test_std, y_test, labels)
    ##########################################################################################################
    if (not multiFlag):
        plt.figure(figsize=(10, 8))
        sPlot.ROC_curve_Generator(
            clf6,
            'KNeighborsClassifier',
            X_test_std,
            y_test, multiFlag=multiFlag, n_classes= len(labels))
        
        plt.figure(figsize=(10, 8))
        sPlot.PR_curve_Generator(clf6, 'KNeighborsClassifier', X_test_std, y_test, multiFlag=multiFlag, n_classes= len(labels))

    else:
        plt.figure(figsize=(10, 8))
        sPlot.ROC_curve_Generator(
            clf6_m,
            'KNeighborsClassifier',
            X_test_std,
            y_test_m, multiFlag=multiFlag, n_classes= len(labels))
        
        plt.figure(figsize=(10, 8))
        sPlot.PR_curve_Generator(clf6_m, 'KNeighborsClassifier', X_test_std, y_test_m, multiFlag=multiFlag, n_classes= len(labels))
        
    ##########################################################################################################

    fpr_list = list()
    tpr_list = list()
    roc_auc_list = list()
    precision_list = list()
    recall_list = list()
    average_precision_list = list()
    f1Score_list = list()

    if (not multiFlag):

        for i, item in enumerate(clf_list):
            fpr, tpr, roc_auc=sFunc.Cal_ROC_curve_parameters(item, model_list[i],X_test_std,y_test, multiFlag=multiFlag)
            precision, recall, average_precision, f1Score=sFunc.Cal_PR_curve_parameters(item, model_list[i],X_test_std,y_test, multiFlag=multiFlag)
            fpr_list.append(fpr)
            tpr_list.append(tpr)
            roc_auc_list.append(roc_auc)
            precision_list.append(precision)
            recall_list.append(recall)
            average_precision_list.append(average_precision)
            f1Score_list.append(f1Score)

        fpr_nn, tpr_nn, roc_auc_nn = sFunc.Cal_nn_ROC_parameters(neuralnet, X_test_std, y_test_m, multiFlag=multiFlag)
        precision_nn, recall_nn, average_precision_nn, f1Score_nn=sFunc.Cal_nn_PR_parameters(neuralnet,X_test_std,y_test_m,labels,multiFlag=multiFlag)
    else: 
        for i, item in enumerate(clf_m_list):
            fpr, tpr, roc_auc=sFunc.Cal_ROC_curve_parameters(item, model_list[i],X_test_std,y_test_m, multiFlag=multiFlag)
            precision, recall, average_precision, f1Score=sFunc.Cal_PR_curve_parameters(item, model_list[i],X_test_std,y_test_m, multiFlag=multiFlag)
            fpr_list.append(fpr)
            tpr_list.append(tpr)
            roc_auc_list.append(roc_auc)
            precision_list.append(precision)
            recall_list.append(recall)
            average_precision_list.append(average_precision)
            f1Score_list.append(f1Score)

#        fpr_nn, tpr_nn, roc_auc_nn = sFunc.Cal_nn_ROC_parameters(neuralnet, X_test_std, y_test_m,multiFlag=multiFlag)
 #       precision_nn, recall_nn, average_precision_nn, f1Score_nn=sFunc.Cal_nn_PR_parameters(neuralnet,X_test_std,y_test_m,labels,multiFlag=multiFlag)

    colors = [
             'navy', 'red', 'darkorchid', 'gold', 'chartreuse', 'seagreen','deepskyblue', 'blue', 'violet'  
        ]

    plt.figure(figsize=(10, 8))
    for i, color in zip(range(len(fpr_list)), colors):
        plt.plot(
            fpr_list[i],
            tpr_list[i],
            color=color,
            lw=1.5,
            label='ROC curve of %s (area = %.3f)' % (model_list[i], roc_auc_list[i]))
    
#    plt.plot(fpr_nn, tpr_nn,color='darkorange', lw=1.5, label='ROC curve of %s (area = %.3f)' %('neural network', roc_auc_nn))

    plt.plot([0, 1], [0, 1], 'k--', lw=1.5)
    plt.title('ROC(receiver operating characteristic) curves of different classifiers')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend(loc="lower right")
    ##########################################################################################################
    plt.figure(figsize=(10, 8))
    f_scores = np.linspace(0.2, 0.8, num=4)
    lines = []
    for f_score in f_scores:
        x = np.linspace(0.01, 1)
        y = f_score * x / (2 * x - f_score)
        l, = plt.plot(x[y >= 0], y[y >= 0], color='gray', alpha=0.2)
        plt.annotate('f1={0:0.1f}'.format(f_score), xy=(0.9, y[45] + 0.02))

    lines.append(l)

    for i, color in zip(range(len(fpr_list)), colors):
        l, = plt.plot(recall_list[i], precision_list[i], color=color, lw=1.5,label= 'PR for %s (area = %.3f, f1 Score = %.3f)'
            %(model_list[i], average_precision_list[i], f1Score_list[i]))
        lines.append(l)

#    l, = plt.plot(recall_nn, precision_nn, color= 'darkorange', lw = 1.5, label='PR for %s (area = %.3f, f1 Score = %.3f)'
#            %('neural network', average_precision_nn, f1Score_nn))
    lines.append(l)

    plt.title('Precision-recall curves of different classifiers')
    plt.legend(loc="lower right")

    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.0])

    plt.show()
    plt.pause(3)
    plt.close()