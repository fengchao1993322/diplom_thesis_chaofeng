'''
    try Scapy functions
'''

# import scapy.all as scapy
# import numpy as np
# from sklearn.model_selection import train_test_split
# import sys
# from binascii import hexlify
# import pandas as pd

# from sklearn.preprocessing import StandardScaler
# from imblearn.over_sampling import RandomOverSampler, SMOTE, ADASYN, BorderlineSMOTE
# from collections import Counter
# import warnings


# def readPcap(path):
#     pkts = scapy.rdpcap(path)

#     for pktsIndex in range(len(pkts)): 
#         pkt = pkts[pktsIndex]
#         pktBytes=bytes(pkt)  
#         if pktsIndex == 0 :
#             pktBytearray=bytearray(pktBytes)
#         else:
#             pktBytearrayAdding=bytearray(pktBytes)
#             pktBytearray=np.vstack((pktBytearray,pktBytearrayAdding))

#     pktsDataframe=pd.DataFrame(pktBytearray)
#     pktsDataframeFloat = pktsDataframe.astype(float)
#     return pktsDataframeFloat

# if __name__ == "__main__":
#     pkts = scapy.rdpcap("/home/student/Documents/aa_scripter/test0.pcap")
#     pkt0 = pkts[0]
#     print(repr(pkt0))
#     print(pkt0['IP'])
#     #print(dir(pkt0['IP']))
#     print(pkt0.getlayer('IP').options)
#     #print(pkt0['Raw'].options)
#     #print(pkt0_dic.layers)
#     # print(bytearray(bytes(pkt0)))
#     # print(dir(pkts[0]))
#     # print(scapy.raw(pkts[0]))
#     # print(bytearray(scapy.raw(pkts[0]))[0])
#     # print(bytearray(scapy.raw(pkts[0]))[1])
#     # print(dir(bytearray(scapy.raw(pkts[0]))[0]))
#     # print(type(scapy.hexdump(pkts[0])))
#     # print(scapy.ls(pkts[0]))
#     print(pkts[0].show())
#     # print(pkts[0].pdfdump())
#     # print(dir(pkts[0]))
#     # val = pkts[0][2].load
#     # print("%x %s" % (ord(val), val))
#     # print(pkts[1][0].load)
#     # print(bytes(pkts[1].id))
#     # print(pkts[0].__all_slots__)
#     # print(pkts[0].__class__)
#     # print(pkts[0].__contains__)
#     # print(pkts[0].__dict__)
#     # print(pkts[0].__format__)


###############################################################################################
###############################################################################################


'''
    try dict
'''

# def kw_dict(**kwargs):
#         return kwargs
# print(kw_dict(a=1,b=2,c=3))

# from collections import defaultdict

# d = defaultdict(set)
# d['a'].add(1)
# d['a'].add(2)
# d['b'].add(4)

# print(d)

'''
    call another class
'''

# class A:
#     def __init__(self,xx,yy):
#         self.x=xx
#         self.y=yy
#     def add(self):
#         print("x和y的和为：%d"%(self.x+self.y))
#         return self.x+self.y

# class B:
#     def __init__(self, DD):
#         self.DD=DD

#     def minus(self):
#         a=A(self.DD,3)
#         print('aaaaa: ', a.add())

# b= B(3)
# b.minus()

'''
    try summaryFunc
'''

# import summaryFunc as sFunc

# AA = sFunc.machineLearningModel()
# AA.inputParameters(lr_penalty='l1',lr_dual=True)

# import numpy as np
# import matplotlib.pyplot as plt
# from itertools import cycle

# from sklearn import svm, datasets
# from sklearn.metrics import roc_curve, auc
# from sklearn.model_selection import train_test_split
# from sklearn.preprocessing import label_binarize
# from sklearn.multiclass import OneVsRestClassifier
# from scipy import interp

# # Import some data to play with
# iris = datasets.load_iris()
# X = iris.data
# y = iris.target

# print(y)

# # Binarize the output
# y = label_binarize(y, classes=[0, 1, 2])
# n_classes = y.shape[1]

# # Add noisy features to make the problem harder
# random_state = np.random.RandomState(0)
# n_samples, n_features = X.shape
# X = np.c_[X, random_state.randn(n_samples, 200 * n_features)]

# # shuffle and split training and test sets
# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.5,
#                                                     random_state=0)

# # Learn to predict each class against the other
# classifier = OneVsRestClassifier(svm.SVC(kernel='linear', probability=True,
#                                  random_state=random_state))
# y_score = classifier.fit(X_train, y_train).decision_function(X_test)

# # Compute ROC curve and ROC area for each class
# fpr = dict()
# tpr = dict()
# roc_auc = dict()
# for i in range(n_classes):
#     fpr[i], tpr[i], _ = roc_curve(y_test[:, i], y_score[:, i])
#     roc_auc[i] = auc(fpr[i], tpr[i])

# # Compute micro-average ROC curve and ROC area
# fpr["micro"], tpr["micro"], _ = roc_curve(y_test.ravel(), y_score.ravel())
# roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])


# plt.figure()
# lw = 2
# plt.plot(fpr[2], tpr[2], color='darkorange',
#          lw=lw, label='ROC curve (area = %0.2f)' % roc_auc[2])
# plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
# plt.xlim([0.0, 1.0])
# plt.ylim([0.0, 1.05])
# plt.xlabel('False Positive Rate')
# plt.ylabel('True Positive Rate')
# plt.title('Receiver operating characteristic example')
# plt.legend(loc="lower right")
# plt.show()

# import matplotlib.pyplot as plt
 
# name_list = ['Monday','Tuesday','Friday','Sunday']
# num_list = [1.5,0.6,7.8,6]
# num_list1 = [1,2,3,1]
# x =list(range(len(num_list)))
# print(x)
# total_width, n = 0.8, 2
# width = total_width / n
 
# plt.bar(x, num_list, width=width, label='boy',fc = 'y')
# for i in range(len(x)):
#     x[i] = x[i] + width
# plt.bar(x, num_list1, width=width, label='girl',tick_label = name_list,fc = 'r')
# plt.legend()
# plt.show()

# import numpy as np
# import matplotlib.pyplot as plt
 
# size = 5
# x = np.arange(size)
# print(x)
# a = np.random.random(size)
# b = np.random.random(size)
# c = np.random.random(size)
 
# total_width, n = 0.8, 3
# width = total_width / n
# x = x - (total_width - width) / 2
# print(x)

# name_list = ['Monday','Tuesday','Friday','Sunday', 'AA']

# plt.bar(x, a, width=width, tick_label = name_list, label='a')
# plt.bar(x + width, b, width=width, label='b')
# plt.bar(x + 2 * width, c, width=width, label='c')
# plt.legend()
# plt.show()

'''
    comparison between different classifiers, data based on bytes
'''

import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import auc, confusion_matrix, f1_score, roc_curve
from sklearn.preprocessing import label_binarize
import csv
from itertools import cycle

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# from . import summaryFunc as sFunc 
# from . import synthesizedPlot as sPlot
import summaryFunc as sFunc
import synthesizedPlot as sPlot

if __name__ == "__main__":
    
    #loadingPath = "/home/student/Documents/PacketVisualiser/dumps/asterisk_rtp_downstream_240.pcap"
    loadingPath = "/home/student/Documents/PacketVisualiser/dumps/radio_miki_acks_66_no_options_0_ip_id_fix_wnd_sz.pcap"
    #loadingPath = "/home/student/Documents/PacketVisualiser/dumps/franka_ip_only_10000.pcap"
    pktsDataFloat = sFunc.readPcap(loadingPath)

    '''
    6 features of one byte(package index meanless due to probabily the packages arrive disorderedly):
        feature 0: byte position;
        feature 1: package index;(removed)
        feature 2: byte value;
        feature 3: value of its previous byte in column parallelly;
        feature 4: value of its next byte in column parallelly;
        feature 5: value of its last byte horizontally in the same package; 
        feature 6: value of its next byte horizontally in the same package;
    '''
    featuresOfOneByte = 6
    byteInfoArray=np.zeros(shape=(len(pktsDataFloat)*pktsDataFloat.shape[1], featuresOfOneByte))

    for rowIndexOfPktsDataframe in range(len(pktsDataFloat)):
        for columnIndexOfPktsDataframe in range(pktsDataFloat.shape[1]):
            if rowIndexOfPktsDataframe == 0: 
                if columnIndexOfPktsDataframe ==0:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
                elif columnIndexOfPktsDataframe ==pktsDataFloat.shape[1]-1:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], 0 ,pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], 0]
                else:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], 0 ,pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
            elif rowIndexOfPktsDataframe == len(pktsDataFloat)-1:
                if columnIndexOfPktsDataframe ==0:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], 0, 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
                elif columnIndexOfPktsDataframe == pktsDataFloat.shape[1]-1:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], 0]
                else:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
            else:
                if columnIndexOfPktsDataframe ==0:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
                elif columnIndexOfPktsDataframe == pktsDataFloat.shape[1]-1:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], 0]
                else:
                    byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
    # print(byteInfoArray)

    byteLabel2dArray = sFunc.byteLabelPreOperation(pktsDataFloat)

    labels = sFunc.outputLabel(byteLabel2dArray.ravel())
    if len(labels) <= 2:
        print('Binary classification:')
        print('original labels: ', labels)
        # the most important label which controls if it's multi-classification:
        multiFlag=False
    else:
        print('Multiple classification:')
        print('original labels: ', labels)
        # the most important label which controls if it's multi-classification:
        multiFlag=True

    # Split into training and test
    testSize = 0.5
    randomState = np.random.randint(0,100)
    if multiFlag == False:
        X_train, X_test, y_train, y_test = sFunc.trainTestSeparate(
                byteInfoArray, byteLabel2dArray.ravel(), testSize,random_state= randomState)
        # Use label_binarize to be multi-label like settings
        y_binarized = label_binarize(byteLabel2dArray.ravel(), classes=labels)
        print(y_binarized)
        
        AA = list()
        for i, item in enumerate(y_binarized):
            if item == 0:
                AA.append(1)
            elif item ==1:
                AA.append(0)

        print (np.array(AA))
        print(np.insert(y_binarized,0,values=AA, axis=1))
    else:
        # Use label_binarize to be multi-label like settings
        y_binarized = label_binarize(byteLabel2dArray.ravel(), classes=labels)
        #print(y_binarized)
        #n_classes = y_binarized.shape[1]

        X_train, X_test, y_train, y_test = sFunc.trainTestSeparate(
                byteInfoArray, byteLabel2dArray.ravel(), testSize, random_state=randomState)
        
        _, _, y_train_m, y_test_m = sFunc.trainTestSeparate(
            byteInfoArray, y_binarized, testSize, random_state=randomState)
        print("normal class labels of 'y_test': \n", y_test)
        print("binarized class labels of 'y_test': \n", y_test_m)