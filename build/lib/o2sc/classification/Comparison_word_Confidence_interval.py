"""
comparison MAE, MSE and R Square between non-online and online machine learning, data based on bytes
"""

# Authors: Chao Feng <friedrich1993322@163.com>

import sys
import os
import timeit
import psutil
import gc

import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import pandas as pd
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from sklearn.preprocessing import label_binarize
import csv
from itertools import cycle
import math

# from . import summaryFunc as sFunc
# from . import synthesizedPlot as sPlot
import functions_chao as cFunc
import summaryFunc as sFunc
import synthesizedPlot as sPlot

if __name__ == "__main__":
#def comparison():
    #loadingPath = "/home/chao/Desktop/o2sc/pcaps/streams/vlc_v6_mts_gsm_s16l_ts_rtp_2_fixed_pkt_size.pcap"
    #loadingPath = "/home/chao/Desktop/o2sc/pcaps/streams/easyRtp_test_100_1000.pcap"
    loadingPath = "/home/chao/Desktop/o2sc/pcaps/headers/hardRtpv6NoCsrc_noPayl.pcap"
    pktsDataFloat = sFunc.readPcap(loadingPath)

    LengthofWord = 32

    NumberofBytes = int(LengthofWord / 8)
    '''
    6 features of one word(package index meanless due to probabily the packages arrive disorderedly):
        feature 0: word position;
        feature 1: package index;(removed)
        feature 2: word value;
        feature 3: value of its previous word in column parallelly;
        feature 4: value of its next word in column parallelly;
        feature 5: value of its last word horizontally in the same package; 
        feature 6: value of its next word horizontally in the same package;
    '''
    featuresOfOneByte = 6
    wordInfoArray = np.zeros(shape=(len(pktsDataFloat) * math.ceil(pktsDataFloat.shape[1]/NumberofBytes), featuresOfOneByte))
    ColumnNumber = int(math.ceil(pktsDataFloat.shape[1] / NumberofBytes))
    wordValue = np.zeros(shape=(len(pktsDataFloat), ColumnNumber))
    Bytevalue = 256


    for rowIndexOfPktsDataframe in range(len(pktsDataFloat)):
        for columnIndexOfPktsDataframe in range(ColumnNumber):
            if rowIndexOfPktsDataframe == 0:
                if columnIndexOfPktsDataframe == 0:
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = 0
                    for indexofbytes in range(NumberofBytes-1, -1, -1):
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3]+pktsDataFloat.iat[rowIndexOfPktsDataframe + 1, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*(columnIndexOfPktsDataframe + 1)+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)

                elif columnIndexOfPktsDataframe == ColumnNumber - 1:

                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = 0
                    for indexofbytes in range(NumberofBytes-1, -1, -1):
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3]+pktsDataFloat.iat[rowIndexOfPktsDataframe + 1, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*(columnIndexOfPktsDataframe - 1)+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)

                else:

                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = 0
                    for indexofbytes in range(NumberofBytes - 1, -1, -1):
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] =  wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3]+pktsDataFloat.iat[rowIndexOfPktsDataframe + 1, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*(columnIndexOfPktsDataframe - 1)+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*(columnIndexOfPktsDataframe + 1)+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)

            elif rowIndexOfPktsDataframe == len(pktsDataFloat) - 1:
                if columnIndexOfPktsDataframe == 0:

                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = 0
                    for indexofbytes in range(NumberofBytes - 1, -1, -1):
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2]+pktsDataFloat.iat[rowIndexOfPktsDataframe - 1, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*(columnIndexOfPktsDataframe + 1)+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)

                elif columnIndexOfPktsDataframe == ColumnNumber - 1:

                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = 0
                    for indexofbytes in range(NumberofBytes - 1, -1, -1):
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2]+pktsDataFloat.iat[rowIndexOfPktsDataframe - 1, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*(columnIndexOfPktsDataframe - 1)+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)



                else:

                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = 0
                    for indexofbytes in range(NumberofBytes - 1, -1, -1):
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2]+pktsDataFloat.iat[rowIndexOfPktsDataframe - 1, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*(columnIndexOfPktsDataframe - 1)+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*(columnIndexOfPktsDataframe + 1)+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)


            else:
                if columnIndexOfPktsDataframe == 0:
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = 0
                    for indexofbytes in range(NumberofBytes - 1, -1, -1):
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2]+pktsDataFloat.iat[rowIndexOfPktsDataframe - 1, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3]+pktsDataFloat.iat[rowIndexOfPktsDataframe + 1, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*(columnIndexOfPktsDataframe + 1)+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)


                elif columnIndexOfPktsDataframe == ColumnNumber - 1:

                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = 0
                    for indexofbytes in range(NumberofBytes - 1, -1, -1):
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2]+pktsDataFloat.iat[rowIndexOfPktsDataframe - 1, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3]+pktsDataFloat.iat[rowIndexOfPktsDataframe + 1, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*(columnIndexOfPktsDataframe - 1)+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)


                else:

                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = 0
                    wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = 0
                    for indexofbytes in range(NumberofBytes - 1, -1, -1):
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1]+pktsDataFloat.iat[rowIndexOfPktsDataframe - 1, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1]+pktsDataFloat.iat[rowIndexOfPktsDataframe + 1, NumberofBytes*columnIndexOfPktsDataframe+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*(columnIndexOfPktsDataframe - 1)+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
                        wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1]+pktsDataFloat.iat[rowIndexOfPktsDataframe, NumberofBytes*(columnIndexOfPktsDataframe + 1)+(NumberofBytes-1-indexofbytes)]*(Bytevalue**indexofbytes)
            wordValue[rowIndexOfPktsDataframe][columnIndexOfPktsDataframe]=wordInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1]





    print('----------------------------------------------------------------------')

    print(wordValue)

    print(wordInfoArray[0][1])
    print('------------------------------------------------------------------------------------------------------------------------')

    pktsDataframe = pd.DataFrame(wordValue)
    wordValueFloat = pktsDataframe.astype(float)





    wordLabel2dArray = cFunc.wordLabelPreOperation(wordValueFloat, NumberofBytes)
    print(wordLabel2dArray)

    print(wordLabel2dArray.shape)

    print('-------------------------------------------------------------------------------------------------------------------------')
    labels = sFunc.outputLabel(wordLabel2dArray.ravel())
    if len(labels) <= 2:
        print('Binary classification.')
        print('original labels: ', labels)
        # the most important label which controls if it's multi-classification:
        multiFlag=False
    else:
        print('Multiple classification.')
        print('original labels: ', labels)
        # the most important label which controls if it's multi-classification:
        multiFlag=True

    # Split into training and test
    testSize = 0.5
    randomState = np.random.randint(0,100)
    y_Num = sFunc.labels2Num(labels, wordLabel2dArray.ravel())
    print('#########################')
    print(wordInfoArray)

    print(wordInfoArray.shape)
    #print()
    print(y_Num)
    print(y_Num.shape)
    print('########################')
    X_train, X_test, y_train, y_test = sFunc.trainTestSeparate(
            wordInfoArray, y_Num, testSize, random_state=randomState)



    print("number represented labels of 'y_test': \n", y_test)
    # print("binarized class labels of 'y_test': \n", y_test_m)

    X_train_std, X_test_std = sFunc.dataStandardization(X_train, X_test)


    gc.collect()

    bytes_MAE_list = [[] for i in range(10)]
    bytes_MSE_list = [[] for i in range(10)]
    bytes_R_square_list = [[] for i in range(10)]
    elapsed_time_train_list = [[] for i in range(10)]
    elapsed_time_predict_list = [[] for i in range(10)]



    for i in range(10):
        y_pred, elapsed_time_train, elapsed_time_predict, clf0 = cFunc.LogisticRegression(X_train_std, X_test_std, y_train)
        print('current input Num labels: ', clf0.classes_)
        print(" 'y_pred': \n", y_pred)
        print('Test MAE of Logistic Regression:', mean_absolute_error(y_test, y_pred))
        bytes_MAE_list[i].append(mean_absolute_error(y_test, y_pred))
        print('Test MSE of Logistic Regression:', mean_squared_error(y_test, y_pred))
        bytes_MSE_list[i].append(mean_squared_error(y_test, y_pred))
        print('Test R square of Logistic Regression:', r2_score(y_test, y_pred))
        bytes_R_square_list[i].append(r2_score(y_test, y_pred))
        #clf_list[i].append(clf0)
        #model_list[i].append('LogisticRegression')

        print('Elapsed time (train): %s seconds' %elapsed_time_train)
        print('Elapsed time (predict): %s seconds' %elapsed_time_predict)
        elapsed_time_train_list[i].append(elapsed_time_train)
        elapsed_time_predict_list[i].append(elapsed_time_predict)

        #memory_fit.append(2.2)
        #memory_predict.append(1.1)
        '''
        online learning
        '''
        print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        # lists1 = [[] for i in range(3)]
        # for num in range(1,10):
        #     tstSize=num/10
        #     X_train_online, X_test_online, y_train_online, y_test_online = sFunc.trainTestSeparate(wordInfoArray, y_Num, tstSize, random_state=randomState)
        #     X_train_std_online, X_test_std_online = sFunc.dataStandardization(X_train_online, X_test_online)
        #     y_pred_online, elapsed_time_train, elapsed_time_predict, clf0 = cFunc.LogisticRegression(X_train_std_online, X_test_std_online, y_train_online)
        #     print(" 'y_pred': \n", y_pred_online)
        #     print('online learning in %s time:' %num, 'MAE',mean_absolute_error(y_test_online, y_pred_online),
        #           'MSE',mean_squared_error(y_test_online, y_pred_online),'R squared', r2_score(y_test_online, y_pred_online))
        #     lists1[0].append(mean_absolute_error(y_test_online, y_pred_online))
        #     lists1[1].append(mean_squared_error(y_test_online, y_pred_online))
        #     lists1[2].append(r2_score(y_test_online, y_pred_online))

        print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        print('------------------------------------------------------------------------------------')

        y_pred, elapsed_time_train, elapsed_time_predict, clf1 = cFunc.LinearSVC(X_train_std, X_test_std, y_train)
        print(" 'y_pred': \n", y_pred)
        print('Test MAE of Linear SVM:', mean_absolute_error(y_test, y_pred))
        bytes_MAE_list[i].append(mean_absolute_error(y_test, y_pred))
        print('Test MSE of Linear SVM:', mean_squared_error(y_test, y_pred))
        bytes_MSE_list[i].append(mean_squared_error(y_test, y_pred))
        print('Test R square of Linear SVM:', r2_score(y_test, y_pred))
        bytes_R_square_list[i].append(r2_score(y_test, y_pred))
        # clf_list[i].append(clf1)
        # model_list[i].append('LinearSVC')

        print('Elapsed time (train): %s seconds' % elapsed_time_train)
        print('Elapsed time (predict): %s seconds' % elapsed_time_predict)
        elapsed_time_train_list[i].append(elapsed_time_train)
        elapsed_time_predict_list[i].append(elapsed_time_predict)

        #memory_fit.append(0.4)
        #memory_predict.append(0)
        print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        # lists2 = [[] for i in range(3)]
        # for num in range(1,10):
        #     tstSize=num/10
        #     X_train_online, X_test_online, y_train_online, y_test_online = sFunc.trainTestSeparate(wordInfoArray, y_Num, tstSize, random_state=randomState)
        #     X_train_std_online, X_test_std_online = sFunc.dataStandardization(X_train_online, X_test_online)
        #     y_pred_online, elapsed_time_train, elapsed_time_predict, clf0 = cFunc.LinearSVC(X_train_std_online, X_test_std_online, y_train_online)
        #     print(" 'y_pred': \n", y_pred_online)
        #     print('online learning in %s time:' %num, 'MAE',mean_absolute_error(y_test_online, y_pred_online),
        #           'MSE',mean_squared_error(y_test_online, y_pred_online),'R squared', r2_score(y_test_online, y_pred_online))
        #     lists2[0].append(mean_absolute_error(y_test_online, y_pred_online))
        #     lists2[1].append(mean_squared_error(y_test_online, y_pred_online))
        #     lists2[2].append(r2_score(y_test_online, y_pred_online))



        print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        print('------------------------------------------------------------------------------------')

        y_pred, elapsed_time_train, elapsed_time_predict, clf2 = cFunc.SVC(X_train_std, X_test_std, y_train)
        print(" 'y_pred': \n", y_pred)
        print('Test MAE of Non-linear SVM:', mean_absolute_error(y_test, y_pred))
        bytes_MAE_list[i].append(mean_absolute_error(y_test, y_pred))
        print('Test MSE of Non-linear SVM:', mean_squared_error(y_test, y_pred))
        bytes_MSE_list[i].append(mean_squared_error(y_test, y_pred))
        print('Test R square of Non-linear SVM:', r2_score(y_test, y_pred))
        bytes_R_square_list[i].append(r2_score(y_test, y_pred))
        #clf_list[i].append(clf2)
        #model_list[i].append('SVC')
        print('Elapsed time (train): %s seconds' %elapsed_time_train)
        print('Elapsed time (predict): %s seconds' %elapsed_time_predict)
        elapsed_time_train_list[i].append(elapsed_time_train)
        elapsed_time_predict_list[i].append(elapsed_time_predict)

        #memory_fit.append(0)
        #memory_predict.append(0)
        print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        # lists3 = [[] for i in range(3)]
        # for num in range(1,10):
        #      tstSize=num/10
        #      X_train_online, X_test_online, y_train_online, y_test_online = sFunc.trainTestSeparate(wordInfoArray, y_Num, tstSize, random_state=randomState)
        #      X_train_std_online, X_test_std_online = sFunc.dataStandardization(X_train_online, X_test_online)
        #      y_pred_online, elapsed_time_train, elapsed_time_predict, clf0 = cFunc.SVC(X_train_std_online, X_test_std_online, y_train_online)
        #      print(" 'y_pred': \n", y_pred_online)
        #      print('online learning in %s time:' %num, 'MAE',mean_absolute_error(y_test_online, y_pred_online),
        #            'MSE',mean_squared_error(y_test_online, y_pred_online),'R squared', r2_score(y_test_online, y_pred_online))
        #      lists3[0].append(mean_absolute_error(y_test_online, y_pred_online))
        #      lists3[1].append(mean_squared_error(y_test_online, y_pred_online))
        #      lists3[2].append(r2_score(y_test_online, y_pred_online))

        print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        print('------------------------------------------------------------------------------------')
        #info=psutil.virtual_memory()
        #print(u'used memory:',psutil.Process(os.getpid()).memory_info().rss)

        #gc.collect()
        y_pred, elapsed_time_train, elapsed_time_predict , clf3 = cFunc.DecisionTreeClassifier(X_train_std, X_test_std, y_train)
        print(" 'y_pred': \n", y_pred)
        print('Test MAE of Decision Tree:', mean_absolute_error(y_test, y_pred))
        bytes_MAE_list[i].append(mean_absolute_error(y_test, y_pred))
        print('Test MSE of Decision Tree:', mean_squared_error(y_test, y_pred))
        bytes_MSE_list[i].append(mean_squared_error(y_test, y_pred))
        print('Test R square of Decision Tree:', r2_score(y_test, y_pred))
        bytes_R_square_list[i].append(r2_score(y_test, y_pred))
        #clf_list[i].append(clf3)
        #model_list[i].append('DecisionTreeClassifier')
        print('Elapsed time (train): %s seconds' %elapsed_time_train)
        print('Elapsed time (predict): %s seconds' %elapsed_time_predict)
        elapsed_time_train_list[i].append(elapsed_time_train)
        elapsed_time_predict_list[i].append(elapsed_time_predict)
        #memory_fit.append(0)
        #memory_predict.append(0)
        print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        # lists4 = [[] for i in range(3)]
        # for num in range(1,10):
        #      tstSize=num/10
        #      X_train_online, X_test_online, y_train_online, y_test_online = sFunc.trainTestSeparate(wordInfoArray, y_Num, tstSize, random_state=randomState)
        #      X_train_std_online, X_test_std_online = sFunc.dataStandardization(X_train_online, X_test_online)
        #      y_pred_online, elapsed_time_train, elapsed_time_predict, clf0 = cFunc.DecisionTreeClassifier(X_train_std_online, X_test_std_online, y_train_online)
        #      print(" 'y_pred': \n", y_pred_online)
        #      print('online learning in %s time:' %num, 'MAE',mean_absolute_error(y_test_online, y_pred_online),
        #            'MSE',mean_squared_error(y_test_online, y_pred_online),'R squared', r2_score(y_test_online, y_pred_online))
        #      lists4[0].append(mean_absolute_error(y_test_online, y_pred_online))
        #      lists4[1].append(mean_squared_error(y_test_online, y_pred_online))
        #      lists4[2].append(r2_score(y_test_online, y_pred_online))

        print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        print('------------------------------------------------------------------------------------')

        y_pred, elapsed_time_train, elapsed_time_predict , clf3 = cFunc.DecisionTreeClassifier(X_train_std, X_test_std, y_train)
        print(" 'y_pred': \n", y_pred)
        print('Test MAE of Decision Tree:', mean_absolute_error(y_test, y_pred))
        bytes_MAE_list[i].append(mean_absolute_error(y_test, y_pred))
        print('Test MSE of Decision Tree:', mean_squared_error(y_test, y_pred))
        bytes_MSE_list[i].append(mean_squared_error(y_test, y_pred))
        print('Test R square of Decision Tree:', r2_score(y_test, y_pred))
        bytes_R_square_list[i].append(r2_score(y_test, y_pred))
        #clf_list[i].append(clf3)
        #model_list[i].append('DecisionTreeClassifier')
        print('Elapsed time (train): %s seconds' %elapsed_time_train)
        print('Elapsed time (predict): %s seconds' %elapsed_time_predict)
        elapsed_time_train_list[i].append(elapsed_time_train)
        elapsed_time_predict_list[i].append(elapsed_time_predict)

        #memory_fit.append(0.1)
        #memory_predict.append(0)
        print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        # lists5 = [[] for i in range(3)]
        # for num in range(1,10):
        #      tstSize=num/10
        #      X_train_online, X_test_online, y_train_online, y_test_online = sFunc.trainTestSeparate(wordInfoArray, y_Num, tstSize, random_state=randomState)
        #      X_train_std_online, X_test_std_online = sFunc.dataStandardization(X_train_online, X_test_online)
        #      y_pred_online, elapsed_time_train, elapsed_time_predict, clf0 = cFunc.RandomForestClassifier(X_train_std_online, X_test_std_online, y_train_online)
        #      print(" 'y_pred': \n", y_pred_online)
        #      print('online learning in %s time:' %num, 'MAE',mean_absolute_error(y_test_online, y_pred_online),
        #            'MSE',mean_squared_error(y_test_online, y_pred_online),'R squared', r2_score(y_test_online, y_pred_online))
        #      lists5[0].append(mean_absolute_error(y_test_online, y_pred_online))
        #      lists5[1].append(mean_squared_error(y_test_online, y_pred_online))
        #      lists5[2].append(r2_score(y_test_online, y_pred_online))

        print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        print('------------------------------------------------------------------------------------')
        #info=psutil.virtual_memory()
        #print(u'used memory:',psutil.Process(os.getpid()).memory_info().rss)

        #gc.collect()
        y_pred, elapsed_time_train, elapsed_time_predict , clf5 = cFunc.GaussianNB(X_train_std, X_test_std, y_train)
        print(" 'y_pred': \n", y_pred)
        print('Test MAE of Gaussian Naive Bayes:', mean_absolute_error(y_test, y_pred))
        bytes_MAE_list[i].append(mean_absolute_error(y_test, y_pred))
        print('Test MSE of Gaussian Naive Bayes:', mean_squared_error(y_test, y_pred))
        bytes_MSE_list[i].append(mean_squared_error(y_test, y_pred))
        print('Test R square of Gaussian Naive Bayes:', r2_score(y_test, y_pred))
        bytes_R_square_list[i].append(r2_score(y_test, y_pred))
        #clf_list[i].append(clf5)
        #model_list[i].append('GaussianNB')

        print('Elapsed time (train): %s seconds' %elapsed_time_train)
        print('Elapsed time (predict): %s seconds' %elapsed_time_predict)
        elapsed_time_train_list[i].append(elapsed_time_train)
        elapsed_time_predict_list[i].append(elapsed_time_predict)

        #memory_fit.append(0)
        #memory_predict.append(0)
        print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        # lists6 = [[] for i in range(3)]
        # for num in range(1,10):
        #      tstSize=num/10
        #      X_train_online, X_test_online, y_train_online, y_test_online = sFunc.trainTestSeparate(wordInfoArray, y_Num, tstSize, random_state=randomState)
        #      X_train_std_online, X_test_std_online = sFunc.dataStandardization(X_train_online, X_test_online)
        #      y_pred_online, elapsed_time_train, elapsed_time_predict, clf0 = cFunc.GaussianNB(X_train_std_online, X_test_std_online, y_train_online)
        #      print(" 'y_pred': \n", y_pred_online)
        #      print('online learning in %s time:' %num, 'MAE',mean_absolute_error(y_test_online, y_pred_online),
        #            'MSE',mean_squared_error(y_test_online, y_pred_online),'R squared', r2_score(y_test_online, y_pred_online))
        #      lists6[0].append(mean_absolute_error(y_test_online, y_pred_online))
        #      lists6[1].append(mean_squared_error(y_test_online, y_pred_online))
        #      lists6[2].append(r2_score(y_test_online, y_pred_online))

        print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        print('------------------------------------------------------------------------------------')
        #info=psutil.virtual_memory()
        #print(u'used memory:',psutil.Process(os.getpid()).memory_info().rss)

        #gc.collect()
        y_pred, elapsed_time_train, elapsed_time_predict , clf6 = cFunc.KNeighborsClassifier(X_train_std, X_test_std, y_train)
        print(" 'y_pred': \n", y_pred)
        print('Test MAE of K Nearest Neighbors Classifier:', mean_absolute_error(y_test, y_pred))
        bytes_MAE_list[i].append(mean_absolute_error(y_test, y_pred))
        print('Test MSE of K Nearest Neighbors Classifier:', mean_squared_error(y_test, y_pred))
        bytes_MSE_list[i].append(mean_squared_error(y_test, y_pred))
        print('Test R square of K Nearest Neighbors Classifier:', r2_score(y_test, y_pred))
        bytes_R_square_list[i].append(r2_score(y_test, y_pred))
        #clf_list[i].append(clf6)
        #model_list[i].append('KNeighborsClassifier')

        print('Elapsed time (train): %s seconds' %elapsed_time_train)
        print('Elapsed time (predict): %s seconds' %elapsed_time_predict)
        elapsed_time_train_list[i].append(elapsed_time_train)
        elapsed_time_predict_list[i].append(elapsed_time_predict)

        #memory_fit.append(0.4)
        #memory_predict.append(0)
        print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        # lists7 = [[] for i in range(3)]
        # for num in range(1,10):
        #      tstSize=num/10
        #      X_train_online, X_test_online, y_train_online, y_test_online = sFunc.trainTestSeparate(wordInfoArray, y_Num, tstSize, random_state=randomState)
        #      X_train_std_online, X_test_std_online = sFunc.dataStandardization(X_train_online, X_test_online)
        #      y_pred_online, elapsed_time_train, elapsed_time_predict, clf0 = cFunc.KNeighborsClassifier(X_train_std_online, X_test_std_online, y_train_online)
        #      print(" 'y_pred': \n", y_pred_online)
        #      print('online learning in %s time:' %num, 'MAE',mean_absolute_error(y_test_online, y_pred_online),
        #            'MSE',mean_squared_error(y_test_online, y_pred_online),'R squared', r2_score(y_test_online, y_pred_online))
        #      lists7[0].append(mean_absolute_error(y_test_online, y_pred_online))
        #      lists7[1].append(mean_squared_error(y_test_online, y_pred_online))
        #      lists7[2].append(r2_score(y_test_online, y_pred_online))

    print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    print('------------------------------------------------------------------------------------')

    bytes_MAE_list_means = list()
    bytes_MSE_list_means = list()
    bytes_R_square_list_means = list()
    elapsed_time_train_list_means = list()
    elapsed_time_predict_list_means = list()

    bytes_MAE_list_interval = list()
    bytes_MSE_list_interval = list()
    bytes_R_square_list_interval = list()
    elapsed_time_train_list_interval = list()
    elapsed_time_predict_list_interval = list()

    for j in range(7):
        bytes_MAE_list_means.append((bytes_MAE_list[0][j] + bytes_MAE_list[1][j] + bytes_MAE_list[2][j] + bytes_MAE_list[3][
            j] + bytes_MAE_list[4][j] + bytes_MAE_list[5][j] + bytes_MAE_list[6][j] + bytes_MAE_list[7][j] +
                                     bytes_MAE_list[8][j] + bytes_MAE_list[9][j]) / 10)
        bytes_MSE_list_means.append((bytes_MSE_list[0][j] + bytes_MSE_list[1][j] + bytes_MSE_list[2][j] + bytes_MSE_list[3][
            j] + bytes_MSE_list[4][j] + bytes_MSE_list[5][j] + bytes_MSE_list[6][j] + bytes_MSE_list[7][j] +
                                     bytes_MSE_list[8][j] + bytes_MSE_list[9][j]) / 10)
        bytes_R_square_list_means.append((bytes_R_square_list[0][j] + bytes_R_square_list[1][j] + bytes_R_square_list[2][
            j] + bytes_R_square_list[3][j] + bytes_R_square_list[4][j] + bytes_R_square_list[5][j] + bytes_R_square_list[6][
                                              j] + bytes_R_square_list[7][j] + bytes_R_square_list[8][j] +
                                          bytes_R_square_list[9][j]) / 10)
        elapsed_time_train_list_means.append((elapsed_time_train_list[0][j] + elapsed_time_train_list[1][j] +
                                              elapsed_time_train_list[2][j] + elapsed_time_train_list[3][j] +
                                              elapsed_time_train_list[4][j] + elapsed_time_train_list[5][j] +
                                              elapsed_time_train_list[6][j] + elapsed_time_train_list[7][j] +
                                              elapsed_time_train_list[8][j] + elapsed_time_train_list[9][j]) / 10)
        elapsed_time_predict_list_means.append((elapsed_time_predict_list[0][j] + elapsed_time_predict_list[1][j] +
                                                elapsed_time_predict_list[2][j] + elapsed_time_predict_list[3][j] +
                                                elapsed_time_predict_list[4][j] + elapsed_time_predict_list[5][j] +
                                                elapsed_time_predict_list[6][j] + elapsed_time_predict_list[7][j] +
                                                elapsed_time_predict_list[8][j] + elapsed_time_predict_list[9][j]) / 10)

        bytes_MAE_list_interval.append(1.96 * (np.sqrt(
            np.square(bytes_MAE_list[0][j] - bytes_MAE_list_means[j]) + np.square(
                bytes_MAE_list[1][j] - bytes_MAE_list_means[j]) + np.square(
                bytes_MAE_list[2][j] - bytes_MAE_list_means[j]) + np.square(
                bytes_MAE_list[3][j] - bytes_MAE_list_means[j]) + np.square(
                bytes_MAE_list[4][j] - bytes_MAE_list_means[j]) + np.square(
                bytes_MAE_list[5][j] - bytes_MAE_list_means[j]) + np.square(
                bytes_MAE_list[6][j] - bytes_MAE_list_means[j]) + np.square(
                bytes_MAE_list[7][j] - bytes_MAE_list_means[j]) + np.square(
                bytes_MAE_list[8][j] - bytes_MAE_list_means[j]) + np.square(
                bytes_MAE_list[9][j] - bytes_MAE_list_means[j]))) / 10)

        bytes_MSE_list_interval.append(1.96 * (np.sqrt(
            np.square(bytes_MSE_list[0][j] - bytes_MSE_list_means[j]) + np.square(
                bytes_MSE_list[1][j] - bytes_MSE_list_means[j]) + np.square(
                bytes_MSE_list[2][j] - bytes_MSE_list_means[j]) + np.square(
                bytes_MSE_list[3][j] - bytes_MSE_list_means[j]) + np.square(
                bytes_MSE_list[4][j] - bytes_MSE_list_means[j]) + np.square(
                bytes_MSE_list[5][j] - bytes_MSE_list_means[j]) + np.square(
                bytes_MSE_list[6][j] - bytes_MSE_list_means[j]) + np.square(
                bytes_MSE_list[7][j] - bytes_MSE_list_means[j]) + np.square(
                bytes_MSE_list[8][j] - bytes_MSE_list_means[j]) + np.square(
                bytes_MSE_list[9][j] - bytes_MSE_list_means[j]))) / 10)
        bytes_R_square_list_interval.append(1.96 * (np.sqrt(
            np.square(bytes_R_square_list[0][j] - bytes_R_square_list_means[j]) + np.square(
                bytes_R_square_list[1][j] - bytes_R_square_list_means[j]) + np.square(
                bytes_R_square_list[2][j] - bytes_R_square_list_means[j]) + np.square(
                bytes_R_square_list[3][j] - bytes_R_square_list_means[j]) + np.square(
                bytes_R_square_list[4][j] - bytes_R_square_list_means[j]) + np.square(
                bytes_R_square_list[5][j] - bytes_R_square_list_means[j]) + np.square(
                bytes_R_square_list[6][j] - bytes_R_square_list_means[j]) + np.square(
                bytes_R_square_list[7][j] - bytes_R_square_list_means[j]) + np.square(
                bytes_R_square_list[8][j] - bytes_R_square_list_means[j]) + np.square(
                bytes_R_square_list[9][j] - bytes_R_square_list_means[j]))) / 10)
        elapsed_time_train_list_interval.append(1.96 * (np.sqrt(
            np.square(elapsed_time_train_list[0][j] - elapsed_time_train_list_means[j]) + np.square(
                elapsed_time_train_list[1][j] - elapsed_time_train_list_means[j]) + np.square(
                elapsed_time_train_list[2][j] - elapsed_time_train_list_means[j]) + np.square(
                elapsed_time_train_list[3][j] - elapsed_time_train_list_means[j]) + np.square(
                elapsed_time_train_list[4][j] - elapsed_time_train_list_means[j]) + np.square(
                elapsed_time_train_list[5][j] - elapsed_time_train_list_means[j]) + np.square(
                elapsed_time_train_list[6][j] - elapsed_time_train_list_means[j]) + np.square(
                elapsed_time_train_list[7][j] - elapsed_time_train_list_means[j]) + np.square(
                elapsed_time_train_list[8][j] - elapsed_time_train_list_means[j]) + np.square(
                elapsed_time_train_list[9][j] - elapsed_time_train_list_means[j]))) / 10)
        elapsed_time_predict_list_interval.append(1.96 * (np.sqrt(
            np.square(elapsed_time_predict_list[0][j] - elapsed_time_predict_list_means[j]) + np.square(
                elapsed_time_predict_list[1][j] - elapsed_time_predict_list_means[j]) + np.square(
                elapsed_time_predict_list[2][j] - elapsed_time_predict_list_means[j]) + np.square(
                elapsed_time_predict_list[3][j] - elapsed_time_predict_list_means[j]) + np.square(
                elapsed_time_predict_list[4][j] - elapsed_time_predict_list_means[j]) + np.square(
                elapsed_time_predict_list[5][j] - elapsed_time_predict_list_means[j]) + np.square(
                elapsed_time_predict_list[6][j] - elapsed_time_predict_list_means[j]) + np.square(
                elapsed_time_predict_list[7][j] - elapsed_time_predict_list_means[j]) + np.square(
                elapsed_time_predict_list[8][j] - elapsed_time_predict_list_means[j]) + np.square(
                elapsed_time_predict_list[9][j] - elapsed_time_predict_list_means[j]))) / 10)

    plt.figure(figsize=(20, 16))
    matplotlib.rcParams['axes.unicode_minus'] = False

    label_list = ['Logistic Regession', 'Linear SVM', 'Non-linear SVM', 'Decision Tree','Random Forest','Gaussian Naive Bayes','K Nearest Neighbors Classifier']

    rects1 = plt.bar(np.arange(len(bytes_MAE_list_means)), height=bytes_MAE_list_means, width=0.1, alpha=0.8,
                     yerr=bytes_MAE_list_interval, color='red', label="MAE")
    rects2 = plt.bar([i + 0.1 for i in np.arange(len(bytes_MAE_list_means))], height=bytes_MSE_list_means, width=0.1,
                     yerr=bytes_MSE_list_interval, color='green', label="MSE")
    rects3 = plt.bar([i + 0.2 for i in np.arange(len(bytes_MAE_list_means))], height=bytes_R_square_list_means, width=0.1,
                     yerr=bytes_R_square_list_interval, color='blue', label="$R^2$")
    #    rects4 = plt.bar(left=[i + 1.2 for i in x], height=elapsed_time_list, width=0.4, color='yellow', label="elapsed time")
    plt.ylim(0, 1)
    plt.ylabel("MAE/MSE/$R^2$")

    plt.xticks([index + 0.1 for index in np.arange(len(bytes_MAE_list_means))], label_list)
    # plt.xticks([index + 0.1 for index in np.arange(len(bytes_MAE_list))])
    plt.xlabel("Various machine learning algorithms ")
    # plt.title("Comparison of various machine learning algorithms (Non-online learning)")
    plt.grid(linestyle='-.')
    plt.legend(loc=[1, 0])

#####################################################################################################################################
    # online_MAE_list = [[] for i in range(7)]
    # online_MSE_list = [[] for i in range(7)]
    # online_R_Squared_list = [[] for i in range(7)]
    # online_MAE_list[0]=lists1[0]
    # online_MAE_list[1]=lists2[0]
    # online_MAE_list[2]=lists3[0]
    # online_MAE_list[3]=lists4[0]
    # online_MAE_list[4]=lists5[0]
    # online_MAE_list[5]=lists6[0]
    # online_MAE_list[6]=lists7[0]
    #
    # online_MSE_list[0] = lists1[1]
    # online_MSE_list[1] = lists2[1]
    # online_MSE_list[2] = lists3[1]
    # online_MSE_list[3] = lists4[1]
    # online_MSE_list[4] = lists5[1]
    # online_MSE_list[5] = lists6[1]
    # online_MSE_list[6] = lists7[1]
    #
    # online_R_Squared_list[0] = lists1[2]
    # online_R_Squared_list[1] = lists2[2]
    # online_R_Squared_list[2] = lists3[2]
    # online_R_Squared_list[3] = lists4[2]
    # online_R_Squared_list[4] = lists5[2]
    # online_R_Squared_list[5] = lists6[2]
    # online_R_Squared_list[6] = lists7[2]
    #
    # plt.figure(figsize=(8, 5))
    # times = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    # x = np.arange(20, 350)
    # l1 = plt.plot(times, online_MAE_list[0], 'r--', label='Logistic Regression')
    # l2 = plt.plot(times, online_MAE_list[1], 'g--', label='Linear SVM')
    # l3 = plt.plot(times, online_MAE_list[2], 'b--', label='Non-linear SVM')
    # l4 = plt.plot(times, online_MAE_list[3], 'k--', label='Decision Tree')
    # l5 = plt.plot(times, online_MAE_list[4], 'y--', label='Random Forest')
    # l6 = plt.plot(times, online_MAE_list[5], 'c--', label='Gaussian Naive Bayes')
    # l7 = plt.plot(times, online_MAE_list[6], 'm--', label='K Nearest Neighbors Classifier')
    # #plt.plot(times, online_MAE_list[0], 'ro-', times, online_MAE_list[1], 'g+-', times, online_MAE_list[2], 'b^-', times, online_MAE_list[3], 'k*-', times, online_MAE_list[4], 'y+-', times, online_MAE_list[5], 'c^-', times, online_MAE_list[6], 'm*-' )
    # plt.plot(times, online_MAE_list[0], 'ro-', times, online_MAE_list[1], 'g+-', times, online_MAE_list[2], 'b^-', times, online_MAE_list[3], 'k*-', times, online_MAE_list[4], 'y+-', times, online_MAE_list[5], 'c^-')
    # #plt.title('The MAE scores of various online ML algorithms.')
    # #plt.xlabel('Various online ML algorithms')
    # plt.ylabel('MAE')
    # plt.grid(linestyle='-.')
    # plt.legend()
    #
    # plt.figure(figsize=(8, 5))
    # times = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    # #x = np.arange(20, 350)
    # l1 = plt.plot(times, online_MSE_list[0], 'r--', label='Logistic Regression')
    # l2 = plt.plot(times, online_MSE_list[1], 'g--', label='Linear SVM')
    # l3 = plt.plot(times, online_MSE_list[2], 'b--', label='Non-linear SVM')
    # l4 = plt.plot(times, online_MSE_list[3], 'k--', label='Decision Tree')
    # l5 = plt.plot(times, online_MSE_list[4], 'y--', label='Random Forest')
    # l6 = plt.plot(times, online_MSE_list[5], 'c--', label='Gaussian Naive Bayes')
    # l7 = plt.plot(times, online_MSE_list[6], 'm--', label='K Nearest Neighbors Classifier')
    # # plt.plot(times, online_MSE_list[0], 'ro-', times, online_MSE_list[1], 'g+-', times, online_MSE_list[2], 'b^-', times,
    # #      online_MSE_list[3], 'k*-', times, online_MSE_list[4], 'y+-', times, online_MSE_list[5], 'c^-', times,
    # #      online_MSE_list[6], 'm*-')
    # plt.plot(times, online_MAE_list[0], 'ro-', times, online_MAE_list[1], 'g+-', times, online_MAE_list[2], 'b^-', times, online_MAE_list[3], 'k*-', times, online_MAE_list[4], 'y+-', times, online_MAE_list[5], 'c^-' )
    # #plt.title('The MSE scores of various online ML algorithms.')
    # #plt.xlabel('Various online ML algorithms')
    # plt.ylabel('MSE')
    # plt.grid(linestyle='-.')
    # plt.legend()
    #
    #
    #
    # plt.figure(figsize=(8, 5))
    # times = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    # #x = np.arange(20, 350)
    # l1 = plt.plot(times, online_R_Squared_list[0], 'r--', label='Logistic Regression')
    # l2 = plt.plot(times, online_R_Squared_list[1], 'g--', label='Linear SVM')
    # l3 = plt.plot(times, online_R_Squared_list[2], 'b--', label='Non-linear SVM')
    # l4 = plt.plot(times, online_R_Squared_list[3], 'k--', label='Decision Tree')
    # l5 = plt.plot(times, online_R_Squared_list[4], 'y--', label='Random Forest')
    # l6 = plt.plot(times, online_R_Squared_list[5], 'c--', label='Gaussian Naive Bayes')
    # l7 = plt.plot(times, online_R_Squared_list[6], 'm--', label='K Nearest Neighbors Classifier')
    # plt.plot(times, online_R_Squared_list[0], 'ro-', times, online_R_Squared_list[1], 'g+-', times, online_R_Squared_list[2], 'b^-', times,
    #      online_R_Squared_list[3], 'k*-', times, online_R_Squared_list[4], 'y+-', times, online_R_Squared_list[5], 'c^-', times,
    #      online_R_Squared_list[6], 'm*-')
    # #plt.title('The R Squared scores of various online ML algorithms.')
    # #plt.xlabel('Various online ML algorithms')
    # plt.ylabel('$R^2$')
    # plt.grid(linestyle='-.')
    # plt.legend()
    ##############################################################################################################################################################
#

    plt.figure(figsize=(10, 8))
    #plt.bar(np.arange(len(bytes_MAE_list)), height=elapsed_time_list, width=0.8, alpha=0.8, color='red',label="MAE")
    #rects1 = plt.bar(np.arange(len(bytes_MAE_list)), height=elapsed_time_list, width=0.6, alpha=0.8, color='red', label="MAE")

    plt.bar(np.arange(len(bytes_MAE_list_means)), elapsed_time_train_list_means, yerr=elapsed_time_train_list_interval, color='green', label='$Elapsed\_time_{train}$')
    plt.bar(np.arange(len(bytes_MAE_list_means)), elapsed_time_predict_list_means, yerr=elapsed_time_predict_list_interval, bottom=elapsed_time_train_list_means, color='red', label='$Elapsed\_time_{predict}$')
    plt.legend(loc=[1, 0])
    plt.ylabel("Elapsed time/s")

    plt.xticks([j for j in np.arange(len(bytes_MAE_list_means))], label_list)
    # plt.xticks([index + 0.1 for index in np.arange(len(bytes_MAE_list))])
    plt.xlabel("Various machine learning algorithms ")
    #plt.title("Elapsed time of various machine learning algorithms (Non-online learning)")
    plt.grid(linestyle='-.')

    ##############################################################################################################################################################
    plt.show()

    print('#################################################################################################')
    print('#################################################################################################')

