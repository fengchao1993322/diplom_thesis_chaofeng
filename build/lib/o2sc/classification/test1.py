
"""
comparison MAE, MSE and R Square between non-online and online machine learning, data based on bytes
"""

# Authors: Chao Feng <friedrich1993322@163.com>

import sys
import os
import timeit
import psutil
import gc

import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import pandas as pd
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from sklearn.preprocessing import label_binarize
import csv
from itertools import cycle
import math

# from . import summaryFunc as sFunc
# from . import synthesizedPlot as sPlot
import functions_chao as cFunc
import summaryFunc as sFunc
import synthesizedPlot as sPlot






if __name__ == "__main__":
#def comparison():
    #loadingPath = "/home/chao/Desktop/o2sc/pcaps/streams/vlc_v6_mts_gsm_s16l_ts_rtp_2_fixed_pkt_size.pcap"
    #loadingPath = "/home/chao/Desktop/o2sc/pcaps/streams/easyRtp_test_100_1000.pcap"
    loadingPath = "/home/chao/Desktop/o2sc/pcaps/headers/hardRtpv6NoCsrc_noPayl.pcap"
    pktsDataFloat = sFunc.readPcap(loadingPath)
    Numberofbit = 8
    '''
    6 features of one bit(package index meanless due to probabily the packages arrive disorderedly):
        feature 0: bit position;
        feature 1: package index;(removed)
        feature 2: bit value;
        feature 3: value of its previous bit in column parallelly;
        feature 4: value of its next bit in column parallelly;
        feature 5: value of its last bit horizontally in the same package; 
        feature 6: value of its next bit horizontally in the same package;
    '''
    featuresOfOnebit = 6
    ColumnNumber = pktsDataFloat.shape[1] * Numberofbit
    bitInfoArray = np.zeros(shape=(len(pktsDataFloat) * ColumnNumber, featuresOfOnebit))

    bitValue = np.zeros(shape=(len(pktsDataFloat), ColumnNumber))
    print(pktsDataFloat)

    for rowIndexOfPktsDataframe in range(len(pktsDataFloat)):
        for columnIndexOfPktsDataframe in range(ColumnNumber):
            if rowIndexOfPktsDataframe == 0:
                if columnIndexOfPktsDataframe == 0:
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    Binary1=cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe//8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = Binary1[columnIndexOfPktsDataframe%8]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = 0
                    Binary2 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe+1,(columnIndexOfPktsDataframe//8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = Binary2[columnIndexOfPktsDataframe%8]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = 0
                    Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe+1)//8])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary3[columnIndexOfPktsDataframe%8]

                elif columnIndexOfPktsDataframe == ColumnNumber - 1:

                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    Binary1 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe//8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = Binary1[columnIndexOfPktsDataframe%8]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = 0
                    Binary2 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe+1,(columnIndexOfPktsDataframe//8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = Binary2[columnIndexOfPktsDataframe%8]
                    Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe-1)//8])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary3[columnIndexOfPktsDataframe%8]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = 0

                else:

                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    Binary1 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe//8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = Binary1[columnIndexOfPktsDataframe%8]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = 0
                    Binary2 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe+1,(columnIndexOfPktsDataframe//8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = Binary2[columnIndexOfPktsDataframe%8]
                    Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe-1)//8])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary3[(columnIndexOfPktsDataframe-1)%8]
                    Binary4 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe+1)//8])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary4[(columnIndexOfPktsDataframe+1)%8]

            elif rowIndexOfPktsDataframe == len(pktsDataFloat) - 1:
                if columnIndexOfPktsDataframe == 0:

                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    Binary1 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = Binary1[columnIndexOfPktsDataframe%8]
                    Binary2 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe-1,(columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = Binary2[columnIndexOfPktsDataframe%8]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = 0
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = 0
                    Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe, (columnIndexOfPktsDataframe + 1) // 8])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary3[columnIndexOfPktsDataframe % 8]

                elif columnIndexOfPktsDataframe == ColumnNumber - 1:

                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    Binary1 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = Binary1[columnIndexOfPktsDataframe%8]
                    Binary2 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe - 1, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = Binary2[columnIndexOfPktsDataframe % 8]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = 0
                    Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe-1)//8])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary3[columnIndexOfPktsDataframe%8]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = 0

                else:

                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    Binary1 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = Binary1[columnIndexOfPktsDataframe%8]
                    Binary2 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe - 1, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = Binary2[columnIndexOfPktsDataframe % 8]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = 0
                    Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe, (columnIndexOfPktsDataframe - 1) // 8])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary3[columnIndexOfPktsDataframe % 8]
                    Binary4 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe+1)//8])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary4[(columnIndexOfPktsDataframe+1)%8]


            else:
                if columnIndexOfPktsDataframe == 0:
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    Binary1 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = Binary1[columnIndexOfPktsDataframe%8]
                    Binary2 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe - 1, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = Binary2[columnIndexOfPktsDataframe % 8]
                    Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe+1,(columnIndexOfPktsDataframe//8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = Binary3[columnIndexOfPktsDataframe%8]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = 0
                    Binary4 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe+1)//8])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary4[columnIndexOfPktsDataframe%8]

                elif columnIndexOfPktsDataframe == ColumnNumber - 1:

                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    Binary1 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = Binary1[columnIndexOfPktsDataframe%8]
                    Binary2 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe - 1, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = Binary2[columnIndexOfPktsDataframe % 8]
                    Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe+1,(columnIndexOfPktsDataframe//8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = Binary3[columnIndexOfPktsDataframe%8]
                    Binary4 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe-1)//8])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary4[columnIndexOfPktsDataframe%8]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = 0


                else:

                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    Binary1 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = Binary1[columnIndexOfPktsDataframe%8]
                    Binary2 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe - 1, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = Binary2[columnIndexOfPktsDataframe % 8]
                    Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe+1,(columnIndexOfPktsDataframe//8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = Binary3[columnIndexOfPktsDataframe%8]
                    Binary4 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe-1)//8])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary4[columnIndexOfPktsDataframe%8]
                    Binary5 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe+1)//8])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary5[columnIndexOfPktsDataframe%8]

            bitValue[rowIndexOfPktsDataframe][columnIndexOfPktsDataframe]=bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1]

    print("---------------------")
    print(bitInfoArray)
    print(bitInfoArray.shape)

    print("-----------------")
    print(bitValue)
    print(bitValue.shape)