import sys
from binascii import hexlify
import summaryFunc as sFunc
import synthesizedPlot as sPlot
# from . import summaryFunc as sFunc
# from . import synthesizedPlot as sPlot

import numpy as np
import pandas as pd
import scapy.all as scapy
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
# optimalize the ML, make it standardlized
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import label_binarize

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt

loadingPath = "/home/student/Documents/aa_scripter/test5.pcap"

pktsDataFloat = sFunc.readPcap(loadingPath)
#print('pktsDataframeFloat: ', pktsDataFloat)

'''
6 features of one byte(package index meanless due to probabily the packages arrive disorderedly):
    feature 0: byte position;
    feature 1: package index;(removed)
    feature 2: byte value;
    feature 3: value of its previous byte in column parallelly;
    feature 4: value of its next byte in column parallelly;
    feature 5: value of its last byte horizontally in the same packet;
    feature 6: value of its next byte horizontally in the same packet;
'''
featuresOfOneByte = 6
byteInfoArray=np.zeros(shape=(len(pktsDataFloat)*pktsDataFloat.shape[1], featuresOfOneByte))

for rowIndexOfPktsDataframe in range(len(pktsDataFloat)):
    for columnIndexOfPktsDataframe in range(pktsDataFloat.shape[1]):
        if rowIndexOfPktsDataframe == 0: 
            if columnIndexOfPktsDataframe ==0:
                byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
            elif columnIndexOfPktsDataframe ==pktsDataFloat.shape[1]-1:
                byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], 0 ,pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], 0]
            else:
                byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], 0 ,pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
        elif rowIndexOfPktsDataframe == len(pktsDataFloat)-1:
            if columnIndexOfPktsDataframe ==0:
                byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], 0, 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
            elif columnIndexOfPktsDataframe == pktsDataFloat.shape[1]-1:
                byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], 0]
            else:
                byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
        else:
            if columnIndexOfPktsDataframe ==0:
                byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
            elif columnIndexOfPktsDataframe == pktsDataFloat.shape[1]-1:
                byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], 0]
            else:
                byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
# print(byteInfoArray)

byteLabel2dArray = sFunc.byteLabelPreOperation(pktsDataFloat)

labels = sFunc.outputLabel(byteLabel2dArray.ravel())


testSize = 0.5
randomState=0
X_train, X_test, y_train, y_test = sFunc.trainTestSeparate(byteInfoArray, byteLabel2dArray.ravel(), testSize,random_state=randomState)

y_binarized = label_binarize(y_train, classes=labels)

X_train_std, X_test_std = sFunc.dataStandardization(X_train, X_test)

class_names = ['Constant','Increment','Decrement','Random']

mp = sFunc.classifierAndItsParameters(MlModel = 'NeuralNetwork')

'''
    For neural network it doesn't need input train set here, only builds model in function 'buildingModelAndFittingTrainSet()'.
    Configurations use keras own commands. 
'''
neuralnet = mp.buildingModelAndFittingTrainSet(None, None)
neuralnet.add(keras.layers.Dense(6, activation=tf.nn.relu))
neuralnet.add(keras.layers.Dense(24, activation=tf.nn.relu))
neuralnet.add(keras.layers.Dense(12, activation=tf.nn.relu))
neuralnet.add(keras.layers.Dense(4, activation=tf.nn.softmax))


neuralnet.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])


H=neuralnet.fit(X_train_std, y_train, batch_size=20, epochs=50)

test_loss, test_acc = neuralnet.evaluate(X_test_std, y_test)

print('Test accuracy:', test_acc)

predictions = neuralnet.predict(X_test_std)
print(predictions)

A,B,C = sFunc.Cal_nn_ROC_parameters(neuralnet, X_test_std, y_binarized)
D, E,F,G=sFunc.Cal_nn_PR_parameters(neuralnet,X_test_std,y_binarized,labels)

print(A)
print(B)
print(C)
print(D)
print(E)
print(F)
print(G)


#print('prediction[0]: ', predictions[0])
# print('true y_test[0]: ', y_test[16])
# print('prediction[0]: ', np.argmax(predictions[20]))
# print('true y_test[0]: ', y_test[20])
# print('prediction[0]: ', np.argmax(predictions[27]))
# print('true y_test[0]: ', y_test[27])
# print('prediction[0]: ', np.argmax(predictions[23]))
# print('true y_test[0]: ', y_test[23])
# print('prediction[0]: ', np.argmax(predictions[41]))
# print('true y_test[0]: ', y_test[41])

plt.figure()
sPlot.plot_Acc_Loss(H.history['acc'],H.history['loss'])
plt.show()
plt.pause(3)
plt.close()





