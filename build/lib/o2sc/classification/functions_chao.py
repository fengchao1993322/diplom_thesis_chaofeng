"""
Subfunctions were written by chao
"""

# Authors: Chao Feng <friedrich1993322@163.com>

import sys
import os
import timeit
import psutil
import gc

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from sklearn.preprocessing import label_binarize
import csv
from itertools import cycle

# from . import summaryFunc as sFunc
# from . import synthesizedPlot as sPlot
import summaryFunc as sFunc
import synthesizedPlot as sPlot

from memory_profiler import profile



@profile
def LogisticRegression(X_train_std, X_test_std, y_train):
    start0=timeit.default_timer()
    mlModel0 = sFunc.classifierAndItsParameters(MlModel='LogisticRegression', lr_C=10.0, lr_class_weight= 'balanced', lr_solver='liblinear', lr_multi_class= 'auto' )
    clf0 = mlModel0.buildingModelAndFittingTrainSet(X_train_std, y_train)
    end0 = timeit.default_timer()
    start1 = timeit.default_timer()
    y_pred = clf0.predict(X_test_std)
    end1=timeit.default_timer()
    elapsed_time1 = end0 - start0
    elapsed_time2 = end1 - start1
    return y_pred, elapsed_time1, elapsed_time2, clf0

@profile
def LogisticRegression_OnevsRest(X_train_std, X_test_std, y_train_m, multiFlag):
    start0=timeit.default_timer()
    #mlModel0 = sFunc.classifierAndItsParameters(MlModel = 'LogisticRegression', lr_C=10.0, lr_class_weight= 'balanced', lr_solver='liblinear', lr_multi_class= 'auto' )
    mlModel0_m = sFunc.classifierAndItsParameters(MlModel='LogisticRegression', multiLabel=multiFlag, lr_C=10.0, lr_class_weight='balanced', lr_solver='liblinear', lr_multi_class='auto')
    #clf0 = mlModel0_m.buildingModelAndFittingTrainSet(X_train_std, y_train)
    clf0_m = mlModel0_m.buildingModelAndFittingTrainSet(X_train_std, y_train_m)
    end0 = timeit.default_timer()
    start1 = timeit.default_timer()
    y_pred = clf0_m.predict(X_test_std)
    end1=timeit.default_timer()
    elapsed_time1 = end0 - start0
    elapsed_time2 = end1 - start1
    return y_pred, elapsed_time1, elapsed_time2, clf0_m

@profile
def LinearSVC(X_train_std, X_test_std, y_train):
    start0 = timeit.default_timer()
    mlModel1 = sFunc.classifierAndItsParameters(MlModel='LinearSVC', lsvc_C=1.0, lsvc_max_iter=10000, lsvc_class_weight='balanced')
    clf1 = mlModel1.buildingModelAndFittingTrainSet(X_train_std, y_train)
    end0 = timeit.default_timer()
    start1 = timeit.default_timer()
    y_pred = clf1.predict(X_test_std)
    end1 = timeit.default_timer()
    elapsed_time1 = end0 - start0
    elapsed_time2 = end1 - start1
    return y_pred, elapsed_time1, elapsed_time2, clf1

@profile
def LinearSVC_OnevsRest(X_train_std, X_test_std, y_train_m, multiFlag):
    start0=timeit.default_timer()
    mlModel1_m = sFunc.classifierAndItsParameters(MlModel='LinearSVC', multiLabel=multiFlag, lsvc_C=1.0, lsvc_max_iter=10000, lsvc_class_weight='balanced')
    clf1_m = mlModel1_m.buildingModelAndFittingTrainSet(X_train_std, y_train_m)
    end0 = timeit.default_timer()
    start1 = timeit.default_timer()
    y_pred = clf1_m.predict(X_test_std)
    end1=timeit.default_timer()
    elapsed_time1 = end0 - start0
    elapsed_time2 = end1 - start1
    return y_pred, elapsed_time1, elapsed_time2, clf1_m

@profile
def SVC(X_train_std, X_test_std, y_train):
    start0=timeit.default_timer()
    mlModel2 = sFunc.classifierAndItsParameters(MlModel = 'SVC', svc_kernel= 'rbf', svc_C=1.0, svc_gamma = 'scale', svc_class_weight='balanced' )
    clf2 = mlModel2.buildingModelAndFittingTrainSet(X_train_std, y_train)
    end0 = timeit.default_timer()
    start1 = timeit.default_timer()
    y_pred=clf2.predict(X_test_std)
    end1 = timeit.default_timer()
    elapsed_time1 = end0 - start0
    elapsed_time2 = end1 - start1
    return y_pred, elapsed_time1, elapsed_time2, clf2

@profile
def SVC_OnevsRest(X_train_std, X_test_std, y_train_m, multiFlag):
    start0=timeit.default_timer()
    mlModel2_m = sFunc.classifierAndItsParameters(MlModel='SVC', multiLabel=multiFlag, svc_kernel='rbf', svc_C=1.0, svc_gamma='scale', svc_class_weight='balanced')
    clf2_m = mlModel2_m.buildingModelAndFittingTrainSet(X_train_std, y_train_m)
    end0 = timeit.default_timer()
    start1 = timeit.default_timer()
    y_pred = clf2_m.predict(X_test_std)
    end1=timeit.default_timer()
    elapsed_time1 = end0 - start0
    elapsed_time2 = end1 - start1
    return y_pred, elapsed_time1, elapsed_time2, clf2_m

@profile
def DecisionTreeClassifier(X_train_std, X_test_std, y_train):
    start0=timeit.default_timer()
    mlModel3 = sFunc.classifierAndItsParameters(MlModel = 'DecisionTreeClassifier', dt_class_weight='balanced', dt_criterion='entropy' )
    clf3 = mlModel3.buildingModelAndFittingTrainSet(X_train_std, y_train)
    end0 = timeit.default_timer()
    start1 = timeit.default_timer()
    y_pred=clf3.predict(X_test_std)
    end1 = timeit.default_timer()
    elapsed_time1 = end0 - start0
    elapsed_time2 = end1 - start1
    return y_pred, elapsed_time1, elapsed_time2, clf3

@profile
def DecisionTreeClassifier_OnevsRest(X_train_std, X_test_std, y_train_m, multiFlag):
    start0=timeit.default_timer()
    mlModel3_m = sFunc.classifierAndItsParameters(MlModel='DecisionTreeClassifier', multiLabel=multiFlag, dt_class_weight='balanced', dt_criterion='entropy')
    clf3_m = mlModel3_m.buildingModelAndFittingTrainSet(X_train_std, y_train_m)
    end0 = timeit.default_timer()
    start1 = timeit.default_timer()
    y_pred = clf3_m.predict(X_test_std)
    end1=timeit.default_timer()
    elapsed_time1 = end0 - start0
    elapsed_time2 = end1 - start1
    return y_pred, elapsed_time1, elapsed_time2, clf3_m

@profile
def RandomForestClassifier(X_train_std, X_test_std, y_train):
    start0=timeit.default_timer()
    mlModel4 = sFunc.classifierAndItsParameters(MlModel = 'RandomForestClassifier', rf_n_estimators=20, rf_class_weight='balanced')
    clf4 = mlModel4.buildingModelAndFittingTrainSet(X_train_std, y_train)
    end0 = timeit.default_timer()
    start1 = timeit.default_timer()
    y_pred=clf4.predict(X_test_std)
    end1 = timeit.default_timer()
    elapsed_time1=end0-start0
    elapsed_time2 = end1 - start1
    return y_pred, elapsed_time1, elapsed_time2, clf4

@profile
def RandomForestClassifier_OnevsRest(X_train_std, X_test_std, y_train_m, multiFlag):
    start0=timeit.default_timer()
    mlModel4_m = sFunc.classifierAndItsParameters(MlModel='RandomForestClassifier', multiLabel=multiFlag, rf_n_estimators=20, rf_class_weight='balanced')
    clf4_m = mlModel4_m.buildingModelAndFittingTrainSet(X_train_std, y_train_m)
    end0 = timeit.default_timer()
    start1 = timeit.default_timer()
    y_pred = clf4_m.predict(X_test_std)
    end1=timeit.default_timer()
    elapsed_time1 = end0 - start0
    elapsed_time2=end1-start1
    return y_pred, elapsed_time1, elapsed_time2, clf4_m

@profile
def GaussianNB(X_train_std, X_test_std, y_train):
    start0=timeit.default_timer()
    mlModel5 = sFunc.classifierAndItsParameters(MlModel = 'GaussianNB')
    clf5 = mlModel5.buildingModelAndFittingTrainSet(X_train_std, y_train)
    end0 = timeit.default_timer()
    start1 = timeit.default_timer()
    y_pred=clf5.predict(X_test_std)
    end1 = timeit.default_timer()
    elapsed_time1=end0-start0
    elapsed_time2 = end1 - start1
    return y_pred, elapsed_time1, elapsed_time2, clf5

@profile
def GaussianNB_OnevsRest(X_train_std, X_test_std, y_train_m, multiFlag):
    start0=timeit.default_timer()
    mlModel5_m = sFunc.classifierAndItsParameters(MlModel='GaussianNB', multiLabel=multiFlag)
    clf5_m = mlModel5_m.buildingModelAndFittingTrainSet(X_train_std, y_train_m)
    end0 = timeit.default_timer()
    start1 = timeit.default_timer()
    y_pred = clf5_m.predict(X_test_std)
    end1=timeit.default_timer()
    elapsed_time1=end0-start0
    elapsed_time2 = end1 - start1
    return y_pred, elapsed_time1, elapsed_time2, clf5_m

@profile
def KNeighborsClassifier(X_train_std, X_test_std, y_train):
    start0=timeit.default_timer()
    mlModel6 = sFunc.classifierAndItsParameters(MlModel = 'KNeighborsClassifier', knn_algorithm='auto')
    clf6 = mlModel6.buildingModelAndFittingTrainSet(X_train_std, y_train)
    end0 = timeit.default_timer()
    start1 = timeit.default_timer()
    y_pred=clf6.predict(X_test_std)
    end1 = timeit.default_timer()
    elapsed_time1=end0-start0
    elapsed_time2 = end1 - start1
    return y_pred, elapsed_time1, elapsed_time2, clf6

@profile
def KNeighborsClassifier_OnevsRest(X_train_std, X_test_std, y_train_m, multiFlag):
    start0=timeit.default_timer()
    mlModel6_m = sFunc.classifierAndItsParameters(MlModel='KNeighborsClassifier', multiLabel=multiFlag, knn_algorithm='auto')
    clf6_m = mlModel6_m.buildingModelAndFittingTrainSet(X_train_std, y_train_m)
    end0 = timeit.default_timer()
    start1 = timeit.default_timer()
    y_pred = clf6_m.predict(X_test_std)
    end1=timeit.default_timer()
    elapsed_time1=end0-start0
    elapsed_time2 = end1 - start1
    return y_pred, elapsed_time1, elapsed_time2, clf6_m


def DecimalToBinary(num):
    Binary=np.zeros(8)
    Binary[7] = num % 2
    Binary[6] = num // 2 % 2
    Binary[5] = num // 2 // 2 % 2
    Binary[4] = num // 2 // 2 // 2 % 2
    Binary[3] = num // 2 // 2 // 2 // 2 % 2
    Binary[2] = num // 2 // 2 // 2 // 2 // 2 % 2
    Binary[1] = num // 2 // 2 // 2 // 2 // 2 // 2 % 2
    Binary[0] = num // 2 // 2 // 2 // 2 // 2 // 2 // 2 % 2
    return Binary



def bitLabelPreOperation(pktsDataframe):
    resultPkts2dArray = np.empty([len(pktsDataframe), pktsDataframe.shape[1]])
    '''
    In the package data preparation,
    0 means static;
    9 means non-static
    '''
    for columnIndexOfPktsDataframe in range(pktsDataframe.shape[1]):
        Static = True


        for rowIndexOfPktsDataframe in range(len(pktsDataframe)):
            if rowIndexOfPktsDataframe == 0:
                pass
            else:
                if pktsDataframe.iat[
                        rowIndexOfPktsDataframe,
                        columnIndexOfPktsDataframe] == pktsDataframe.iat[
                            rowIndexOfPktsDataframe -
                            1, columnIndexOfPktsDataframe]:
                    if rowIndexOfPktsDataframe ==1:
                        resultPkts2dArray[rowIndexOfPktsDataframe-1,
                                      columnIndexOfPktsDataframe] = 0

                    resultPkts2dArray[rowIndexOfPktsDataframe,
                                      columnIndexOfPktsDataframe] = 0
                else:
                    if rowIndexOfPktsDataframe == 1:
                        resultPkts2dArray[rowIndexOfPktsDataframe-1, columnIndexOfPktsDataframe] = 9
                        Static = False

                    resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe] = 9




        '''
            'changingNum<=2' limits the continuity, if changingNum is big, then the byte position random
        '''
        if  Static == False:
            resultPkts2dArray[:, columnIndexOfPktsDataframe] = 9

    return resultPkts2dArray



############################################################################################################################################################################################################

def wordLabelPreOperation(pktsDataframe, NumberofBytes):
    resultPkts2dArray = np.empty([len(pktsDataframe), pktsDataframe.shape[1]])
    '''
    In the package data preparation,
    0 means static;
    1 means increment;
    2 means decrement;
    3 means random.
    '''
    for columnIndexOfPktsDataframe in range(pktsDataframe.shape[1]):
        NumOfIncr = 0
        NumOfDecr = 0
        NumOfCons = 0
        Incr = False
        Decr = False
        Cons = False
        # 'changingNum' means the word info changing times, except the first packet.
        changingNum = 0
        for rowIndexOfPktsDataframe in range(len(pktsDataframe)):
            if rowIndexOfPktsDataframe == 0:
                pass
            else:
                if pktsDataframe.iat[
                        rowIndexOfPktsDataframe,
                        columnIndexOfPktsDataframe] == pktsDataframe.iat[
                            rowIndexOfPktsDataframe -
                            1, columnIndexOfPktsDataframe]:
                    if rowIndexOfPktsDataframe ==1:
                        resultPkts2dArray[rowIndexOfPktsDataframe-1,
                                      columnIndexOfPktsDataframe] = 0
                        NumOfCons += 1
                        Cons = True
                    resultPkts2dArray[rowIndexOfPktsDataframe,
                                      columnIndexOfPktsDataframe] = 0
                    NumOfCons += 1
                    Cons = True
                elif pktsDataframe.iat[
                        rowIndexOfPktsDataframe,
                        columnIndexOfPktsDataframe] > pktsDataframe.iat[
                            rowIndexOfPktsDataframe -
                            1, columnIndexOfPktsDataframe]:
                    if ((pktsDataframe.iat[rowIndexOfPktsDataframe,
                                           columnIndexOfPktsDataframe] == 256**NumberofBytes-1)
                            or
                        (pktsDataframe.iat[rowIndexOfPktsDataframe,
                                           columnIndexOfPktsDataframe] == 256**NumberofBytes-2)
                        ) and (pktsDataframe.iat[rowIndexOfPktsDataframe -
                                                 1, columnIndexOfPktsDataframe]
                               == 0):
                        if rowIndexOfPktsDataframe == 1:
                            resultPkts2dArray[rowIndexOfPktsDataframe-1,
                                          columnIndexOfPktsDataframe] = 2
                            NumOfDecr += 1
                            Decr = True
                        resultPkts2dArray[rowIndexOfPktsDataframe,
                                          columnIndexOfPktsDataframe] = 2
                        NumOfDecr += 1
                        Decr = True
                    else:
                        if rowIndexOfPktsDataframe == 1:
                            resultPkts2dArray[rowIndexOfPktsDataframe-1,
                                          columnIndexOfPktsDataframe] = 1
                            NumOfIncr += 1
                            Incr = True
                        resultPkts2dArray[rowIndexOfPktsDataframe,
                                          columnIndexOfPktsDataframe] = 1
                        NumOfIncr += 1
                        Incr = True
                else:
                    if (pktsDataframe.iat[rowIndexOfPktsDataframe,
                                          columnIndexOfPktsDataframe] == 0
                        ) and ((pktsDataframe.
                                iat[rowIndexOfPktsDataframe -
                                    1, columnIndexOfPktsDataframe] == 256**NumberofBytes-1) or
                               (pktsDataframe.
                                iat[rowIndexOfPktsDataframe -
                                    1, columnIndexOfPktsDataframe] == 256**NumberofBytes-2)):
                        if rowIndexOfPktsDataframe == 1:
                            resultPkts2dArray[rowIndexOfPktsDataframe-1,
                                          columnIndexOfPktsDataframe] = 1
                            NumOfIncr += 1
                            Incr = True
                        resultPkts2dArray[rowIndexOfPktsDataframe,
                                          columnIndexOfPktsDataframe] = 1
                        NumOfIncr += 1
                        Incr = True
                    else:
                        if rowIndexOfPktsDataframe == 1:
                            resultPkts2dArray[rowIndexOfPktsDataframe-1,
                                          columnIndexOfPktsDataframe] = 2
                            NumOfDecr += 1
                            Decr = True
                        resultPkts2dArray[rowIndexOfPktsDataframe,
                                          columnIndexOfPktsDataframe] = 2
                        NumOfDecr += 1
                        Decr = True

            if rowIndexOfPktsDataframe == 1:
                if resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe] == 0:
                    posiState = 0
                elif resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe] == 1:
                    posiState = 1
                elif resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe] == 2:
                    posiState = 2

            if rowIndexOfPktsDataframe > 1:
                if resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe] == posiState:
                    pass
                else:
                    posiState = resultPkts2dArray[rowIndexOfPktsDataframe, columnIndexOfPktsDataframe]
                    changingNum +=1
        '''
            'changingNum<=2' limits the continuity, if changingNum is big, then the byte position random
        '''
        if ((Incr and Decr) or (Incr and Cons) or (Cons and Decr))and(changingNum > 2):
            resultPkts2dArray[:, columnIndexOfPktsDataframe] = 3
            incrPercentage = float(NumOfIncr) / len(pktsDataframe)
            decrPercentage = float(NumOfDecr) / len(pktsDataframe)
            consPercentage = float(NumOfCons) / len(pktsDataframe)
            # print("random column's Index : ", columnIndexOfPktsDataframe,
            #       '; Total quantity of this column elements: ',
            #       len(pktsDataframe))
            # print('posiStateChangingTimes: ', changingNum)
            # print('Increment number: ', NumOfIncr, '; Percentage: ',
            #       incrPercentage)
            # print('Decrement number: ', NumOfDecr, '; Percentage: ',
            #       decrPercentage)
            # print('Constant number: ', NumOfCons, '; Percentage: ',
            #       consPercentage)

    # print("result Pkts 2d Array: ", resultPkts2dArray)
    return resultPkts2dArray