import sys
from binascii import hexlify

import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression

import summaryFunc as sFunc

loadingPath = "/home/student/Documents/aa_scripter/test3.pcap"
pktsDataFloat = sFunc.readPcap(loadingPath)
resultPkts2dArray = sFunc.byteLabelPreOperation(pktsDataFloat)
pktsDataFloat.index = sFunc.pktLabelGeneration(resultPkts2dArray)

###################################################################################
testSize = 0.2
X_train, X_test, y_train, y_test = sFunc.trainTestSeparate(pktsDataFloat, pktsDataFloat.index, testSize)

X_train_std, X_test_std = sFunc.dataStandardization(X_train, X_test)
_, pktsDataframe_std = sFunc.dataStandardization(X_train, pktsDataFloat)


lr = LogisticRegression(C=100000.0, random_state=0, class_weight= 'balanced', solver='liblinear', multi_class= 'auto') 
lr.fit(X_train_std, y_train) 

print('Test accuracy:', lr.score(X_test_std, y_test))
print(lr.classes_)
'''
check sample's probality, 
                ['01000300203300000000' '10010300113311101111']
example index:    rest                   0                     
'''
print('pktsDataframe_Std[0,:]: ', lr.predict_proba(pktsDataframe_std)[0,:])
print('pktsDataframe_Std[1,:]: ', lr.predict_proba(pktsDataframe_std)[1,:])
print('pktsDataframe_Std[2,:]: ', lr.predict_proba(pktsDataframe_std)[2,:])
print('pktsDataframe_Std[3,:]: ', lr.predict_proba(pktsDataframe_std)[3,:])
print('pktsDataframe_Std[4,:]: ', lr.predict_proba(pktsDataframe_std)[4,:])
print('pktsDataframe_Std[191,:]: ', lr.predict_proba(pktsDataframe_std)[191,:])
print('pktsDataframe_Std[194,:]: ', lr.predict_proba(pktsDataframe_std)[194,:])

for i in range(len(X_test)):
    print('predict: ', lr.predict(X_test_std)[i], "true label: ", y_test[i])

