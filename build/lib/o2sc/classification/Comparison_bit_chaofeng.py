"""
comparison MAE, MSE and R Square between non-online and online machine learning, data based on bytes
"""

# Authors: Chao Feng <friedrich1993322@163.com>

import sys
import os
import timeit
import psutil
import gc

import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import pandas as pd
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from sklearn.preprocessing import label_binarize
import csv
from itertools import cycle
import math

# from . import summaryFunc as sFunc
# from . import synthesizedPlot as sPlot
import functions_chao as cFunc
import summaryFunc as sFunc
import synthesizedPlot as sPlot

if __name__ == "__main__":
#def comparison():
    #loadingPath = "/home/chao/Desktop/o2sc/pcaps/streams/vlc_v6_mts_gsm_s16l_ts_rtp_2_fixed_pkt_size.pcap"
    #loadingPath = "/home/chao/Desktop/o2sc/pcaps/streams/easyRtp_test_100_1000.pcap"
    loadingPath = "/home/chao/Desktop/o2sc/pcaps/headers/hardRtpv6NoCsrc_noPayl.pcap"
    pktsDataFloat = sFunc.readPcap(loadingPath)
    Numberofbit = 8
    '''
    6 features of one bit(package index meanless due to probabily the packages arrive disorderedly):
        feature 0: bit position;
        feature 1: package index;(removed)
        feature 2: bit value;
        feature 3: value of its previous bit in column parallelly;
        feature 4: value of its next bit in column parallelly;
        feature 5: value of its last bit horizontally in the same package; 
        feature 6: value of its next bit horizontally in the same package;
    '''
    featuresOfOnebit = 6
    ColumnNumber = pktsDataFloat.shape[1] * Numberofbit
    bitInfoArray = np.zeros(shape=(len(pktsDataFloat) * ColumnNumber, featuresOfOnebit))

    bitValue = np.zeros(shape=(len(pktsDataFloat), ColumnNumber))
    #print(pktsDataFloat)

    for rowIndexOfPktsDataframe in range(len(pktsDataFloat)):
        for columnIndexOfPktsDataframe in range(ColumnNumber):
            if rowIndexOfPktsDataframe == 0:
                if columnIndexOfPktsDataframe == 0:
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    Binary1=cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe//8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = Binary1[columnIndexOfPktsDataframe%8]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = 0
                    Binary2 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe+1,(columnIndexOfPktsDataframe//8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = Binary2[columnIndexOfPktsDataframe%8]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = 0
                    #Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe+1)//8])
                    if columnIndexOfPktsDataframe%8 < 7:
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary1[columnIndexOfPktsDataframe%8+1]
                    else:
                        Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe+1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary3[0]

                elif columnIndexOfPktsDataframe == ColumnNumber - 1:

                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    Binary1 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe//8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = Binary1[columnIndexOfPktsDataframe%8]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = 0
                    Binary2 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe+1,(columnIndexOfPktsDataframe//8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = Binary2[columnIndexOfPktsDataframe%8]
                    #Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe-1)//8])
                    if columnIndexOfPktsDataframe % 8 > 0:
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary1[columnIndexOfPktsDataframe%8-1]
                    else:
                        Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe, (columnIndexOfPktsDataframe - 1) // 8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary3[7]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = 0

                else:

                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    Binary1 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe//8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = Binary1[columnIndexOfPktsDataframe%8]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = 0
                    Binary2 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe+1,(columnIndexOfPktsDataframe//8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = Binary2[columnIndexOfPktsDataframe%8]
                    if (columnIndexOfPktsDataframe % 8 > 0) and (columnIndexOfPktsDataframe%8 < 7):
                    #Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe-1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary1[columnIndexOfPktsDataframe%8-1]
                    #Binary4 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe+1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary1[columnIndexOfPktsDataframe%8+1]
                    elif columnIndexOfPktsDataframe % 8 == 0:
                        Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe-1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary3[7]
                        # Binary4 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe+1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary1[columnIndexOfPktsDataframe % 8 + 1]
                    else:
                        # Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe-1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary1[columnIndexOfPktsDataframe % 8 - 1]
                        Binary4 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe+1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary4[0]



            elif rowIndexOfPktsDataframe == len(pktsDataFloat) - 1:
                if columnIndexOfPktsDataframe == 0:

                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    Binary1 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = Binary1[columnIndexOfPktsDataframe%8]
                    Binary2 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe-1,(columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = Binary2[columnIndexOfPktsDataframe%8]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = 0
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = 0
                    if columnIndexOfPktsDataframe%8 < 7:
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary1[columnIndexOfPktsDataframe%8+1]
                    else:
                        Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe+1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary3[0]

                elif columnIndexOfPktsDataframe == ColumnNumber - 1:

                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    Binary1 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = Binary1[columnIndexOfPktsDataframe%8]
                    Binary2 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe - 1, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = Binary2[columnIndexOfPktsDataframe % 8]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = 0
                    if columnIndexOfPktsDataframe % 8 > 0:
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary1[columnIndexOfPktsDataframe%8-1]
                    else:
                        Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe, (columnIndexOfPktsDataframe - 1) // 8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary3[7]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = 0

                else:

                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    Binary1 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = Binary1[columnIndexOfPktsDataframe%8]
                    Binary2 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe - 1, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = Binary2[columnIndexOfPktsDataframe % 8]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = 0
                    if (columnIndexOfPktsDataframe % 8 > 0) and (columnIndexOfPktsDataframe%8 < 7):
                    #Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe-1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary1[columnIndexOfPktsDataframe%8-1]
                    #Binary4 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe+1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary1[columnIndexOfPktsDataframe%8+1]
                    elif columnIndexOfPktsDataframe % 8 == 0:
                        Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe-1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary3[7]
                        # Binary4 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe+1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary1[columnIndexOfPktsDataframe % 8 + 1]
                    else:
                        # Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe-1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary1[columnIndexOfPktsDataframe % 8 - 1]
                        Binary4 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe+1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary4[0]



            else:
                if columnIndexOfPktsDataframe == 0:
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    Binary1 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = Binary1[columnIndexOfPktsDataframe%8]
                    Binary2 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe - 1, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = Binary2[columnIndexOfPktsDataframe % 8]
                    Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe+1,(columnIndexOfPktsDataframe//8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = Binary3[columnIndexOfPktsDataframe%8]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = 0
                    if columnIndexOfPktsDataframe%8 < 7:
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary1[columnIndexOfPktsDataframe%8+1]
                    else:
                        Binary4 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe+1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary4[0]

                elif columnIndexOfPktsDataframe == ColumnNumber - 1:

                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    Binary1 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = Binary1[columnIndexOfPktsDataframe%8]
                    Binary2 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe - 1, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = Binary2[columnIndexOfPktsDataframe % 8]
                    Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe+1,(columnIndexOfPktsDataframe//8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = Binary3[columnIndexOfPktsDataframe%8]
                    if columnIndexOfPktsDataframe % 8 > 0:
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary1[
                            columnIndexOfPktsDataframe % 8 - 1]
                    else:
                        Binary4 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe, (columnIndexOfPktsDataframe - 1) // 8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary4[7]
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = 0


                else:

                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][0] = columnIndexOfPktsDataframe
                    Binary1 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1] = Binary1[columnIndexOfPktsDataframe%8]
                    Binary2 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe - 1, (columnIndexOfPktsDataframe // 8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][2] = Binary2[columnIndexOfPktsDataframe % 8]
                    Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe+1,(columnIndexOfPktsDataframe//8)])
                    bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][3] = Binary3[columnIndexOfPktsDataframe%8]
                    if (columnIndexOfPktsDataframe % 8 > 0) and (columnIndexOfPktsDataframe%8 < 7):
                    #Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe-1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary1[columnIndexOfPktsDataframe%8-1]
                    #Binary4 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe+1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary1[columnIndexOfPktsDataframe%8+1]
                    elif columnIndexOfPktsDataframe % 8 == 0:
                        Binary4 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe-1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary4[7]
                        # Binary4 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe+1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary1[columnIndexOfPktsDataframe % 8 + 1]
                    else:
                        # Binary3 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe-1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][4] = Binary1[columnIndexOfPktsDataframe % 8 - 1]
                        Binary5 = cFunc.DecimalToBinary(pktsDataFloat.iat[rowIndexOfPktsDataframe,(columnIndexOfPktsDataframe+1)//8])
                        bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][5] = Binary5[0]

            bitValue[rowIndexOfPktsDataframe][columnIndexOfPktsDataframe]=bitInfoArray[rowIndexOfPktsDataframe * ColumnNumber + columnIndexOfPktsDataframe][1]
    print('------------------------------------------------------------------------------------------------------------------------')

    pktsDataframe = pd.DataFrame(bitValue)
    bitValueFloat = pktsDataframe.astype(float)





    bitLabel2dArray = cFunc.bitLabelPreOperation(bitValueFloat)

    labels = sFunc.outputLabel(bitLabel2dArray.ravel())
    if len(labels) <= 2:
        print('Binary classification.')
        print('original labels: ', labels)
        # the most important label which controls if it's multi-classification:
        multiFlag=False
    else:
        print('Multiple classification.')
        print('original labels: ', labels)
        # the most important label which controls if it's multi-classification:
        multiFlag=True

    # Split into training and test
    testSize = 0.5
    randomState = np.random.randint(0,100)
    y_Num = sFunc.labels2Num(labels, bitLabel2dArray.ravel())
    print('#########################')
    # print(bitInfoArray)
    #
    # print(bitInfoArray.shape)
    # #print()
    # print(y_Num)
    # print(y_Num.shape)
    print('########################')


    X_train, X_test, y_train, y_test = sFunc.trainTestSeparate(
            bitInfoArray, y_Num, testSize, random_state=randomState)



    print("number represented labels of 'y_test': \n", y_test)
    # print("binarized class labels of 'y_test': \n", y_test_m)

    X_train_std, X_test_std = sFunc.dataStandardization(X_train, X_test)


    gc.collect()

    bytes_MAE_list = list()
    bytes_MSE_list = list()
    bytes_R_square_list = list()
    elapsed_time_train_list = list()
    elapsed_time_predict_list = list()
    clf_list = list()
    model_list = list()
    memory_fit = list()
    memory_predict = list()


    y_pred, elapsed_time_train, elapsed_time_predict, clf0 = cFunc.LogisticRegression(X_train_std, X_test_std, y_train)
    print('current input Num labels: ', clf0.classes_)
    print(" 'y_pred': \n", y_pred)
    print('Test MAE of Logistic Regression:', mean_absolute_error(y_test, y_pred))
    bytes_MAE_list.append(mean_absolute_error(y_test, y_pred))
    print('Test MSE of Logistic Regression:', mean_squared_error(y_test, y_pred))
    bytes_MSE_list.append(mean_squared_error(y_test, y_pred))
    print('Test R square of Logistic Regression:', r2_score(y_test, y_pred))
    bytes_R_square_list.append(r2_score(y_test, y_pred))
    clf_list.append(clf0)
    model_list.append('LogisticRegression')

    print('Elapsed time (train): %s seconds' %elapsed_time_train)
    print('Elapsed time (predict): %s seconds' %elapsed_time_predict)
    elapsed_time_train_list.append(elapsed_time_train)
    elapsed_time_predict_list.append(elapsed_time_predict)

    memory_fit.append(24.4)
    memory_predict.append(0.9)
    '''
    online learning
    '''
    print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    lists1 = [[] for i in range(3)]
    for num in range(1,10):
        tstSize=num/10
        X_train_online, X_test_online, y_train_online, y_test_online = sFunc.trainTestSeparate(bitInfoArray, y_Num, tstSize, random_state=randomState)
        X_train_std_online, X_test_std_online = sFunc.dataStandardization(X_train_online, X_test_online)
        y_pred_online, elapsed_time_train, elapsed_time_predict, clf0 = cFunc.LogisticRegression(X_train_std_online, X_test_std_online, y_train_online)
        print(" 'y_pred': \n", y_pred_online)
        print('online learning in %s time:' %num, 'MAE',mean_absolute_error(y_test_online, y_pred_online),
              'MSE',mean_squared_error(y_test_online, y_pred_online),'R square', r2_score(y_test_online, y_pred_online))
        lists1[0].append(mean_absolute_error(y_test_online, y_pred_online))
        lists1[1].append(mean_squared_error(y_test_online, y_pred_online))
        lists1[2].append(r2_score(y_test_online, y_pred_online))
    # #print(lists)
    # plt.figure(figsize=(8, 5))
    # times = [1,2,3,4,5,6,7,8,9]
    # x = np.arange(20, 350)
    # l1 = plt.plot(times, lists1[0], 'r--', label='MAE')
    # l2 = plt.plot(times, lists1[1], 'g--', label='MSE')
    # l3 = plt.plot(times, lists1[2], 'b--', label='R Square')
    # plt.plot(times, lists1[0], 'ro-', times, lists1[1], 'g+-', times, lists1[2], 'b^-')
    # plt.title('The online learning of Logistic Regession ')
    # plt.xlabel('Times of online learning')
    # plt.ylabel('Value')
    # plt.legend()
    # #plt.show()
    #

    print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    print('------------------------------------------------------------------------------------')
    #info=psutil.virtual_memory()
    #print(u'used memory:',psutil.Process(os.getpid()).memory_info().rss)



    #gc.collect()
    y_pred, elapsed_time_train, elapsed_time_predict, clf1 = cFunc.LinearSVC(X_train_std, X_test_std, y_train)
    print(" 'y_pred': \n", y_pred)
    print('Test MAE of Linear SVM:', mean_absolute_error(y_test, y_pred))
    bytes_MAE_list.append(mean_absolute_error(y_test, y_pred))
    print('Test MSE of Linear SVM:', mean_squared_error(y_test, y_pred))
    bytes_MSE_list.append(mean_squared_error(y_test, y_pred))
    print('Test R square of Linear SVM:', r2_score(y_test, y_pred))
    bytes_R_square_list.append(r2_score(y_test, y_pred))
    clf_list.append(clf1)
    model_list.append('LinearSVC')

    print('Elapsed time (train): %s seconds' %elapsed_time_train)
    print('Elapsed time (predict): %s seconds' %elapsed_time_predict)
    elapsed_time_train_list.append(elapsed_time_train)
    elapsed_time_predict_list.append(elapsed_time_predict)
    # bytes_MAE_list.append(0.1)
    # bytes_MSE_list.append(0.1)
    # bytes_R_square_list.append(0.1)
    memory_fit.append(33.7)
    memory_predict.append(0)
    print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    lists2 = [[] for i in range(3)]
    for num in range(1,10):
        tstSize=num/10
        X_train_online, X_test_online, y_train_online, y_test_online = sFunc.trainTestSeparate(bitInfoArray, y_Num, tstSize, random_state=randomState)
        X_train_std_online, X_test_std_online = sFunc.dataStandardization(X_train_online, X_test_online)
        y_pred_online, elapsed_time_train, elapsed_time_predict, clf0 = cFunc.LinearSVC(X_train_std_online, X_test_std_online, y_train_online)
        print(" 'y_pred': \n", y_pred_online)
        print('online learning in %s time:' %num, 'MAE',mean_absolute_error(y_test_online, y_pred_online),
              'MSE',mean_squared_error(y_test_online, y_pred_online),'R square', r2_score(y_test_online, y_pred_online))
        lists2[0].append(mean_absolute_error(y_test_online, y_pred_online))
        lists2[1].append(mean_squared_error(y_test_online, y_pred_online))
        lists2[2].append(r2_score(y_test_online, y_pred_online))
    # # print(lists)
    # plt.figure(figsize=(8, 5))
    # times = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    # x = np.arange(20, 350)
    # l1 = plt.plot(times, lists2[0], 'r--', label='MAE')
    # l2 = plt.plot(times, lists2[1], 'g--', label='MSE')
    # l3 = plt.plot(times, lists2[2], 'b--', label='R Square')
    # plt.plot(times, lists2[0], 'ro-', times, lists2[1], 'g+-', times, lists2[2], 'b^-')
    # plt.title('The online learning of Linear SVM ')
    # plt.xlabel('Times of online learning')
    # plt.ylabel('Value')
    # plt.legend()
    # #plt.show()


    print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    print('------------------------------------------------------------------------------------')
    #info=psutil.virtual_memory()
    #print(u'used memory:',psutil.Process(os.getpid()).memory_info().rss)

    #gc.collect()
    y_pred, elapsed_time_train, elapsed_time_predict, clf2 = cFunc.SVC(X_train_std, X_test_std, y_train)
    print(" 'y_pred': \n", y_pred)
    print('Test MAE of Non-linear SVM:', mean_absolute_error(y_test, y_pred))
    bytes_MAE_list.append(mean_absolute_error(y_test, y_pred))
    print('Test MSE of Non-linear SVM:', mean_squared_error(y_test, y_pred))
    bytes_MSE_list.append(mean_squared_error(y_test, y_pred))
    print('Test R square of Non-linear SVM:', r2_score(y_test, y_pred))
    bytes_R_square_list.append(r2_score(y_test, y_pred))
    clf_list.append(clf2)
    model_list.append('SVC')
    print('Elapsed time (train): %s seconds' %elapsed_time_train)
    print('Elapsed time (predict): %s seconds' %elapsed_time_predict)
    elapsed_time_train_list.append(elapsed_time_train)
    elapsed_time_predict_list.append(elapsed_time_predict)

    memory_fit.append(23.34)
    memory_predict.append(0)
    print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    lists3 = [[] for i in range(3)]
    for num in range(1,10):
         tstSize=num/10
         X_train_online, X_test_online, y_train_online, y_test_online = sFunc.trainTestSeparate(bitInfoArray, y_Num, tstSize, random_state=randomState)
         X_train_std_online, X_test_std_online = sFunc.dataStandardization(X_train_online, X_test_online)
         y_pred_online, elapsed_time_train, elapsed_time_predict, clf0 = cFunc.SVC(X_train_std_online, X_test_std_online, y_train_online)
         print(" 'y_pred': \n", y_pred_online)
         print('online learning in %s time:' %num, 'MAE',mean_absolute_error(y_test_online, y_pred_online),
               'MSE',mean_squared_error(y_test_online, y_pred_online),'R square', r2_score(y_test_online, y_pred_online))
         lists3[0].append(mean_absolute_error(y_test_online, y_pred_online))
         lists3[1].append(mean_squared_error(y_test_online, y_pred_online))
         lists3[2].append(r2_score(y_test_online, y_pred_online))
    # #print(lists)
    # plt.figure(figsize=(8, 5))
    # times = [1,2,3,4,5,6,7,8,9]
    # x = np.arange(20, 350)
    # l1 = plt.plot(times, lists3[0], 'r--', label='MAE')
    # l2 = plt.plot(times, lists3[1], 'g--', label='MSE')
    # l3 = plt.plot(times, lists3[2], 'b--', label='R Square')
    # plt.plot(times, lists3[0], 'ro-', times, lists3[1], 'g+-', times, lists3[2], 'b^-')
    # plt.title('The online learning of SVM')
    # plt.xlabel('Times of online learning')
    # plt.ylabel('Value')
    # plt.legend()
    # #plt.show()
    print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    print('------------------------------------------------------------------------------------')
    #info=psutil.virtual_memory()
    #print(u'used memory:',psutil.Process(os.getpid()).memory_info().rss)

    #gc.collect()
    y_pred, elapsed_time_train, elapsed_time_predict, clf3 = cFunc.DecisionTreeClassifier(X_train_std, X_test_std, y_train)
    print(" 'y_pred': \n", y_pred)
    print('Test MAE of Decision Tree:', mean_absolute_error(y_test, y_pred))
    bytes_MAE_list.append(mean_absolute_error(y_test, y_pred))
    print('Test MSE of Decision Tree:', mean_squared_error(y_test, y_pred))
    bytes_MSE_list.append(mean_squared_error(y_test, y_pred))
    print('Test R square of Decision Tree:', r2_score(y_test, y_pred))
    bytes_R_square_list.append(r2_score(y_test, y_pred))
    clf_list.append(clf3)
    model_list.append('DecisionTreeClassifier')
    print('Elapsed time (train): %s seconds' %elapsed_time_train)
    print('Elapsed time (predict): %s seconds' %elapsed_time_predict)
    elapsed_time_train_list.append(elapsed_time_train)
    elapsed_time_predict_list.append(elapsed_time_predict)

    memory_fit.append(0.4)
    memory_predict.append(0)
    print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    lists4 = [[] for i in range(3)]
    for num in range(1,10):
         tstSize=num/10
         X_train_online, X_test_online, y_train_online, y_test_online = sFunc.trainTestSeparate(bitInfoArray, y_Num, tstSize, random_state=randomState)
         X_train_std_online, X_test_std_online = sFunc.dataStandardization(X_train_online, X_test_online)
         y_pred_online, elapsed_time_train, elapsed_time_predict, clf0 = cFunc.DecisionTreeClassifier(X_train_std_online, X_test_std_online, y_train_online)
         print(" 'y_pred': \n", y_pred_online)
         print('online learning in %s time:' %num, 'MAE',mean_absolute_error(y_test_online, y_pred_online),
               'MSE',mean_squared_error(y_test_online, y_pred_online),'R square', r2_score(y_test_online, y_pred_online))
         lists4[0].append(mean_absolute_error(y_test_online, y_pred_online))
         lists4[1].append(mean_squared_error(y_test_online, y_pred_online))
         lists4[2].append(r2_score(y_test_online, y_pred_online))
    # #print(lists)
    # plt.figure(figsize=(8, 5))
    # times = [1,2,3,4,5,6,7,8,9]
    # x = np.arange(20, 350)
    # l1 = plt.plot(times, lists4[0], 'r--', label='MAE')
    # l2 = plt.plot(times, lists4[1], 'g--', label='MSE')
    # l3 = plt.plot(times, lists4[2], 'b--', label='R Square')
    # plt.plot(times, lists4[0], 'ro-', times, lists4[1], 'g+-', times, lists4[2], 'b^-')
    # plt.title('The online learning of Decision Tree ')
    # plt.xlabel('Times of online learning')
    # plt.ylabel('Value')
    # plt.legend()
    # #plt.show()
    print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    print('------------------------------------------------------------------------------------')
    #info=psutil.virtual_memory()
    #print(u'used memory:',psutil.Process(os.getpid()).memory_info().rss)

    #gc.collect()
    y_pred, elapsed_time_train, elapsed_time_predict, clf4 = cFunc.RandomForestClassifier(X_train_std, X_test_std, y_train)
    print(" 'y_pred': \n", y_pred)
    print('Test MAE of Random Forest:', mean_absolute_error(y_test, y_pred))
    bytes_MAE_list.append(mean_absolute_error(y_test, y_pred))
    print('Test MSE of Random Forest:', mean_squared_error(y_test, y_pred))
    bytes_MSE_list.append(mean_squared_error(y_test, y_pred))
    print('Test R square of Random Forest:', r2_score(y_test, y_pred))
    bytes_R_square_list.append(r2_score(y_test, y_pred))
    clf_list.append(clf4)
    model_list.append('RandomForestClassifier')

    print('Elapsed time (train): %s seconds' %elapsed_time_train)
    print('Elapsed time (predict): %s seconds' %elapsed_time_predict)
    elapsed_time_train_list.append(elapsed_time_train)
    elapsed_time_predict_list.append(elapsed_time_predict)

    memory_fit.append(0.1)
    memory_predict.append(0)
    print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    lists5 = [[] for i in range(3)]
    for num in range(1,10):
         tstSize=num/10
         X_train_online, X_test_online, y_train_online, y_test_online = sFunc.trainTestSeparate(bitInfoArray, y_Num, tstSize, random_state=randomState)
         X_train_std_online, X_test_std_online = sFunc.dataStandardization(X_train_online, X_test_online)
         y_pred_online, elapsed_time_train, elapsed_time_predict, clf0 = cFunc.RandomForestClassifier(X_train_std_online, X_test_std_online, y_train_online)
         print(" 'y_pred': \n", y_pred_online)
         print('online learning in %s time:' %num, 'MAE',mean_absolute_error(y_test_online, y_pred_online),
               'MSE',mean_squared_error(y_test_online, y_pred_online),'R square', r2_score(y_test_online, y_pred_online))
         lists5[0].append(mean_absolute_error(y_test_online, y_pred_online))
         lists5[1].append(mean_squared_error(y_test_online, y_pred_online))
         lists5[2].append(r2_score(y_test_online, y_pred_online))
    # #print(lists)
    # plt.figure(figsize=(8, 5))
    # times = [1,2,3,4,5,6,7,8,9]
    # x = np.arange(20, 350)
    # l1 = plt.plot(times, lists5[0], 'r--', label='MAE')
    # l2 = plt.plot(times, lists5[1], 'g--', label='MSE')
    # l3 = plt.plot(times, lists5[2], 'b--', label='R Square')
    # plt.plot(times, lists5[0], 'ro-', times, lists5[1], 'g+-', times, lists5[2], 'b^-')
    # plt.title('The online learning of Random Forest ')
    # plt.xlabel('Times of online learning')
    # plt.ylabel('Value')
    # plt.legend()
    # #plt.show()
    print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    print('------------------------------------------------------------------------------------')
    #info=psutil.virtual_memory()
    #print(u'used memory:',psutil.Process(os.getpid()).memory_info().rss)

    #gc.collect()
    y_pred, elapsed_time_train, elapsed_time_predict, clf5 = cFunc.GaussianNB(X_train_std, X_test_std, y_train)
    print(" 'y_pred': \n", y_pred)
    print('Test MAE of Gaussian Naive Bayes:', mean_absolute_error(y_test, y_pred))
    bytes_MAE_list.append(mean_absolute_error(y_test, y_pred))
    print('Test MSE of Gaussian Naive Bayes:', mean_squared_error(y_test, y_pred))
    bytes_MSE_list.append(mean_squared_error(y_test, y_pred))
    print('Test R square of Gaussian Naive Bayes:', r2_score(y_test, y_pred))
    bytes_R_square_list.append(r2_score(y_test, y_pred))
    clf_list.append(clf5)
    model_list.append('GaussianNB')

    print('Elapsed time (train): %s seconds' %elapsed_time_train)
    print('Elapsed time (predict): %s seconds' %elapsed_time_predict)
    elapsed_time_train_list.append(elapsed_time_train)
    elapsed_time_predict_list.append(elapsed_time_predict)

    memory_fit.append(0)
    memory_predict.append(0)
    print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    lists6 = [[] for i in range(3)]
    for num in range(1,10):
         tstSize=num/10
         X_train_online, X_test_online, y_train_online, y_test_online = sFunc.trainTestSeparate(bitInfoArray, y_Num, tstSize, random_state=randomState)
         X_train_std_online, X_test_std_online = sFunc.dataStandardization(X_train_online, X_test_online)
         y_pred_online, elapsed_time_train, elapsed_time_predict, clf0 = cFunc.GaussianNB(X_train_std_online, X_test_std_online, y_train_online)
         print(" 'y_pred': \n", y_pred_online)
         print('online learning in %s time:' %num, 'MAE',mean_absolute_error(y_test_online, y_pred_online),
               'MSE',mean_squared_error(y_test_online, y_pred_online),'R square', r2_score(y_test_online, y_pred_online))
         lists6[0].append(mean_absolute_error(y_test_online, y_pred_online))
         lists6[1].append(mean_squared_error(y_test_online, y_pred_online))
         lists6[2].append(r2_score(y_test_online, y_pred_online))
    # #print(lists)
    # plt.figure(figsize=(8, 5))
    # times = [1,2,3,4,5,6,7,8,9]
    # x = np.arange(20, 350)
    # l1 = plt.plot(times, lists6[0], 'r--', label='MAE')
    # l2 = plt.plot(times, lists6[1], 'g--', label='MSE')
    # l3 = plt.plot(times, lists6[2], 'b--', label='R Square')
    # plt.plot(times, lists6[0], 'ro-', times, lists6[1], 'g+-', times, lists6[2], 'b^-')
    # plt.title('The online learning of Gaussian Naive Bayes')
    # plt.xlabel('Times of online learning')
    # plt.ylabel('Value')
    # plt.legend()
    # #plt.show()
    print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    print('------------------------------------------------------------------------------------')
    #info=psutil.virtual_memory()
    #print(u'used memory:',psutil.Process(os.getpid()).memory_info().rss)

    #gc.collect()
    y_pred, elapsed_time_train, elapsed_time_predict, clf6 = cFunc.KNeighborsClassifier(X_train_std, X_test_std, y_train)
    print(" 'y_pred': \n", y_pred)
    print('Test MAE of K Nearest Neighbors Classifier:', mean_absolute_error(y_test, y_pred))
    bytes_MAE_list.append(mean_absolute_error(y_test, y_pred))
    print('Test MSE of K Nearest Neighbors Classifier:', mean_squared_error(y_test, y_pred))
    bytes_MSE_list.append(mean_squared_error(y_test, y_pred))
    print('Test R square of K Nearest Neighbors Classifier:', r2_score(y_test, y_pred))
    bytes_R_square_list.append(r2_score(y_test, y_pred))
    clf_list.append(clf6)
    model_list.append('KNeighborsClassifier')

    print('Elapsed time (train): %s seconds' %elapsed_time_train)
    print('Elapsed time (predict): %s seconds' %elapsed_time_predict)
    elapsed_time_train_list.append(elapsed_time_train)
    elapsed_time_predict_list.append(elapsed_time_predict)

    memory_fit.append(0.4)
    memory_predict.append(0)
    print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    lists7 = [[] for i in range(3)]
    for num in range(1,10):
         tstSize=num/10
         X_train_online, X_test_online, y_train_online, y_test_online = sFunc.trainTestSeparate(bitInfoArray, y_Num, tstSize, random_state=randomState)
         X_train_std_online, X_test_std_online = sFunc.dataStandardization(X_train_online, X_test_online)
         y_pred_online, elapsed_time, clf0 = cFunc.KNeighborsClassifier(X_train_std_online, X_test_std_online, y_train_online)
         print(" 'y_pred': \n", y_pred_online)
         print('online learning in %s time:' %num, 'MAE',mean_absolute_error(y_test_online, y_pred_online),
               'MSE',mean_squared_error(y_test_online, y_pred_online),'R square', r2_score(y_test_online, y_pred_online))
         lists7[0].append(mean_absolute_error(y_test_online, y_pred_online))
         lists7[1].append(mean_squared_error(y_test_online, y_pred_online))
         lists7[2].append(r2_score(y_test_online, y_pred_online))
    # #print(lists)
    # plt.figure(figsize=(8, 5))
    # times = [1,2,3,4,5,6,7,8,9]
    # x = np.arange(20, 350)
    # l1 = plt.plot(times, lists7[0], 'r--', label='MAE')
    # l2 = plt.plot(times, lists7[1], 'g--', label='MSE')
    # l3 = plt.plot(times, lists7[2], 'b--', label='R Square')
    # plt.plot(times, lists7[0], 'ro-', times, lists7[1], 'g+-', times, lists7[2], 'b^-')
    # plt.title('The online learning of K Nearest Neighbors Classifier ')
    # plt.xlabel('Times of online learning')
    # plt.ylabel('Value')
    # plt.legend()
    # #plt.show()
    print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    print('------------------------------------------------------------------------------------')
    plt.figure(figsize=(20, 16))
    matplotlib.rcParams['axes.unicode_minus'] = False

    label_list = ['Logistic Regession', 'Linear SVM', 'Non-linear SVM', 'Decision Tree','Random Forest','Gaussian Naive Bayes','K Nearest Neighbors Classifier']


    rects1 = plt.bar(np.arange(len(bytes_MAE_list)), height=bytes_MAE_list, width=0.1, alpha=0.8, color='red', label="MAE")
    rects2 = plt.bar([i + 0.1 for i in np.arange(len(bytes_MAE_list))], height=bytes_MSE_list, width=0.1, color='green', label="MSE")
    rects3 = plt.bar([i + 0.2 for i in np.arange(len(bytes_MAE_list))], height=bytes_R_square_list, width=0.1, color='blue', label="$R^2$")
#    rects4 = plt.bar(left=[i + 1.2 for i in x], height=elapsed_time_list, width=0.4, color='yellow', label="elapsed time")
    plt.ylim(-0.5, 1)
    plt.ylabel("MAE/MSE/$R^2$")

    plt.xticks([index + 0.1 for index in np.arange(len(bytes_MAE_list))], label_list)
    #plt.xticks([index + 0.1 for index in np.arange(len(bytes_MAE_list))])
    plt.xlabel("Various machine learning algorithms ")
    #plt.title("Comparison of various machine learning algorithms (Non-online learning)")
    plt.grid(linestyle='-.')
    plt.legend(loc=[1, 0])
    # # redact the text
    # for rect in rects1:
    #     height = rect.get_height()
    #     plt.text(rect.get_x() + rect.get_width() / 2, height + 1, str(height), ha="center", va="bottom")
    # for rect in rects2:
    #     height = rect.get_height()
    #     plt.text(rect.get_x() + rect.get_width() / 2, height + 1, str(height), ha="center", va="bottom")
    #
    # for rect in rects3:
    #     height = rect.get_height()
    #     plt.text(rect.get_x() + rect.get_width() / 2, height + 1, str(height), ha="center", va="bottom")
    # # for rect in rects4:
    # #     height = rect.get_height()
    # #     plt.text(rect.get_x() + rect.get_width() / 2, height + 1, str(height), ha="center", va="bottom")
#####################################################################################################################################
    online_MAE_list = [[] for i in range(7)]
    online_MSE_list = [[] for i in range(7)]
    online_R_Squared_list = [[] for i in range(7)]
    online_MAE_list[0]=lists1[0]
    online_MAE_list[1]=lists2[0]
    online_MAE_list[2]=lists3[0]
    online_MAE_list[3]=lists4[0]
    online_MAE_list[4]=lists5[0]
    online_MAE_list[5]=lists6[0]
    online_MAE_list[6]=lists7[0]

    online_MSE_list[0] = lists1[1]
    online_MSE_list[1] = lists2[1]
    online_MSE_list[2] = lists3[1]
    online_MSE_list[3] = lists4[1]
    online_MSE_list[4] = lists5[1]
    online_MSE_list[5] = lists6[1]
    online_MSE_list[6] = lists7[1]

    online_R_Squared_list[0] = lists1[2]
    online_R_Squared_list[1] = lists2[2]
    online_R_Squared_list[2] = lists3[2]
    online_R_Squared_list[3] = lists4[2]
    online_R_Squared_list[4] = lists5[2]
    online_R_Squared_list[5] = lists6[2]
    online_R_Squared_list[6] = lists7[2]

    plt.figure(figsize=(9, 5))
    times = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    x = np.arange(20, 350)
    l1 = plt.plot(times, online_MAE_list[0], 'r--', label='Logistic Regression')
    l2 = plt.plot(times, online_MAE_list[1], 'g--', label='Linear SVM')
    l3 = plt.plot(times, online_MAE_list[2], 'b--', label='Non-linear SVM')
    l4 = plt.plot(times, online_MAE_list[3], 'k--', label='Decision Tree')
    l5 = plt.plot(times, online_MAE_list[4], 'y--', label='Random Forest')
    l6 = plt.plot(times, online_MAE_list[5], 'c--', label='Gaussian Naive Bayes')
    l7 = plt.plot(times, online_MAE_list[6], 'm--', label='K Nearest Neighbors')
    plt.plot(times, online_MAE_list[0], 'ro-', times, online_MAE_list[1], 'g+-', times, online_MAE_list[2], 'b^-', times, online_MAE_list[3], 'k*-', times, online_MAE_list[4], 'y+-', times, online_MAE_list[5], 'c^-', times, online_MAE_list[6], 'm*-' )
    #plt.title('The MAE scores of various online ML algorithms.')
    #plt.xlabel('Various online ML algorithms')
    plt.ylabel('MAE')
    plt.grid(linestyle='-.')
    plt.legend(loc=[1, 0])

    plt.figure(figsize=(9, 5))
    times = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    #x = np.arange(20, 350)
    l1 = plt.plot(times, online_MSE_list[0], 'r--', label='Logistic Regression')
    l2 = plt.plot(times, online_MSE_list[1], 'g--', label='Linear SVM')
    l3 = plt.plot(times, online_MSE_list[2], 'b--', label='Non-linear SVM')
    l4 = plt.plot(times, online_MSE_list[3], 'k--', label='Decision Tree')
    l5 = plt.plot(times, online_MSE_list[4], 'y--', label='Random Forest')
    l6 = plt.plot(times, online_MSE_list[5], 'c--', label='Gaussian Naive Bayes')
    l7 = plt.plot(times, online_MSE_list[6], 'm--', label='K Nearest Neighbors')
    plt.plot(times, online_MSE_list[0], 'ro-', times, online_MSE_list[1], 'g+-', times, online_MSE_list[2], 'b^-', times,
         online_MSE_list[3], 'k*-', times, online_MSE_list[4], 'y+-', times, online_MSE_list[5], 'c^-', times,
         online_MSE_list[6], 'm*-')
    #plt.title('The MSE scores of various online ML algorithms.')
    #plt.xlabel('Various online ML algorithms')
    plt.ylabel('MSE')
    plt.grid(linestyle='-.')
    plt.legend(loc=[1, 0])



    plt.figure(figsize=(9, 5))
    times = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    #x = np.arange(20, 350)
    l1 = plt.plot(times, online_R_Squared_list[0], 'r--', label='Logistic Regression')
    l2 = plt.plot(times, online_R_Squared_list[1], 'g--', label='Linear SVM')
    l3 = plt.plot(times, online_R_Squared_list[2], 'b--', label='Non-linear SVM')
    l4 = plt.plot(times, online_R_Squared_list[3], 'k--', label='Decision Tree')
    l5 = plt.plot(times, online_R_Squared_list[4], 'y--', label='Random Forest')
    l6 = plt.plot(times, online_R_Squared_list[5], 'c--', label='Gaussian Naive Bayes')
    l7 = plt.plot(times, online_R_Squared_list[6], 'm--', label='K Nearest Neighbors ')
    plt.plot(times, online_R_Squared_list[0], 'ro-', times, online_R_Squared_list[1], 'g+-', times, online_R_Squared_list[2], 'b^-', times,
         online_R_Squared_list[3], 'k*-', times, online_R_Squared_list[4], 'y+-', times, online_R_Squared_list[5], 'c^-', times,
         online_R_Squared_list[6], 'm*-')
    #plt.title('The R Squared scores of various online ML algorithms.')
    #plt.xlabel('Various online ML algorithms')
    plt.ylabel('$R^2$')
    plt.grid(linestyle='-.')
    plt.legend(loc=[1, 0])
    ##############################################################################################################################################################
    plt.figure(figsize=(10, 8))
    plt.bar(np.arange(len(bytes_MAE_list)), memory_fit, color='green', label='$memory_{fit}$')
    plt.bar(np.arange(len(bytes_MAE_list)), memory_predict, bottom=memory_fit, color='red', label='$memory_{predict}$')
    plt.legend(loc=[1, 0])
    plt.ylabel("Memory footprint/MiB")
    plt.xticks([index + 0.1 for index in np.arange(len(bytes_MAE_list))], label_list)
    plt.xlabel("Various machine learning algorithms ")
    #plt.title("Memory footprint of various machine learning algorithms (Non-online learning)")

    plt.figure(figsize=(10, 8))
    #plt.bar(np.arange(len(bytes_MAE_list)), height=elapsed_time_list, width=0.8, alpha=0.8, color='red',label="MAE")

    plt.bar(np.arange(len(bytes_MAE_list)), elapsed_time_train_list, color='green', label='$Elapsed\_time_{train}$')
    plt.bar(np.arange(len(bytes_MAE_list)), elapsed_time_predict_list, bottom=elapsed_time_train_list, color='red',label='$Elapsed\_time_{predict}$')
    plt.legend(loc=[1, 0])
    plt.ylabel("Elapsed time/s")

    plt.xticks([j for j in np.arange(len(bytes_MAE_list))], label_list)
    # plt.xticks([index + 0.1 for index in np.arange(len(bytes_MAE_list))])
    plt.xlabel("Various machine learning algorithms ")
    #plt.title("Elapsed time of various machine learning algorithms (Non-online learning)")
    plt.grid(linestyle='-.')

    ##############################################################################################################################################################
    plt.show()

    print('#################################################################################################')
    print('#################################################################################################')


