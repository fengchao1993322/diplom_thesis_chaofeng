from o2sc import pcap

import sys

import sklearn.preprocessing
import sklearn.linear_model
import sklearn.multiclass
import sklearn.metrics
import numpy as np

class BasicModel(pcap.PcapProcessingOracle):

    __classes       = None
    __model         = None
    __classifier    = None

    x_train = None
    x_test  = None
    y_train = None
    y_test  = None

    def get_class_count(self):
        return len(self.__classes)

    def get_class_one(self):
        return self.__classes[0]

    def __init__(self, model=None):
        if model == None:
            self.__model = sklearn.linear_model.LogisticRegression(
                            C=1.0,
                            class_weight='balanced',
                            solver='lbfgs',
                            multi_class='ovr')
        else:
            self.__model = model

    def train(self, x_raw, oversample=True, split=True):
        # get independent packets x and dependent classes y_prime
        y_prime_raw = self.transform_to_y_prime(x_raw)
        x = None
        y_prime = None
        if oversample == True:
            try:
                x, y_prime = self.oversample(x_raw, y_prime_raw)
            except:
                raise Exception("Oversampling failed!")
        else:
            x = x_raw
            y_prime = y_prime_raw
        y, self.__classes = self.transform_to_y(x, y_prime)
        if len(self.__classes) > 1:
            if split is True:
                self.x_train, self.x_test, self.y_train, self.y_test = self.split(x, y, 0.5)
            else:
                #self.x_train, self.x_test, self.y_train, self.y_test = self.split(x, y, 0.0)
                self.x_train = x 
                self.y_train = y
            if len(self.__classes) > 2:
                self.__classifier = sklearn.multiclass.OneVsRestClassifier(
                    self.__model, len(self.__classes)).fit(self.x_train, self.y_train)
            else:
                self.__classifier = self.__model.fit(self.x_train, self.y_train)

    def score(self):
        try:
            if len(self.__classes) > 1:
                print("F1 = " + str(
                    sklearn.metrics.f1_score(self.y_test,
                    self.__classifier.predict(self.x_test))))
            else:
                print("Model not scoreable!")
        except:
            raise Exception("Model not trained!")

    def predict(self, packet):
        #try:
        if len(self.__classes) == 1:
            return self.__classes[0]
        else:
            c = 0
            try:
                c = self.__classifier.predict(np.array([packet]))
            except:
                raise Exception("Class fitting failed!")
            try:
                return self.__classes[int(c)]
            except:
                raise Exception("No class " + str(c))
        #except:
            #raise Exception("Model not trained!")
