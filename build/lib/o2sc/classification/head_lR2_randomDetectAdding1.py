'''
    data format same as the input of neural network 
'''
import sys
import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn import svm

import summaryFunc as sFunc


loadingPath = "/home/student/Documents/aa_scripter/test3.pcap"

pktsDataFloat = sFunc.readPcap(loadingPath)

'''
5 features of one byte:
    feature 0: byte position;
    feature 1: which package byte belongs;
    feature 2: byte value;
    feature 3: derivation that this byte substracts its previous one in position parallelly;
    feature 4: derivation that the next byte substracts the current one in position parallelly;
'''
featuresOfOneByte = 7
byteInfoArray=np.zeros(shape=(len(pktsDataFloat)*pktsDataFloat.shape[1], featuresOfOneByte))

for rowIndexOfPktsDataframe in range(len(pktsDataFloat)):
    for columnIndexOfPktsDataframe in range(pktsDataFloat.shape[1]):
        if rowIndexOfPktsDataframe == 0: 
            if columnIndexOfPktsDataframe ==0:
                byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, rowIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
            elif columnIndexOfPktsDataframe ==pktsDataFloat.shape[1]-1:
                byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, rowIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], 0 ,pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], 0]
            else:
                byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, rowIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], 0 ,pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
        elif rowIndexOfPktsDataframe == len(pktsDataFloat)-1:
            if columnIndexOfPktsDataframe ==0:
                byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, rowIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], 0, 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
            elif columnIndexOfPktsDataframe == pktsDataFloat.shape[1]-1:
                byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, rowIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], 0]
            else:
                byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, rowIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
        else:
            if columnIndexOfPktsDataframe ==0:
                byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, rowIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], 0, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
            elif columnIndexOfPktsDataframe == pktsDataFloat.shape[1]-1:
                byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, rowIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], 0]
            else:
                byteInfoArray[rowIndexOfPktsDataframe*pktsDataFloat.shape[1]+columnIndexOfPktsDataframe] = [columnIndexOfPktsDataframe, rowIndexOfPktsDataframe, pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe-1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe+1,columnIndexOfPktsDataframe], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe-1], pktsDataFloat.iat[rowIndexOfPktsDataframe,columnIndexOfPktsDataframe+1]]
print(byteInfoArray)

resultPkts2dArray = sFunc.byteLabelPreOperation(pktsDataFloat)

###################################################################################
testSize = 0.1
X_train, X_test, y_train, y_test = sFunc.trainTestSeparate(byteInfoArray, resultPkts2dArray.ravel(), testSize)
#print("X_train: ", X_train)
X_train_std, X_test_std = sFunc.dataStandardization(X_train, X_test)

lr = LogisticRegression(C=100000.0, random_state=1, class_weight= 'balanced', solver='liblinear', multi_class= 'auto') 
lr.fit(X_train_std, y_train) 

print('Test accuracy:', lr.score(X_test_std, y_test))

#print(lr.classes_)

# for i in range(len(X_test)):
#     print(lr.predict(X_test_std)[i], "true label: ", y_test[i])

###################################################################################
#########################dynamical quantity of train set###########################
###################################################################################
# lr = LogisticRegression(C=1000.0, random_state=0, solver='liblinear', multi_class= 'auto') 
# for quantityOfTrain in range(10, len(pktsDataFloat)+1, 10):
#     lr.fit(pktsDataframe_std[0:quantityOfTrain], pktsDataFloat[0:quantityOfTrain].index) 

#     '''
#     check sample's probality, 
#                     ['02000400300400000000' '02000400302400000000' '20020400222422202222']
#     example index:     194                   3                       1
#     '''
#     if quantityOfTrain>=len(pktsDataFloat):
#         print("Train set has total %d packages, check the last package %d's probality in total data: "%(quantityOfTrain,quantityOfTrain))
#         print(lr.classes_)
#         print(lr.predict_proba(pktsDataframe_std)[quantityOfTrain-1,:]) 
#         continue
#     print("Train set has total %d packages, predict the next package %d's probality after train set: "%(quantityOfTrain,quantityOfTrain))
#     print(lr.classes_)
#     print(lr.predict_proba(pktsDataframe_std)[quantityOfTrain,:]) 
