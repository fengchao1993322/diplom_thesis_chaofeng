import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="o2sc",
    version="0.68",
    author="Máté Tömösközi",
    author_email="mate.tomoskozi@tu-dresden.de",
    description="Oracele-structured Stream Compression",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: Private Use",
        "Operating System :: Ubuntu",
    ],
)