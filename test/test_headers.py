from o2sc.classification import model
from test import tester

import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import scapy.all as scapy

class TestEasyRtp():
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        packets_train = scapy.rdpcap('pcaps/headers/easyIp_noPayl_train.pcap')
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_gain_ge_93(self):
        packets_test = scapy.rdpcap('pcaps/headers/easyIp_noPayl_test.pcap')
        g = self.tester.get_gain(packets_test, is_array=True)
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.93

class TestEasyRtpIPv6():
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        packets_train = scapy.rdpcap('pcaps/headers/easyIpv6_noPayl_train.pcap')
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_gain_ge_93(self):
        packets_test = scapy.rdpcap('pcaps/headers/easyIpv6_noPayl_test.pcap')
        g = self.tester.get_gain(packets_test, is_array=True)
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.97

class TestEasyUDP():
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        packets_train = scapy.rdpcap('pcaps/headers/easyUdp_noPayl_train.pcap')
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_gain_ge_93(self):
        packets_test = scapy.rdpcap('pcaps/headers/easyUdp_noPayl_test.pcap')
        g = self.tester.get_gain(packets_test, is_array=True)
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.95

class TestEasyUDPv6():
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        packets_train = scapy.rdpcap('pcaps/headers/easyUdpv6_noPayl_train.pcap')
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_gain_ge_93(self):
        packets_test = scapy.rdpcap('pcaps/headers/easyUdpv6_noPayl_test.pcap')
        g = self.tester.get_gain(packets_test, is_array=True)
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.97

class TestEasyRTP():
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        packets_train = scapy.rdpcap('pcaps/headers/easyRtpNoCsrc_noPayl_train.pcap')
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_gain_ge_93(self):
        packets_test = scapy.rdpcap('pcaps/headers/easyRtpNoCsrc_noPayl_test.pcap')
        g = self.tester.get_gain(packets_test, is_array=True)
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.86

class TestEasyRTPv6():
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        packets_train = scapy.rdpcap('pcaps/headers/easyRtpv6NoCsrc_noPayl_train.pcap')
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_gain_ge_93(self):
        packets_test = scapy.rdpcap('pcaps/headers/easyRtpv6NoCsrc_noPayl_test.pcap')
        g = self.tester.get_gain(packets_test, is_array=True)
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.90

class TestHardRTP():
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        packets_train = scapy.rdpcap('pcaps/headers/hardRtpNoCsrc_noPayl_randTs_train.pcap')
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_gain_ge_93(self):
        packets_test = scapy.rdpcap('pcaps/headers/hardRtpNoCsrc_noPayl_randTs_test.pcap')
        g = self.tester.get_gain(packets_test, is_array=True)
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.83

class TestHardRTPv6():
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        packets_train = scapy.rdpcap('pcaps/headers/hardRtpv6NoCsrc_noPayl_randTs_train.pcap')
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_gain_ge_93(self):
        packets_test = scapy.rdpcap('pcaps/headers/hardRtpv6NoCsrc_noPayl_randTs_test.pcap')
        g = self.tester.get_gain(packets_test, is_array=True)
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.89

def setup_module(module):
    # figure size
    plt.rcParams['figure.figsize'] = (15.0, 8.0)
    font = {#'family' : 'latex',
            #'weight' : 'bold',
            'size'   : 20}
    #plt.rcParams['text.usetex'] = True
    plt.rc('font', **font)
"""
def teardown_module(module):
    plot_gain()
    plot_size()

def mean_confidence_interval(data, confidence=0.95):
    a = 1.0 * np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)
    return m, h, h
"""
gains = []
"""
def plot_gain():
    fig, ax = plt.subplots()

    plt.ylim([-0,1])

    #plt.xlabel(r"Compression Gain")
    plt.ylabel(r"Gain")
    plt.grid()

    gs = [gains[0][0][0], gains[1][0][0], gains[2][0][0], gains[3][0][0], gains[4][0][0], gains[5][0][0], gains[6][0][0]]

    xAxis = [ 0, 1, 2, 3, 4, 5, 6 ]
    plt.xticks(xAxis, (r'Easy RTP', r'Hard RTP', r'Franka', r'Asterisk', r'Radio', r'VLC', r'Ekiga'))

    plt.bar(xAxis, gs, color='red')

    plt.savefig('plots/o2sc_gains.eps', bbox_inches='tight')
    plt.savefig('plots/o2sc_gains.pdf', bbox_inches='tight')
    plt.savefig('plots/o2sc_gains.png', bbox_inches='tight')

def plot_size():
    fig, ax = plt.subplots()

    plt.grid()

    #plt.xlabel(r"Compression Gain")
    plt.ylabel(r"Packet Size (byte)")

    bar_width = 0.35
    plt.rcParams['hatch.linewidth'] = 2.0

    #plt.xticks(xAxis + bar_width*1.5, ip_labels)

    co = [gains[0][0][1], gains[1][0][1], gains[2][0][1], gains[3][0][1], gains[4][0][1], gains[5][0][1], gains[6][0][1]]
    co_co = [mean_confidence_interval(gains[0][2])[1], mean_confidence_interval(gains[1][2])[1],
        mean_confidence_interval(gains[2][2])[1], mean_confidence_interval(gains[3][2])[1],
        mean_confidence_interval(gains[4][2])[1], mean_confidence_interval(gains[5][2])[1],
        mean_confidence_interval(gains[6][2])[1]]
    #[mean_confidence_interval(gain_franka[2])[1], mean_confidence_interval(gain_asterisk[2])[1], mean_confidence_interval(gain_radio[2])[1], mean_confidence_interval(gain_vlc[2])[1], mean_confidence_interval(gain_Ekiga[2])[1]]
    uc = [gains[0][0][2], gains[1][0][2], gains[2][0][2], gains[3][0][2], gains[4][0][2], gains[5][0][2], gains[6][0][2]]
    uc_co = [mean_confidence_interval(gains[0][1])[2], mean_confidence_interval(gains[1][1])[2],
        mean_confidence_interval(gains[2][1])[2], mean_confidence_interval(gains[3][1])[2],
        mean_confidence_interval(gains[4][1])[2], mean_confidence_interval(gains[5][1])[2],
        mean_confidence_interval(gains[6][1])[2]]
    #[mean_confidence_interval(gain_franka[1])[1], mean_confidence_interval(gain_asterisk[1])[1], mean_confidence_interval(gain_radio[1])[1], mean_confidence_interval(gain_vlc[1])[1], mean_confidence_interval(gain_Ekiga[1])[1]]
    mean_confidence_interval
    xAxis = np.arange(len(co))

    plt.xticks(xAxis + bar_width*0.5, (r'Easy RTP', r'Hard RTP', r'Franka', r'Asterisk', r'Radio', r'VLC', r'Ekiga'))

    plt.bar(xAxis, uc, bar_width, color='red', label='Uncompressed', yerr=uc_co)
    plt.bar(xAxis + bar_width, co, bar_width, color='green', hatch='o', label='Compressed', yerr=co_co)

    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.10), frameon=False,
            fancybox=False, shadow=False, ncol=2, markerscale=1, numpoints=1)

    plt.savefig('plots/o2sc_bytes.eps', bbox_inches='tight')
    plt.savefig('plots/o2sc_bytes.pdf', bbox_inches='tight')
    plt.savefig('plots/o2sc_bytes.png', bbox_inches='tight')
"""