from o2sc import compressor
from o2sc import decompressor
from o2sc.classification import model

import numpy as np
import pandas as pd

class Tester:
    oracle  = None
    comp    = None
    decomp  = None
    debug   = False

    def __init__(self, oracle=None, debug=False, static_compression=False):
        if oracle == None:
            raise Exception("Train the model first!")
        self.oracle = oracle
        self.comp   = compressor.Compressor(static_compression=static_compression)
        self.decomp = decompressor.Decompressor()

        self.comp.debug_info_on     = debug
        self.decomp.debug_info_on   = debug

    def get_gain(self, packets, is_array=False):
        #print(type(packets))
        #if packets == None:
        #    raise Exception("No packets received for compression!")

        uc_packet_lens = []
        co_packet_lens = []
        pattern_changes    = 0
        pattern_violations = 0

        #pktsDataframe = pd.DataFrame(packets)
        #resultPkts2dArray = np.empty([len(pktsDataframe), pktsDataframe.shape[1]])


        for i in range(0, len(packets)):
            #if self.debug:
            #    print(i)
            #print(packets[i])
            uc_pkt = None
            if not is_array:
                uc_pkt = list(packets.T[i])
            else:
                uc_pkt = list(bytearray(bytes(packets[i])))
            if self.debug:
                print("Uncompressed: " + str(uc_pkt))
            pattern = self.oracle.predict(uc_pkt)
            #print(pattern)
            co_pkt = self.comp.compress(uc_pkt, pattern)
            pattern_changes += 1 if self.comp.is_pattern_changed else 0
            pattern_violations += 1 if self.comp.is_pattern_violated else 0
            uc_packet_lens.append(len(uc_pkt))
            co_packet_lens.append(len(co_pkt))
            if self.debug:
                print("Compressed: " + str(co_pkt))
            de_pkt = self.decomp.decompress(co_pkt)
            if self.debug:
                print("Decompressed: " + str(de_pkt))
            bytes_different = 0
            for i in range(len(de_pkt)):
                if uc_pkt[i] != de_pkt[i]:
                    print(str(i) + " = " + str(uc_pkt[i]) + " vs. " + str(de_pkt[i]))
                    bytes_different += 1

            if bytes_different > 0:
                print("--------------------------------------------------------")
                #print(uc_pkt)
                #print(de_pkt)
                self.comp.print_contexts()
                print("Difference " + str(bytes_different))
                break

        return [self.comp.get_gain(), uc_packet_lens, co_packet_lens, pattern_changes, pattern_violations]
