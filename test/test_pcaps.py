from o2sc.classification import model
from test import tester

import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import scapy.all as scapy


class TestEasyRtp():
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        packets_train = scapy.rdpcap('pcaps/streams/easyRtp_train_100.pcap')
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_gain_ge_93(self):
        packets_test = scapy.rdpcap('pcaps/streams/easyRtp_test_100_1000.pcap')
        g = self.tester.get_gain(packets_test, is_array=True)
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.93

class TestHardRtp():
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        packets_train = scapy.rdpcap('pcaps/streams/hardRtpNoCsrcFixedPayl_train_100.pcap')
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_gain_ge_90(self):
        pkts_test = scapy.rdpcap('pcaps/streams/hardRtpNoCsrcFixedPayl_test_100_1000.pcap')
        g = self.tester.get_gain(pkts_test, is_array=True)
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.9

class TestFranka:
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        packets_train = scapy.rdpcap('pcaps/streams/franka_train_50.pcap')
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_gain_ge_34(self):
        pkts_test = scapy.rdpcap('pcaps/streams/franka_test_100_1100.pcap')
        g = self.tester.get_gain(pkts_test, is_array=True)
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        print("Uc Size = " + str(g[0][2]))
        print("Co Size = " + str(g[0][1]))
        assert g[0][0] >= 0.34

class TestAsterisk():
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        packets_train = scapy.rdpcap('pcaps/streams/asterisk_rtp_downstream_240_train_100.pcap')
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_gain_ge_48(self):
        pkts_test = scapy.rdpcap('pcaps/streams/asterisk_rtp_downstream_240_test_100_1000.pcap')
        g = self.tester.get_gain(pkts_test, is_array=True)
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        self.tester.oracle.score()
        assert g[0][0] >= 0.48

class TestRadioMiki():
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        packets_train = scapy.rdpcap('pcaps/streams/radio_miki_acks_66_no_options_0_ip_id_fix_wnd_sz_train_100.pcap')
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_gain_ge_84(self):
        pkts_test = scapy.rdpcap('pcaps/streams/radio_miki_acks_66_no_options_0_ip_id_fix_wnd_sz_test_100.pcap')
        g = self.tester.get_gain(self.oracle.convert_pcap_to_df(pkts_test))
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.84

class TestVlc():
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        packets_train = scapy.rdpcap('pcaps/streams/vlc_v6_mts_gsm_s16l_ts_rtp_2_fixed_pkt_size_train_99.pcap')
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_gain_ge_16(self):
        pkts_test = scapy.rdpcap('pcaps/streams/vlc_v6_mts_gsm_s16l_ts_rtp_2_fixed_pkt_size_test_100.pcap')
        g = self.tester.get_gain(pkts_test, is_array=True)
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.16

class TestEkiga():
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        packets_train = scapy.rdpcap('pcaps/streams/Ekiga_test_rtp_only_downstream_filtered_train_100.pcap')
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_gain_ge_11(self):
        pkts_test = scapy.rdpcap('pcaps/streams/Ekiga_test_rtp_only_downstream_filtered_test_100.pcap')
        g = self.tester.get_gain(pkts_test, is_array=True)
        gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.115

class TestCoAP():
    oracle = model.BasicModel()
    tester = None

    def setup(self):
        packets_train = scapy.rdpcap('pcaps/streams/coap_ack_train_10.pcap')
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_gain_ge_91(self):
        pkts_test = scapy.rdpcap('pcaps/streams/coap_ack_test_82.pcap')
        g = self.tester.get_gain(pkts_test, is_array=True)
        #gains.append(g)
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.91

def setup_module(module):
    # figure size
    plt.rcParams['figure.figsize'] = (15.0, 8.0)
    font = {'family' : 'latex', #
            'weight' : 'bold', #
            'size'   : 20}
    plt.rcParams['text.usetex'] = True #
    plt.rc('font', **font)

def teardown_module(module):
    plot_gain()
    plot_size()

def mean_confidence_interval(data, confidence=0.95):
    a = 1.0 * np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)
    return m, h, h

gains = []
def plot_gain():
    fig, ax = plt.subplots()

    plt.ylim([-0,1])

    #plt.xlabel(r"Compression Gain")
    plt.ylabel(r"Gain")
    plt.grid()
    ax.set_axisbelow(True)

    gs = [gains[0][0][0], gains[1][0][0], gains[2][0][0], gains[3][0][0],
        gains[4][0][0], gains[5][0][0], gains[6][0][0]]#, gains[7][0][0]]

    xAxis = [ 0, 1, 2, 3, 4, 5, 6 ]
    #, 7 ]
    plt.xticks(xAxis, (r'Easy RTP', r'Hard RTP', r'Franka', r'Asterisk',
        r'Radio', r'VLC', r'Ekiga'))
        #, r'CoAP'))

    plt.bar(xAxis, gs, color='crimson', hatch='/')
    #plt.bar(xAxis, gs, color='red')

    plt.savefig('plots/o2sc_gains.eps', bbox_inches='tight')
    plt.savefig('plots/o2sc_gains.pdf', bbox_inches='tight')
    plt.savefig('plots/o2sc_gains.png', bbox_inches='tight')

def plot_size():
    fig, ax = plt.subplots()

    plt.grid()
    ax.set_axisbelow(True)

    #plt.xlabel(r"Compression Gain")
    plt.ylabel(r"Packet Size (byte)")

    bar_width = 0.35
    plt.rcParams['hatch.linewidth'] = 2.0

    #plt.xticks(xAxis + bar_width*1.5, ip_labels)

    co = [gains[0][0][1], gains[1][0][1], gains[2][0][1], gains[3][0][1],
        gains[4][0][1], gains[5][0][1], gains[6][0][1]]#, gains[7][0][1]]
    co_co = [mean_confidence_interval(gains[0][2])[1], mean_confidence_interval(gains[1][2])[1],
        mean_confidence_interval(gains[2][2])[1], mean_confidence_interval(gains[3][2])[1],
        mean_confidence_interval(gains[4][2])[1], mean_confidence_interval(gains[5][2])[1],
        mean_confidence_interval(gains[6][2])[1]]#, mean_confidence_interval(gains[7][2])[1]]
    #[mean_confidence_interval(gain_franka[2])[1], mean_confidence_interval(gain_asterisk[2])[1], mean_confidence_interval(gain_radio[2])[1], mean_confidence_interval(gain_vlc[2])[1], mean_confidence_interval(gain_Ekiga[2])[1]]
    uc = [gains[0][0][2], gains[1][0][2], gains[2][0][2], gains[3][0][2],
        gains[4][0][2], gains[5][0][2], gains[6][0][2]]#, gains[7][0][2]]
    uc_co = [mean_confidence_interval(gains[0][1])[2], mean_confidence_interval(gains[1][1])[2],
        mean_confidence_interval(gains[2][1])[2], mean_confidence_interval(gains[3][1])[2],
        mean_confidence_interval(gains[4][1])[2], mean_confidence_interval(gains[5][1])[2],
        mean_confidence_interval(gains[6][1])[2]] #, mean_confidence_interval(gains[7][1])[2]]
    #[mean_confidence_interval(gain_franka[1])[1], mean_confidence_interval(gain_asterisk[1])[1], mean_confidence_interval(gain_radio[1])[1], mean_confidence_interval(gain_vlc[1])[1], mean_confidence_interval(gain_Ekiga[1])[1]]
    mean_confidence_interval
    xAxis = np.arange(len(co))

    plt.xticks(xAxis + bar_width*0.5, (r'Easy RTP', r'Hard RTP', r'Franka',
        r'Asterisk', r'Radio', r'VLC', r'Ekiga'))
        #, r'CoAP'))

    plt.bar(xAxis, uc, bar_width, color='crimson', hatch='/', label='Uncompressed', yerr=uc_co)
    plt.bar(xAxis + bar_width, co, bar_width, color='royalblue', hatch='o', label='Compressed', yerr=co_co)

    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.10), frameon=False,
            fancybox=False, shadow=False, ncol=2, markerscale=1, numpoints=1)

    plt.savefig('plots/o2sc_bytes.eps', bbox_inches='tight')
    plt.savefig('plots/o2sc_bytes.pdf', bbox_inches='tight')
    plt.savefig('plots/o2sc_bytes.png', bbox_inches='tight')
