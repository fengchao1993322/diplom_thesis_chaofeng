from o2sc import compressor
from o2sc import decompressor
from o2sc import oracle
from o2sc.classification import model

import pandas as pd
import scapy.all as scapy
import random

from test import tester

from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from keras.models import Sequential


pcap_train = 'pcaps/streams/hardRtpNoCsrcFixedPayl_train_100.pcap'
pcap_test  = 'pcaps/streams/hardRtpNoCsrcFixedPayl_test_100_1000.pcap'

packets_train = None
packets_test  = None

def setup_module(module):
    global packets_train
    global packets_test
    packets_train = scapy.rdpcap(pcap_train)
    packets_test  = scapy.rdpcap(pcap_test)

packets = [
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x00, 0xf9],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x00, 0xfa],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x00, 0xfb],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x00, 0xfc],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x00, 0xfd],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x00, 0xfe],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x00, 0xff],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x01],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x02],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x03],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x04],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x05],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x06],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x07],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x08],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x09],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x0a],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x0b],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x0c],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x0d],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x0e],
    [0xff, 0xff, random.randint(0x00, 0xff), 0x00, 0x01, 0x0f]]

class TestLogisticRegression():
    oracle = None
    tester = None

    def setup(self):
        clf = LogisticRegression(C=1.0,
                                 class_weight='balanced',
                                 solver='liblinear',
                                 multi_class='ovr')
        self.oracle = model.BasicModel(clf)
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle, static_compression=False)

    def test_1ogistic_regression(self):
        g = self.tester.get_gain(self.oracle.convert_pcap_to_df(packets_test))
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.0

    def test_packets_logistic_regression(self):
        self.oracle.train(self.oracle.convert_pcap_to_df(packets))
        g = self.tester.get_gain(self.oracle.convert_pcap_to_df(packets))
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.0

class TestLinearSVC():
    oracle = None
    tester = None

    def setup(self):
        clf = LinearSVC()
        self.oracle = model.BasicModel(clf)
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_1inear_svc(self):
        g = self.tester.get_gain(self.oracle.convert_pcap_to_df(packets_test))
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.0

    def test_packets_linear_svc(self):
        self.oracle.train(self.oracle.convert_pcap_to_df(packets))
        g = self.tester.get_gain(self.oracle.convert_pcap_to_df(packets))
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.0

class TestSVC():
    oracle = None
    tester = None

    def setup(self):
        clf = SVC()
        self.oracle = model.BasicModel(clf)
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_1vc(self):
        g = self.tester.get_gain(self.oracle.convert_pcap_to_df(packets_test))
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.0

    def test_packets_svc(self):
        self.oracle.train(self.oracle.convert_pcap_to_df(packets))
        g = self.tester.get_gain(self.oracle.convert_pcap_to_df(packets))
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.0

class TestDecisionTree():
    oracle = None
    tester = None

    def setup(self):
        clf = DecisionTreeClassifier()
        self.oracle = model.BasicModel(clf)
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_decision_tree(self):
        g = self.tester.get_gain(self.oracle.convert_pcap_to_df(packets_test))
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.0

    def test_packets_decision_tree(self):
        self.oracle.train(self.oracle.convert_pcap_to_df(packets))
        g = self.tester.get_gain(self.oracle.convert_pcap_to_df(packets))
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.0

class TestRandomForest():
    oracle = None
    tester = None

    def setup(self):
        clf = RandomForestClassifier()
        self.oracle = model.BasicModel(clf)
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_random_forest(self):
        g = self.tester.get_gain(self.oracle.convert_pcap_to_df(packets_test))
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.0

    def test_packets_random_forest(self):
        self.oracle.train(self.oracle.convert_pcap_to_df(packets))
        g = self.tester.get_gain(self.oracle.convert_pcap_to_df(packets))
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.0

class TestGaussianNaiveBayes():
    oracle = None
    tester = None

    def setup(self):
        clf = GaussianNB()
        self.oracle = model.BasicModel(clf)
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_1aussian_naive_bayes(self):
        g = self.tester.get_gain(self.oracle.convert_pcap_to_df(packets_test))
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.0

    def test_packets_gaussian_naive_bayes(self):
        self.oracle.train(self.oracle.convert_pcap_to_df(packets))
        g = self.tester.get_gain(self.oracle.convert_pcap_to_df(packets))
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.0

class TestKNearestNeighbours():
    oracle = None
    tester = None

    def setup(self):
        clf = KNeighborsClassifier()
        self.oracle = model.BasicModel(clf)
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_1_nearest_neighbours(self):
        g = self.tester.get_gain(self.oracle.convert_pcap_to_df(packets_test))
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.0

    def test_packets_k_nearest_neighbours(self):
        self.oracle.train(self.oracle.convert_pcap_to_df(packets))
        g = self.tester.get_gain(self.oracle.convert_pcap_to_df(packets))
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= -0.15

"""
class TestSequential():
    oracle = None
    tester = None

    def setup(self):
        clf = Sequential()
        clf.compile(loss='categorical_crossentropy',
                    optimizer='sgd',
                    metrics=['accuracy'])
        self.oracle = model.BasicModel(clf)
        self.oracle.train(self.oracle.convert_pcap_to_df(packets_train))
        self.tester = tester.Tester(self.oracle)

    def test_1equential(self):
        g = self.tester.get_gain(self.oracle.convert_pcap_to_df(packets_test))
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.0

    def test_packets_sequential(self):
        self.oracle.train(self.oracle.convert_pcap_to_df(packets))
        g = self.tester.get_gain(self.oracle.convert_pcap_to_df(packets))
        print("Gain = " + str(g[0][0]))
        assert g[0][0] >= 0.0
"""
