% all programs run in python 3.7


# To comparison the different metrics (elapsed time, MSE, R squared and so on) of ML algorithms in o2sc 

cd build/lib/o2sc/classification

python comparison_byte_chaofeng.py

python comparison_bit_chaofeng.py

python comparison_word_chaofeng.py

#To calculate the confidence interval

cd build/lib/o2sc/classification

python comparison_byte_Confidence_interval.py

python comparison_bit_Confidence_interval.py

python comparison_word_Confidence_interval.py

# To comparison the compression gains of various ML algorithms

cd example

python Comparison_compression_gain.py

#captured pcap

cd Pcap_chao