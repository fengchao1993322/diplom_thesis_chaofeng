#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""
About: Simple cluster with multiplenodes and one head

Topo:       n_k
          .  |   .
        .    |     .
      n_1---head---n_N
"""
"""
@ToDo: - fix error with @add_ovs_flows 
            (only last node may setup a socket)
            - Do we even need that?
        - more computing power for head
        - automatic return after head has finished
        - live output for nodes and head?
"""

import os
import time
import argparse
from threading import Thread
from shlex import split
from subprocess import check_output, Popen

from comnetsemu.cli import CLI
from comnetsemu.net import Containernet,VNFManager
from mininet.log import error, info, setLogLevel
from mininet.node import Controller
from mininet.link import TCLink
from mininet.term import makeTerm, makeTerms

HOST_SRC = "/home/comnetsemu/malte/simple-topt"


# Function definition
def get_ofport(ifce):
    """Get the openflow port based on iterface name

    :param ifce (str): Name of the interface
    """
    return check_output(
        split("ovs-vsctl get Interface {} ofport".format(ifce)
              )).decode("utf-8")


def add_ovs_flows(net, num_nodes):
    check_output(split("ovs-ofctl del-flows s1"))

    for m_node in range(num_nodes):
        in_port = get_ofport('s1-node_host%s' % (m_node + 1))
        out_port = get_ofport('s1-head_host')

        check_output(split('sudo ovs-ofctl add-flow s1 \'{proto}, in_port={in_port}, actions=output={out_port}\''.format(**{'in_port': in_port, 'out_port': out_port, 'proto': 'tcp'})))

        # Switch in_port and out_port
        check_output(split('sudo ovs-ofctl add-flow s1 \'{proto}, in_port={in_port}, actions=output={out_port}\''.format(**{'in_port': out_port, 'out_port': in_port, 'proto': 'tcp'})))


def disable_cksum_offload(ifces):
    """Disable RX/TX checksum offloading"""
    for ifce in ifces:
        check_output(
            split("sudo ethtool --offload %s rx off tx off" % ifce)
        )


# Topology definition
def removeContainers(mgr, nodes) :
    for m_node in nodes:
        mgr.removeContainer(m_node.name)


def printLog(nodes, num_nodes, log_conf) :
    #if log_conf.get('head', None):
    #    info('*** Head logs: \n')
    #    print(nodes[0].dins.logs(timestamps=False).decode('utf-8'))
    if log_conf.get('node', None):
        for m_node in range(1, num_nodes + 1):
            info('*** Node%s logs\n' % m_node)
            print(nodes[m_node].dins.logs(timestamps=False).decode('utf-8'))


def waitHead(head, log_conf) :
    m_endstr = '$FINISHED'
    while True:
        m_data = head.dins.logs(timestamps=False).decode('utf-8')
        #ret = m_data.find(chr(36))
        ret = m_data.find(m_endstr)
        if ret >= 0:
            info('*** Head has finished\n')
            break


def getLiveLog(node, stop) :
    if log_conf.get('head', None):
        m_old_log = ''
        while True:
            m_log = node.dins.logs(timestamps=False).decode('utf-8')
            if m_log != m_old_log:
                print(m_log.replace(m_old_log,''))
                m_old_log = m_log
            if stop():
                break


def startSensing(node, node_num) :
    if node_num == 0:
        print('Bringing head up')
        ret = node.cmd('python3 head-multiconn.py')
        print('Job done! \n')
        print(ret)
    else :
        print('Bringing node %s up' %node_num)
        node.cmd('python3 node.py %s' % node_num)
        print('Node %s has finished' %node_num)


def startSensingWOT(nodes, num_nodes) :
    info('Bringing head up\n')
    nodes[0].sendCmd('python3 head-multiconn.py')

    for m_node in range (1, num_nodes + 1):
        info('Bringing node %s up\n' %m_node)
        nodes[m_node].sendCmd('python3 node.py %s' %m_node)

    info('*** Waiting for head to finish\n')
    ret = nodes[0].waitOutput()
    print(ret)


def installNodes(mgr, hosts, num_nodes, node_conf) :
    info('*** Adding docker in docker\n')
    nodes= list()
    head = mgr.addContainer(name = 'head',
                            dhost = hosts[0].name,
                            dimage = 'cs_docker',
                            dcmd = 'python3.6 ./cs-head.py',
                            wait = 3)

    nodes.append(head)
    for m_node in range(1, num_nodes + 1):
        m_node_cmd = 'python3.6 ./cs-node.py {} {} {}'.format(m_node, node_conf['dict'], node_conf['dct'])
        node = mgr.addContainer(name = 'node%s' % (m_node),
                                dhost = hosts[m_node].name,
                                dimage = 'cs_docker',
                                dcmd = m_node_cmd,
                                wait = 3)
        nodes.append(node)

    return nodes


def getTopo(net, num_nodes) :
    try:
        info('*** Adding controller\n')
        net.addController('c0')

        info('*** Adding switch\n')
        s1 = net.addSwitch('s1')

        info('*** Adding dockerhost and links\n')
        hosts = list()
        head_host = net.addDockerHost('head_host',
                                        dimage = 'cs_docker',
                                        ip = '10.0.0.21',
                                        cpu_quota = 100000,
                                        cpuset_cpus = '0',
                                        volumes = ["/var/run/docker.sock:/var/run/docker.sock:rw"])
    
        # Head host has index 0 in @hosts
        hosts.append(head_host)
        net.addLinkNamedIfce(s1, head_host, bw = 10, delay = '1ms', loss = 0)

        for m_node in range(num_nodes):
            node_host = net.addDockerHost('node_host%s' % (m_node + 1),
                                        dimage = 'cs_docker',
                                        ip = '10.0.0.%s' % (m_node + 1),
                                        #cpuset_cpus = '0',
                                        volumes = ["/var/run/docker.sock:/var/run/docker.sock:rw"])

            hosts.append(node_host)
            net.addLinkNamedIfce(s1, node_host, bw = 10, delay = '1ms', loss = 0)
    
        return hosts

    except Exception as e:
        error(e)
        net.stop()


def runCompressedSensingTest(num_nodes, node_conf, log_conf) :
    net = Containernet(controller = Controller,
                        link = TCLink,
                        autoSetMacs = True,
                        autoStaticArp = True,
                        xterms = False)

    mgr = VNFManager(net)
    hosts = getTopo(net, num_nodes)

    try: 
        info('*** Starting network\n')
        net.start()
        net.pingAll()
        #add_ovs_flows(net, num_nodes)

        nodes = installNodes(mgr, hosts, num_nodes, node_conf)

        info('*** Wait for head to finish\n\n')
        waithead = Thread(target=waitHead, args=(nodes[0], log_conf,))
        waithead.start()
        stop_log = False
        livelog = Thread(target=getLiveLog,args=(nodes[0], lambda : stop_log,))
        livelog.start()
        waithead.join()
        time.sleep(1)   # Maybe there are still some messages in the pipe
        stop_log = True
        livelog.join()

        time.sleep(2)
        printLog(nodes, num_nodes, log_conf)

        info('*** Removing containers\n')
        removeContainers(mgr, nodes)

    except Exception as e:
        error('Emulation has errors: ')
        error(e)

    except KeyboardInterrupt :
        error('Aborted')
        removeContainers(mgr, head, nodes)
        net.stop()
        mgr.stop()

    finally:
        info('*** Stopping network\n')
        net.stop()
        mgr.stop()



# Main
if __name__ == '__main__':
    # Get number of nodes from command line
    parser = argparse.ArgumentParser()
    parser.add_argument('num_nodes', type = int,
                        help = 'Number of nodes in the cluster')
    parser.add_argument('--DC', default = False,
                        help = '1 = Compare MSE with own dictionary')
    parser.add_argument('--dct', default = False,
                        help = '1 = Run compression with dct as basis; decompression will adapt automatically')
    args = parser.parse_args()
    num_nodes = args.num_nodes

    doDC = ''
    doDCT = ''
    if args.DC:
        doDC = '--DC 1'
    if args.dct:
        doDCT = '--dct 1'

    node_conf = {'dict': doDC,
                 'dct': doDCT}
    print("CONF: ", node_conf)

    setLogLevel('info')
    log_conf = {"head": 1,
                "node": 1}

    runCompressedSensingTest(num_nodes, node_conf, log_conf)
