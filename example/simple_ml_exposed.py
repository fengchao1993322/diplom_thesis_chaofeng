from o2sc import compressor
from o2sc import decompressor
from o2sc.classification import model
from o2sc import pcap
from o2sc.classification import summaryFunc as sFunc

import scapy.all as scapy
import sklearn.linear_model


pcap_test = '/home/chao/Desktop/o2sc/pcaps/streams/asterisk_rtp_downstream_240_test_100_1000.pcap'
#pcap_test = "/home/chao/Desktop/o2sc/pcaps/headers/hardRtpv6NoCsrc_noPayl.pcap"


model = sklearn.linear_model.LogisticRegression(
        C=1.0, class_weight='balanced', solver='liblinear', multi_class='ovr')

pcapHelper = pcap.PcapProcessingOracle()
comp   = compressor.Compressor()
decomp = decompressor.Decompressor()

#pkts_train = scapy.rdpcap('/home/chao/Desktop/o2sc/pcaps/headers/hardRtpv6NoCsrc_noPayl.pcap')
pkts_train = scapy.rdpcap('/home/chao/Desktop/o2sc/pcaps/streams/asterisk_rtp_downstream_240_test_100_1000.pcap')

x_raw = pcapHelper.convert_pcap_to_df(pkts_train)

y_prime_raw = pcapHelper.transform_to_y_prime(x_raw)
#labels = sFunc.outputLabel(y_prime_raw.ravel())
#print('original labels: ', labels)
x, y_prime = pcapHelper.oversample(x_raw, y_prime_raw)
y, classes = pcapHelper.transform_to_y(x, y_prime)

x_train = x_test = y_train = y_test = None
# if len(classes) > 1:
#     if len(classes) > 2:
#         raise Exception("Multi-classification not implemented!")
#     x_train, x_test, y_train, y_test = pcapHelper.split(x, y, 0.5)
# else:
#     raise Exception("Only one class!")

#classifier = model.fit(x_train, y_train)
classifier = model.fit(x, y)


pkts_test = scapy.rdpcap(pcap_test)
for pktsIndex in range(0, len(pkts_test)):
    uc_pkt  = list(bytearray(bytes(pkts_test[pktsIndex])))
    pattern = classes[int(classifier.predict([uc_pkt]))]
    co_pkt  = comp.compress(uc_pkt, pattern)

print(comp.get_gain())
