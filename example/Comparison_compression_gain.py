from o2sc import compressor
from o2sc import decompressor
from o2sc.classification import model
from o2sc import pcap
from o2sc.classification import summaryFunc as sFunc

import scapy.all as scapy
import sklearn.linear_model
import numpy as np
import matplotlib.pyplot as plt


pcap_test = '/home/chao/Desktop/o2sc/pcaps/streams/asterisk_rtp_downstream_240_test_100_1000.pcap'
#pcap_test = '/home/chao/Desktop/DA/Pcap/youtube1.pcap'
#pcap_test = "/home/chao/Desktop/o2sc/pcaps/headers/hardRtpv6NoCsrc_noPayl.pcap"


#model = sklearn.linear_model.LogisticRegression(C=1.0, class_weight='balanced', solver='liblinear', multi_class='ovr')

mlModel0 = sFunc.classifierAndItsParameters(MlModel='LogisticRegression', lr_C=10.0, lr_class_weight= 'balanced', lr_solver='liblinear', lr_multi_class= 'auto' )
mlModel1 = sFunc.classifierAndItsParameters(MlModel='LinearSVC', lsvc_C=1.0, lsvc_max_iter=10000, lsvc_class_weight='balanced')
mlModel2 = sFunc.classifierAndItsParameters(MlModel='SVC', svc_kernel='rbf', svc_C=1.0, svc_gamma='scale', svc_class_weight='balanced')
mlModel3 = sFunc.classifierAndItsParameters(MlModel='DecisionTreeClassifier', dt_class_weight='balanced', dt_criterion='entropy')
mlModel4 = sFunc.classifierAndItsParameters(MlModel='RandomForestClassifier', rf_n_estimators=20, rf_class_weight='balanced')
mlModel5 = sFunc.classifierAndItsParameters(MlModel='GaussianNB')
mlModel6 = sFunc.classifierAndItsParameters(MlModel='KNeighborsClassifier', knn_algorithm='auto')





pcapHelper = pcap.PcapProcessingOracle()
comp   = compressor.Compressor()
decomp = decompressor.Decompressor()

#pkts_train = scapy.rdpcap('/home/chao/Desktop/o2sc/pcaps/headers/hardRtpv6NoCsrc_noPayl.pcap')
pkts_train = scapy.rdpcap('/home/chao/Desktop/o2sc/pcaps/streams/asterisk_rtp_downstream_240_test_100_1000.pcap')
#pkts_train = scapy.rdpcap('/home/chao/Desktop/DA/Pcap/youtube1.pcap')

x_raw = pcapHelper.convert_pcap_to_df(pkts_train)

y_prime_raw = pcapHelper.transform_to_y_prime(x_raw)
#labels = sFunc.outputLabel(y_prime_raw.ravel())
labels = sFunc.outputLabel(y_prime_raw)
print('original labels: ', labels)
x, y_prime = pcapHelper.oversample(x_raw, y_prime_raw)
y, classes = pcapHelper.transform_to_y(x, y_prime)

x_train = x_test = y_train = y_test = None
# if len(classes) > 1:
#     if len(classes) > 2:
#         raise Exception("Multi-classification not implemented!")
#     x_train, x_test, y_train, y_test = pcapHelper.split(x, y, 0.5)
# else:
#     raise Exception("Only one class!")

#classifier = model.fit(x_train, y_train)
#classifier = model.fit(x, y)



clf0 = mlModel0.buildingModelAndFittingTrainSet(x, y)
clf1 = mlModel1.buildingModelAndFittingTrainSet(x, y)
clf2 = mlModel2.buildingModelAndFittingTrainSet(x, y)
clf3 = mlModel3.buildingModelAndFittingTrainSet(x, y)
clf4 = mlModel4.buildingModelAndFittingTrainSet(x, y)
clf5 = mlModel5.buildingModelAndFittingTrainSet(x, y)
clf6 = mlModel6.buildingModelAndFittingTrainSet(x, y)


compression_gain_list = [[] for i in range(10)]
for i in range(10):
    pkts_test = scapy.rdpcap(pcap_test)
    for pktsIndex in range(0, len(pkts_test)):
        uc_pkt  = list(bytearray(bytes(pkts_test[pktsIndex])))
        #pattern = classes[int(classifier.predict([uc_pkt]))]
        pattern0= classes[int(clf0.predict([uc_pkt]))]
        #co_pkt  = comp.compress(uc_pkt, pattern)
        co_pkt0 = comp.compress(uc_pkt, pattern0)

    compression_gain, A, B = comp.get_gain()
    print(compression_gain)
    compression_gain_list[i].append(compression_gain)

    for pktsIndex in range(0, len(pkts_test)):
        uc_pkt  = list(bytearray(bytes(pkts_test[pktsIndex])))
        pattern1= classes[int(clf1.predict([uc_pkt]))]
        co_pkt1 = comp.compress(uc_pkt, pattern1)
    compression_gain, A, B = comp.get_gain()
    print(compression_gain)
    compression_gain_list[i].append(compression_gain)

    for pktsIndex in range(0, len(pkts_test)):
        uc_pkt  = list(bytearray(bytes(pkts_test[pktsIndex])))
        pattern2= classes[int(clf2.predict([uc_pkt]))]
        co_pkt2 = comp.compress(uc_pkt, pattern2)
    compression_gain, A, B = comp.get_gain()
    print(compression_gain)
    compression_gain_list[i].append(compression_gain)

    for pktsIndex in range(0, len(pkts_test)):
        uc_pkt  = list(bytearray(bytes(pkts_test[pktsIndex])))
        pattern3= classes[int(clf3.predict([uc_pkt]))]
        co_pkt3 = comp.compress(uc_pkt, pattern3)
    compression_gain, A, B = comp.get_gain()
    print(compression_gain)
    compression_gain_list[i].append(compression_gain)

    for pktsIndex in range(0, len(pkts_test)):
        uc_pkt  = list(bytearray(bytes(pkts_test[pktsIndex])))
        pattern4= classes[int(clf4.predict([uc_pkt]))]
        co_pkt4 = comp.compress(uc_pkt, pattern4)
    compression_gain, A, B = comp.get_gain()
    print(compression_gain)
    compression_gain_list[i].append(compression_gain)

    for pktsIndex in range(0, len(pkts_test)):
        uc_pkt  = list(bytearray(bytes(pkts_test[pktsIndex])))
        pattern5= classes[int(clf5.predict([uc_pkt]))]
        co_pkt5 = comp.compress(uc_pkt, pattern5)
    compression_gain, A, B = comp.get_gain()
    print(compression_gain)
    compression_gain_list[i].append(compression_gain)

    for pktsIndex in range(0, len(pkts_test)):
        uc_pkt  = list(bytearray(bytes(pkts_test[pktsIndex])))
        pattern6= classes[int(clf6.predict([uc_pkt]))]
        co_pkt6 = comp.compress(uc_pkt, pattern6)
    compression_gain, A, B = comp.get_gain()
    print(compression_gain)
    compression_gain_list[i].append(compression_gain)


compression_gain_means = list()
compression_gain_interval=list()

for j in range(7):
    compression_gain_means.append(100*(compression_gain_list[0][j] + compression_gain_list[1][j] + compression_gain_list[2][j] + compression_gain_list[3][
        j] + compression_gain_list[4][j] + compression_gain_list[5][j] + compression_gain_list[6][j] + compression_gain_list[7][j] +
                                 compression_gain_list[8][j] + compression_gain_list[9][j]) / 10)
    compression_gain_interval.append(1.96 * (np.sqrt(
        np.square(100*compression_gain_list[0][j] - compression_gain_means[j]) + np.square(
            100*compression_gain_list[1][j] - compression_gain_means[j]) + np.square(
            100*compression_gain_list[2][j] - compression_gain_means[j]) + np.square(
            100*compression_gain_list[3][j] - compression_gain_means[j]) + np.square(
            100*compression_gain_list[4][j] - compression_gain_means[j]) + np.square(
            100*compression_gain_list[5][j] - compression_gain_means[j]) + np.square(
            100*compression_gain_list[6][j] - compression_gain_means[j]) + np.square(
            100*compression_gain_list[7][j] - compression_gain_means[j]) + np.square(
            100*compression_gain_list[8][j] - compression_gain_means[j]) + np.square(
            100*compression_gain_list[9][j] - compression_gain_means[j]))) / 10)

label_list = ['Logistic Regession', 'Linear SVM', 'Non-linear SVM', 'Decision Tree','Random Forest','Gaussian Naive Bayes','K Nearest Neighbors Classifier']

plt.figure(figsize=(20, 16))
# plt.bar(np.arange(len(bytes_MAE_list)), height=elapsed_time_list, width=0.8, alpha=0.8, color='red',label="MAE")
# rects1 = plt.bar(np.arange(len(bytes_MAE_list)), height=elapsed_time_list, width=0.6, alpha=0.8, color='red', label="MAE")


plt.bar(np.arange(len(label_list)), height=compression_gain_means, yerr=compression_gain_interval, color='red')
#plt.legend(loc=[1, 0])
plt.ylim(49, 50)
plt.ylabel("compression gain/%")

plt.xticks([j for j in np.arange(len(label_list))], label_list)
# plt.xticks([index + 0.1 for index in np.arange(len(bytes_MAE_list))])
plt.xlabel("compression gain of various machine learning algorithms ")
# plt.title("Elapsed time of various machine learning algorithms (Non-online learning)")
plt.grid(linestyle='-.')

##############################################################################################################################################################
plt.show()