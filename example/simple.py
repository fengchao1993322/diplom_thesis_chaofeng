from o2sc import compressor
from o2sc import decompressor
from o2sc.classification import model

import scapy.all as scapy


pcap_test  = '../pcaps/streams/easyRtp_test_100_1000.pcap'

oracle = model.BasicModel()
comp   = compressor.Compressor()
decomp = decompressor.Decompressor()

pkts_train = scapy.rdpcap('../pcaps/streams/easyRtp_train_100.pcap')
oracle.train(oracle.convert_pcap_to_df(pkts_train))



pkts_test = scapy.rdpcap('../pcaps/streams/easyRtp_test_100_1000.pcap')
for pktsIndex in range(0, len(pkts_test)):
    uc_pkt  = list(bytearray(bytes(pkts_test[pktsIndex])))
    pattern = oracle.predict(uc_pkt)
    co_pkt  = comp.compress(uc_pkt, pattern)

print(comp.get_gain())
